<?php
/* Copyright (C) 2004-2017 Laurent Destailleur  <eldy@users.sourceforge.net>
 * Copyright (C) 2020 Éric Seigne <eric.seigne@cap-rel.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file    doliscan/admin/setup.php
 * \ingroup doliscan
 * \brief   Doliscan setup page.
 */
// require_once __DIR__ . '/../vendor/autoload.php';

// Load Dolibarr environment
$res = 0;
// Try main.inc.php into web root known defined into CONTEXT_DOCUMENT_ROOT (not always defined)
if (!$res && !empty($_SERVER["CONTEXT_DOCUMENT_ROOT"])) {
	$res = @include $_SERVER["CONTEXT_DOCUMENT_ROOT"] . "/main.inc.php";
}
// Try main.inc.php into web root detected using web root calculated from SCRIPT_FILENAME
$tmp = empty($_SERVER['SCRIPT_FILENAME']) ? '' : $_SERVER['SCRIPT_FILENAME'];
$tmp2 = realpath(__FILE__);
$i = strlen($tmp) - 1;
$j = strlen($tmp2) - 1;
while ($i > 0 && $j > 0 && isset($tmp[$i]) && isset($tmp2[$j]) && $tmp[$i] == $tmp2[$j]) {
	$i--;
	$j--;
}
if (!$res && $i > 0 && file_exists(substr($tmp, 0, ($i + 1)) . "/main.inc.php")) {
	$res = @include substr($tmp, 0, ($i + 1)) . "/main.inc.php";
}
if (!$res && $i > 0 && file_exists(dirname(substr($tmp, 0, ($i + 1))) . "/main.inc.php")) {
	$res = @include dirname(substr($tmp, 0, ($i + 1))) . "/main.inc.php";
}
// Try main.inc.php using relative path
if (!$res && file_exists("../../main.inc.php")) {
	$res = @include "../../main.inc.php";
}
if (!$res && file_exists("../../../main.inc.php")) {
	$res = @include "../../../main.inc.php";
}
if (!$res) {
	die("Include of main fails");
}

global $langs, $user;

// Libraries
// require DOL_DOCUMENT_ROOT . '/includes/autoload.php';
require_once DOL_DOCUMENT_ROOT . "/core/lib/admin.lib.php";
//require_once "../class/myclass.class.php";

//On charge les données de configuration des partenaires ...
if (file_exists('../partner.ini')) {
	$partner = @parse_ini_file('../partner.ini');
} else {
	$partner = @parse_ini_file('../partner.ini-dist');
}

dol_include_once('/doliscan/core/modules/modDoliscan.class.php');
dol_include_once('/doliscan/lib/doliscan.lib.php');

$modDoliscan = new modDoliscan($db);

// Translations
$langs->loadLangs(array("admin", "doliscan@doliscan"));

// Access control
if (!$user->admin) {
	accessforbidden();
}

// Parameters
$action = GETPOST('action', 'alpha');
$backtopage = GETPOST('backtopage', 'alpha');

$value = GETPOST('value', 'alpha');

$arrayofparameters = array(
	'DOLISCAN_MAINACCOUNT_EMAIL'  => array('css' => 'minwidth500', 'enabled' => 1),
	'DOLISCAN_MAINACCOUNT_APIKEY' => array('css' => 'minwidth500', 'enabled' => 1),
	'DOLISCAN_STARTDATEIMPORT'    => array('css' => 'minwidth500', 'enabled' => 1)
);

$error = 0;
$setupnotempty = 0;
$checkSecurityCodeEntreprise = $checkSecurityCode = null;

//On recupere la liste des clés de config du serveur doliscan
$applicationKey = dsbackport_getDolGlobalString('DOLISCAN_MAINACCOUNT_APIKEY','');
$email = dsbackport_getDolGlobalString('DOLISCAN_MAINACCOUNT_EMAIL','');
// $email = dsbackport_getDolGlobalString('MAIN_INFO_SOCIETE_MAIL','');
$endpoint = dsbackport_getDolGlobalString('DOLISCAN_MAINSERVER','');
if(empty($endpoint)) {
	$endpoint = $conf->global->DOLISCAN_MAINSERVER = "https://doliscan.fr";
	$result = dolibarr_set_const($db, "DOLISCAN_MAINSERVER", "https://doliscan.fr", 'chaine', 0, '', $conf->entity);
}

/*
 * Actions
 */

if ((float) DOL_VERSION >= 6) {
	include DOL_DOCUMENT_ROOT . '/core/actions_setmoduleoptions.inc.php';
}

if ($action == 'update' && $user->admin) {
	$verifDate = GETPOST('DOLISCAN_STARTDATEIMPORT');
	if (preg_match('/\d{4}-\d{2}-\d{2}/', $verifDate, $matches)) {
		//date ok
	} else {
		setEventMessages($langs->trans("SetupErrorDateFormat"), [], 'errors');
		$result = -1;
	}
}

// if ($action == 'setvalue' && $user->admin) {
// 	//$result=dolibarr_set_const($db, "PAYBOX_IBS_DEVISE",$_POST["PAYBOX_IBS_DEVISE"],'chaine',0,'',$conf->entity);

// 	// $verifDate = GETPOST('DOLISCAN_STARTDATEIMPORT','string');
// 	// if (preg_match('/\d{4}-\d{2}-\d{2}\/', $verifDate, $matches)) {
// 	//     //date ok
// 	// } else {
// 	//     $mesg = $langs->trans("SetupErrorDateFormat");
// 	//     setEventMessages($mesg, [], 'errors');
// 	//     $result = -1;
// 	// }

// 	if ($result >= 0) {
// 		$mesg = '<div class="ok">' . $langs->trans("SetupSaved") . '</div>';
// 	} else {
// 		dol_print_error($db);
// 	}
// }

// if ($action == 'setvalue_account' && $user->admin) {
// 	if ($result >= 0) {
// 		$mesg = '<div class="ok">' . $langs->trans("SetupSaved") . '</div>';
// 	} else {
// 		dol_print_error($db);
// 	}
// }

if ($action == 'checkSecurityCodeEntreprise') {
	$checkSecurityCodeEntreprise = GETPOST('checkSecurityCodeEntreprise', 'alpha');
	dol_syslog("DoliSCAN: checkSecurityCodeEntreprise set (" . $checkSecurityCodeEntreprise . "), retry connect process");
	$action = "connect";
}
//Le serveur doliscan a envoyé un code de sécurité par mail qu'il faut recopier ici
if ($action == 'checkSecurityCode') {
	$checkSecurityCode = GETPOST('checkSecurityCode', 'alpha');
	dol_syslog("DoliSCAN: checkSecurityCode set (" . $checkSecurityCode . "), retry connect process");
	$action = "connect";
}
if ($action == 'checkConnect') {
	//Si on a deja une cle de connexion on l'essaye
	// Informations about your application

	//On teste pour voir si cette clé d'API est utilisable
	if ($applicationKey != "") {
		$param = [ 'email' => $email ];
		$result = getURLContent($endpoint . '/api/ping', 'POST', json_encode($param), 1, doliSCANApiCommonHeader($applicationKey), ['http','https'], 2);
		if (isset($result['curl_error_no'])) {
			handleDoliscanTimeoutBlacklist($result);
		}

		if (is_array($result) && $result['http_code'] == 200 && isset($result['content'])) {
			$ret = json_decode($result['content']);
			if (isset($ret->result)) {
				$data = $ret->result;
			}
			// dol_syslog("DoliSCAN: Request response body :  " . json_encode($data), LOG_DEBUG);
			$mesg = '<div class="ok">' . $langs->trans("CheckConnectOK") . '</div>';
			setEventMessages($mesg, [], 'mesgs');
			$mesg = "";
		} else {
			$error++;
			$curl_message = $result['curl_error_msg'] ?? '';
			$mesg = '<div class="error">' . $langs->trans("CheckConnectErrorAdmin") . $curl_message . '</div>';
			setEventMessages($mesg, [], 'errors');
			$mesg = "";
		}
	}
}
if ($action == 'connect') {
	// Informations about your application
	$applicationKey = dsbackport_getDolGlobalString('DOLISCAN_MAINACCOUNT_APIKEY','');
	$prenom = $user->firstname;
	$nom = $user->lastname;
	//adresse mail : priorité conf spécifique
	$email = dsbackport_getDolGlobalString('DOLISCAN_MAINACCOUNT_EMAIL',$mysoc->email ?? $user->email);
	$emailSoc = dsbackport_getDolGlobalString('MAIN_INFO_SOCIETE_MAIL','');

	$error = 0;
	$message = "";

	if ($prenom == "") {
		$message .= "<li>" . $langs->trans("ErrorUserFirstnameEmpty") . "</li>";
		$error++;
	}
	if ($nom == "") {
		$message .= "<li>" . $langs->trans("ErrorUserLastnameEmpty") . "</li>";
		$error++;
	}
	if ($email == "") {
		$message .= "<li>" . $langs->trans("ErrorUserEmailEmpty") . "</li>";
		$error++;
	}
	if ($emailSoc == "") {
		$message .= "<li>" . $langs->trans("ErrorSocEmailEmpty") . "</li>";
		$error++;
	}

	//On détaille le message d'erreur
	$errorSoc = 0;
	$mSoc = "";
	$tabRequired = array(
		'name' => "ErrorSocEmptyNOM",
		'address' => "ErrorSocEmptyADDRESS",
		'zip' => "ErrorSocEmptyZIP",
		'town' => "ErrorSocEmptyTOWN",
		'country' => "ErrorSocEmptyCOUNTRY",
		'url' => "ErrorSocEmptyWEB",
		'phone' => "ErrorSocEmptyTEL",
		'idprof2' => "ErrorSocEmptySIRET"
	);
	//TODO pour la version worldwide voir $langs->transcountry("ProfId2", $mysoc->country_code);

	foreach ($tabRequired as $k => $v) {
		if ($mysoc->$k == "") {
			$mSoc .= "<li>" . $langs->trans($v) . "</li>";
			$errorSoc++;
		}
	}
	if ($errorSoc > 0) {
		$message .= "<li>" . $langs->trans("ErrorSocEmpty") . "</li>";
		$message .= $mSoc;
		$error++;
	}

	if ($applicationKey == "" && $error == 0) {
		dol_syslog("DoliSCAN: Request credential to endpoint " . $endpoint . "/api/register");
		$param = [
			'firstname' => $prenom,
			'name' => $nom,
			'email' => $email,
			'emailSoc' => $emailSoc,
			'siret' => ($mysoc->idprof2),
			'siren' => ($mysoc->idprof1),
			'cgu' => '1',
			'autre' => '',
			'checkSecurityCode' => $checkSecurityCode,
			'checkSecurityCodeEntreprise' => $checkSecurityCodeEntreprise,
			'partner' => $partner['emailrvd'],
			'askForAPI' => 'true',
			'askForAPIAppName' => 'dolibarr-' . $mysoc->name,
		];
		$result = getURLContent($endpoint . '/api/register', 'POST', json_encode($param), 1, doliSCANApiCommonHeader('', false), ['http','https'], 2);

		if (isset($result['curl_error_no'])) {
			handleDoliscanTimeoutBlacklist($result);
		}

		if (is_array($result)) {
			$json = json_decode($result['content']);
			$data = null;
			if (isset($json->data)) {
				$data = $json->data;
			}
			if ($result['http_code'] == 202) {
				$action = 'checkSecurityCode1';
				$message = $langs->trans("DoliscanSetupPleaseEnterCode");
			} elseif ($result['http_code'] > 199 && $result['http_code'] < 300) {
				$applicationKey = $data->api_token;
				dol_syslog("DoliSCAN: register returns = " . json_encode($data));

				dol_syslog("DoliSCAN: Update entreprise infos " . $endpoint . " with applicationKey=" . $applicationKey);
				//On lance l'actualisation de l'entité entreprise vu qu'on a toutes les infos dans dolibarr
				//S'il n'a qu'une seule entreprise ça passe ... s'il y en a plus ça ne devrait pas passer ...
				$param2 = [
					'name' => ($mysoc->name),
					'adresse' => ($mysoc->address),
					'cp' => ($mysoc->zip),
					'ville' => ($mysoc->town),
					'pays' => ($mysoc->country),
					'web' => ($mysoc->url),
					'tel' => ($mysoc->phone),
					'siret' => ($mysoc->idprof2),
					'siren' => ($mysoc->idprof1),
					'email' => $emailSoc,
					'emailAdmin' => $email,
					'partner' => $partner['emailrvd'],
					'checkSecurityCodeEntreprise' => $checkSecurityCodeEntreprise,
				];
				$result2 = getURLContent($endpoint . '/api/Entreprise', 'PUTALREADYFORMATED', json_encode($param2), 1, doliSCANApiCommonHeader($applicationKey), ['http','https'], 2);

				if (isset($result['curl_error_no'])) {
					handleDoliscanTimeoutBlacklist($result);
				}

				if (is_array($result2)) {
					$data2 = json_decode($result2['content']);
					if ($result2['http_code'] == 202) {
						$action = 'checkSecurityCode2';
					} elseif ($result2['http_code'] > 199 && $result2['http_code'] < 300) {
						$r1 = dolibarr_set_const($db, "DOLISCAN_MAINACCOUNT_APIKEY", $applicationKey, 'chaine', 0, '', $conf->entity);
						$r2 = dolibarr_set_const($db, "DOLISCAN_MAINACCOUNT_EMAIL", $email, 'chaine', 0, '', $conf->entity);
						$message .=  $langs->trans("SetupSaved");
					} else {
						$error++;
						$message .= 'server response is bad (0), server error code is : ' . $result2['http_code'];
						if (isset($result2['content'])) {
							$message .= $result2['content'];
						}
						handleDoliscanTimeoutBlacklist($result2);
						dol_syslog("DoliSCAN: Erreur serveur " . json_encode($result2), LOG_WARNING);
					}
				} else {
					dol_syslog("DoliSCAN: Erreur serveur " . json_encode($result2), LOG_WARNING);
					$message .= 'server response is bad (1), server error code is : ' . $result2['http_code'];
					$error++;
					handleDoliscanTimeoutBlacklist($result2);
				}
			} else {
				if (isset($result['content'])) {
					$message .= $result['content'];
				}
				dol_syslog("DoliSCAN: Erreur serveur " . json_encode($result), LOG_WARNING);
				$message .= 'server response is bad (2), server error code is : ' . $result['http_code'];
				$error++;
				handleDoliscanTimeoutBlacklist($result);
			}
		} else {
			dol_syslog("DoliSCAN: Erreur serveur " . json_encode($result), LOG_WARNING);
			$message .= 'server response is bad (3), server error code is : ' . $result['http_code'];
			$error++;
			handleDoliscanTimeoutBlacklist($result);
		}
	}
	if ($error > 0) {
		$mesg = '<div class="error">' . $message . '</div>';
		// dol_print_error($db);
	} else {
		$mesg = '<div class="ok">' . $message . '</div>';
	}
}

/*
 * View
 */

$form = new Form($db);

$dirmodels = array_merge(array('/'), (array) $conf->modules_parts['models']);

$page_name = "DoliscanSetup";
llxHeader('', $langs->trans($page_name));

// Subheader
$linkback = '<a href="' . ($backtopage ? $backtopage : DOL_URL_ROOT . '/admin/modules.php?restore_lastsearch_values=1') . '">' . $langs->trans("BackToModuleList") . '</a>';

print load_fiche_titre($langs->trans($page_name), $linkback, 'object_doliscan@doliscan');

// Configuration header
$head = doliscanAdminPrepareHead();
dol_fiche_head($head, 'admin', '', -1, "doliscan@doliscan");

// Setup page goes here
echo '<span class="">' . $langs->trans("DoliscanSetupPage") . '</span><br><br>';

$iconWarn = "fas fa-exclamation-triangle";
if (((int) DOL_VERSION) < 18) {
	$iconWarn = "fa fa-warning";
}

echo '<div class="warning">';
if (dsbackport_getDolGlobalString('DOLISCAN_MAINSERVER','') == "https://doliscan.fr") {
	echo '<span class="' . $iconWarn . '"> </span> <span class="clear"> ' . $langs->trans("DoliscanSetupInfo1", $partner['tarifs']) . '</span><br />';
	echo '<span class="">' . $langs->trans("DoliscanSetupInfo2", $partner['conditionsgenerales']) . '</span><br />';
}

echo '<span class="">' . $langs->trans("DoliscanSetupInfo3", $partner['documentation']) . '</span><br />';
echo '<span class="">' . $langs->trans("DoliscanSetupInfo4", "https://cap-rel.fr/") . '</span><br />';
echo '</div>';

if ($action == 'edit') {
	print '<form method="POST" action="' . $_SERVER["PHP_SELF"] . '">';
	print '<input type="hidden" name="token" value="' . newToken() . '">';
	print '<input type="hidden" name="action" value="update">';

	print '<table class="noborder centpercent">';
	print '<tr class="liste_titre"><td class="titlefield">' . $langs->trans("Parameter") . '</td><td>' . $langs->trans("Value") . '</td></tr>';

	if (dsbackport_getDolGlobalString('DOLISCAN_STARTDATEIMPORT','') == "") {
		$conf->global->DOLISCAN_STARTDATEIMPORT = date("Y-m") . '-01';
	}

	foreach ($arrayofparameters as $key => $val) {
		print '<tr class="oddeven"><td>';
		$tooltiphelp = (($langs->trans($key . 'Tooltip') != $key . 'Tooltip') ? $langs->trans($key . 'Tooltip') : '');
		print $form->textwithpicto($langs->trans($key), $tooltiphelp);
		print '</td><td><input name="' . $key . '"  class="flat ' . (empty($val['css']) ? 'minwidth200' : $val['css']) . '" value="' . ($conf->global->$key ?? '') . '"></td></tr>';
	}
	print '</table>';

	print '<br><div class="center">';
	print '<input class="button" type="submit" value="' . $langs->trans("Save") . '">';
	print '</div>';

	print '</form>';
	print '<br>';
} elseif ($action == "checkSecurityCode1") {
	print "<p>" . $langs->trans("DoliscanSetupAccountExistMailSent") . "</p>";

	print '<form method="POST" action="' . $_SERVER["PHP_SELF"] . '">';
	print '<input type="hidden" name="token" value="' . newToken() . '">';
	print '<input type="hidden" name="action" value="checkSecurityCode">';

	print '<table class="noborder centpercent">';
	print '<tr class="liste_titre"><td class="">' . $langs->trans("Parameter") . '</td><td>' . $langs->trans("Value") . '</td></tr>';
	print '<tr class="oddeven"><td>' . $langs->trans("DoliscanSetupPleaseEnterCode");
	print '</td><td style="word-wrap: anywhere;"><input type="text" name="checkSecurityCode"></td></tr>';
	print '</table>';

	print '<br><div class="center">';
	print '<input class="button" type="submit" value="' . $langs->trans("Check") . '">';
	print '</div>';

	print '</form>';
	print '<br>';
} elseif ($action == "checkSecurityCode2") {
	print '<form method="POST" action="' . $_SERVER["PHP_SELF"] . '">';
	print '<input type="hidden" name="token" value="' . newToken() . '">';
	print '<input type="hidden" name="action" value="checkSecurityCodeEntreprise">';

	print '<table class="noborder centpercent">';
	print '<tr class="liste_titre"><td class="">' . $langs->trans("Parameter") . '</td><td>' . $langs->trans("Value") . '</td></tr>';
	print "<tr class='oddeven'><td>" . $langs->trans("DoliscanSetupPleaseEnterCodeCompany");
	print '</td><td style="word-wrap: anywhere;"><input type="text" name="checkSecurityCodeEntreprise"></td></tr>';
	print '</table>';

	print '<br><div class="center">';
	print '<input class="button" type="submit" value="' . $langs->trans("Check") . '">';
	print '</div>';

	print '</form>';
	print '<br>';
} else {
	if (!empty($arrayofparameters)) {
		$checked = "";
		if (dsbackport_getDolGlobalString('DOLISCAN_MAINACCOUNT_APIKEY','') != '') {
			$checked = "checked";
		}

		if (dsbackport_getDolGlobalString('DOLISCAN_MAINSERVER','') == "https://doliscan.fr") {
			print "<div>
        <label for=\"terms_and_conditions\">" . $langs->trans("AcceptCGU") . "</label>
        <input type=\"checkbox\" id=\"terms_and_conditions\" value=\"1\" $checked onclick=\"if(this.checked) {jQuery('#initialisationForm').removeClass('ui-state-disabled')} else {jQuery('#initialisationForm').addClass('ui-state-disabled')}\" />
	    </div>";
		} else {
			$checked = "checked";
		}

		if ($checked) {
			print "<div id=\"initialisationForm\" class=\"\">";
		} else {
			print "<div id=\"initialisationForm\" class=\"ui-state-disabled\">";
		}
		print '<table class="noborder centpercent">';
		print '<tr class="liste_titre"><td class="titlefield">' . $langs->trans("Parameter") . '</td><td>' . $langs->trans("Value") . '</td></tr>';

		if (dsbackport_getDolGlobalString('DOLISCAN_STARTDATEIMPORT','') == "") {
			$conf->global->DOLISCAN_STARTDATEIMPORT = dol_get_first_day((int) date("Y"), (int) date("m"));
		}

		foreach ($arrayofparameters as $key => $val) {
			$setupnotempty++;

			print '<tr class="oddeven"><td>';
			$tooltiphelp = (($langs->trans($key . 'Tooltip') != $key . 'Tooltip') ? $langs->trans($key . 'Tooltip') : '');
			print $form->textwithpicto($langs->trans($key), $tooltiphelp);
			print '</td><td style="word-wrap: anywhere;">' . ($conf->global->$key ?? '') . '</td></tr>';
		}

		print '</table>';

		print '<div class="tabsAction">';

		if (dsbackport_getDolGlobalString('DOLISCAN_MAINACCOUNT_APIKEY','') != '') {
			print '<a class="butAction" href="' . $_SERVER["PHP_SELF"] . '?action=checkConnect">' . $langs->trans("CheckConnectToDoliSCAN") . '</a>';
		} else {
			print '<a class="butAction" href="' . $_SERVER["PHP_SELF"] . '?action=connect">' . $langs->trans("NewAccountDoliSCAN") . '</a>';
		}
		print '<a class="butAction" href="' . $_SERVER["PHP_SELF"] . '?action=edit">' . $langs->trans("ManualConfigDoliSCAN") . '</a>';

		print '</div>';
	} else {
		print '<br>' . $langs->trans("NothingToSetup");
	}
}


$moduledir = 'doliscan';
$myTmpObjects = array();
$myTmpObjects['MyObject'] = array('includerefgeneration' => 0, 'includedocgeneration' => 0);


foreach ($myTmpObjects as $myTmpObjectKey => $myTmpObjectArray) {
	if ($myTmpObjectKey == 'MyObject') {
		continue;
	}
	if ($myTmpObjectArray['includerefgeneration']) {
		/*
		 * Orders Numbering model
		 */
		$setupnotempty++;

		print load_fiche_titre($langs->trans("NumberingModules", $myTmpObjectKey), '', '');

		print '<table class="noborder centpercent">';
		print '<tr class="liste_titre">';
		print '<td>' . $langs->trans("Name") . '</td>';
		print '<td>' . $langs->trans("Description") . '</td>';
		print '<td class="nowrap">' . $langs->trans("Example") . '</td>';
		print '<td class="center" width="60">' . $langs->trans("Status") . '</td>';
		print '<td class="center" width="16">' . $langs->trans("ShortInfo") . '</td>';
		print '</tr>' . "\n";

		clearstatcache();

		foreach ($dirmodels as $reldir) {
			$dir = dol_buildpath($reldir . "core/modules/" . $moduledir);

			if (is_dir($dir)) {
				$handle = opendir($dir);
				if (is_resource($handle)) {
					while (($file = readdir($handle)) !== false) {
						if (strpos($file, 'mod_' . strtolower($myTmpObjectKey) . '_') === 0 && substr($file, dol_strlen($file) - 3, 3) == 'php') {
							$file = substr($file, 0, dol_strlen($file) - 4);

							require_once $dir . '/' . $file . '.php';

							$module = new $file($db);

							// Show modules according to features level
							if ($module->version == 'development' && dsbackport_getDolGlobalString('MAIN_FEATURES_LEVEL',-1) < 2) {
								continue;
							}
							if ($module->version == 'experimental' && dsbackport_getDolGlobalString('MAIN_FEATURES_LEVEL',-1) < 1) {
								continue;
							}

							if ($module->isEnabled()) {
								dol_include_once('/' . $moduledir . '/class/' . strtolower($myTmpObjectKey) . '.class.php');

								print '<tr class="oddeven"><td>' . $module->name . "</td><td>\n";
								print $module->info();
								print '</td>';

								// Show example of numbering model
								print '<td class="nowrap">';
								$tmp = $module->getExample();
								if (preg_match('/^Error/', $tmp)) {
									print '<div class="error">' . $langs->trans($tmp) . '</div>';
								} elseif ($tmp == 'NotConfigured') {
									print $langs->trans($tmp);
								} else {
									print $tmp;
								}
								print '</td>' . "\n";

								print '<td class="center">';
								$constforvar = 'DOLISCAN_' . strtoupper($myTmpObjectKey) . '_ADDON';
								if ($conf->global->$constforvar == $file) {
									print img_picto($langs->trans("Activated"), 'switch_on');
								} else {
									print '<a href="' . $_SERVER["PHP_SELF"] . '?action=setmod&object=' . strtolower($myTmpObjectKey) . '&value=' . $file . '">';
									print img_picto($langs->trans("Disabled"), 'switch_off');
									print '</a>';
								}
								print '</td>';

								$mytmpinstance = new $myTmpObjectKey($db);
								$mytmpinstance->initAsSpecimen();

								// Info
								$htmltooltip = '';
								$htmltooltip .= '' . $langs->trans("Version") . ': <b>' . $module->getVersion() . '</b><br>';

								$nextval = $module->getNextValue($mytmpinstance);
								if ("$nextval" != $langs->trans("NotAvailable")) {  // Keep " on nextval
									$htmltooltip .= '' . $langs->trans("NextValue") . ': ';
									if ($nextval) {
										if (preg_match('/^Error/', $nextval) || $nextval == 'NotConfigured') {
											$nextval = $langs->trans($nextval);
										}
										$htmltooltip .= $nextval . '<br>';
									} else {
										$htmltooltip .= $langs->trans($module->error) . '<br>';
									}
								}

								print '<td class="center">';
								print $form->textwithpicto('', $htmltooltip, 1, 0);
								print '</td>';

								print "</tr>\n";
							}
						}
					}
					closedir($handle);
				}
			}
		}
		print "</table><br>\n";
	}

	if ($myTmpObjectArray['includedocgeneration']) {
		/*
		 * Document templates generators
		 */
		$setupnotempty++;
		$type = strtolower($myTmpObjectKey);

		print load_fiche_titre($langs->trans("DocumentModules", $myTmpObjectKey), '', '');

		// Load array def with activated templates
		$def = array();
		$sql = "SELECT nom";
		$sql .= " FROM " . MAIN_DB_PREFIX . "document_model";
		$sql .= " WHERE type = '" . $type . "'";
		$sql .= " AND entity IN (" . getEntity('user') . ')';
		$resql = $db->query($sql);
		if ($resql) {
			$i = 0;
			$num_rows = $db->num_rows($resql);
			while ($i < $num_rows) {
				$array = $db->fetch_array($resql);
				array_push($def, $array[0]);
				$i++;
			}
		} else {
			dol_print_error($db);
		}

		print "<table class=\"noborder\" width=\"100%\">\n";
		print "<tr class=\"liste_titre\">\n";
		print '<td>' . $langs->trans("Name") . '</td>';
		print '<td>' . $langs->trans("Description") . '</td>';
		print '<td class="center" width="60">' . $langs->trans("Status") . "</td>\n";
		print '<td class="center" width="60">' . $langs->trans("Default") . "</td>\n";
		print '<td class="center" width="38">' . $langs->trans("ShortInfo") . '</td>';
		print '<td class="center" width="38">' . $langs->trans("Preview") . '</td>';
		print "</tr>\n";

		clearstatcache();

		foreach ($dirmodels as $reldir) {
			foreach (array('', '/doc') as $valdir) {
				$realpath = $reldir . "core/modules/" . $moduledir . $valdir;
				$dir = dol_buildpath($realpath);

				if (is_dir($dir)) {
					$handle = opendir($dir);
					if (is_resource($handle)) {
						while (($file = readdir($handle)) !== false) {
							$filelist[] = $file;
						}
						closedir($handle);
						arsort($filelist);

						foreach ($filelist as $file) {
							if (preg_match('/\.modules\.php$/i', $file) && preg_match('/^(pdf_|doc_)/', $file)) {
								if (file_exists($dir . '/' . $file)) {
									$name = substr($file, 4, dol_strlen($file) - 16);
									$classname = substr($file, 0, dol_strlen($file) - 12);

									require_once $dir . '/' . $file;
									$module = new $classname($db);

									$modulequalified = 1;
									if ($module->version == 'development' && dsbackport_getDolGlobalString('MAIN_FEATURES_LEVEL',-1) < 2) {
										$modulequalified = 0;
									}
									if ($module->version == 'experimental' && dsbackport_getDolGlobalString('MAIN_FEATURES_LEVEL',-1) < 1) {
										$modulequalified = 0;
									}

									if ($modulequalified) {
										print '<tr class="oddeven"><td width="100">';
										print(empty($module->name) ? $name : $module->name);
										print "</td><td>\n";
										if (method_exists($module, 'info')) {
											print $module->info($langs);
										} else {
											print $module->description;
										}
										print '</td>';

										// Active
										if (in_array($name, $def)) {
											print '<td class="center">' . "\n";
											print '<a href="' . $_SERVER["PHP_SELF"] . '?action=del&value=' . $name . '">';
											print img_picto($langs->trans("Enabled"), 'switch_on');
											print '</a>';
											print '</td>';
										} else {
											print '<td class="center">' . "\n";
											print '<a href="' . $_SERVER["PHP_SELF"] . '?action=set&value=' . $name . '&amp;scan_dir=' . $module->scandir . '&amp;label=' . urlencode($module->name) . '">' . img_picto($langs->trans("Disabled"), 'switch_off') . '</a>';
											print "</td>";
										}

										// Default
										print '<td class="center">';
										$constforvar = 'DOLISCAN_' . strtoupper($myTmpObjectKey) . '_ADDON';
										if ($conf->global->$constforvar == $name) {
											print img_picto($langs->trans("Default"), 'on');
										} else {
											print '<a href="' . $_SERVER["PHP_SELF"] . '?action=setdoc&value=' . $name . '&amp;scan_dir=' . $module->scandir . '&amp;label=' . urlencode($module->name) . '" alt="' . $langs->trans("Default") . '">' . img_picto($langs->trans("Disabled"), 'off') . '</a>';
										}
										print '</td>';

										// Info
										$htmltooltip = '' . $langs->trans("Name") . ': ' . $module->name;
										$htmltooltip .= '<br>' . $langs->trans("Type") . ': ' . ($module->type ? $module->type : $langs->trans("Unknown"));
										if ($module->type == 'pdf') {
											$htmltooltip .= '<br>' . $langs->trans("Width") . '/' . $langs->trans("Height") . ': ' . $module->page_largeur . '/' . $module->page_hauteur;
										}
										$htmltooltip .= '<br>' . $langs->trans("Path") . ': ' . preg_replace('/^\//', '', $realpath) . '/' . $file;

										$htmltooltip .= '<br><br><u>' . $langs->trans("FeaturesSupported") . ':</u>';
										$htmltooltip .= '<br>' . $langs->trans("Logo") . ': ' . yn($module->option_logo, 1, 1);
										$htmltooltip .= '<br>' . $langs->trans("MultiLanguage") . ': ' . yn($module->option_multilang, 1, 1);

										print '<td class="center">';
										print $form->textwithpicto('', $htmltooltip, 1, 0);
										print '</td>';

										// Preview
										print '<td class="center">';
										if ($module->type == 'pdf') {
											print '<a href="' . $_SERVER["PHP_SELF"] . '?action=specimen&module=' . $name . '&object=' . $myTmpObjectKey . '">' . img_object($langs->trans("Preview"), 'generic') . '</a>';
										} else {
											print img_object($langs->trans("PreviewNotAvailable"), 'generic');
										}
										print '</td>';

										print "</tr>\n";
									}
								}
							}
						}
					}
				}
			}
		}

		print '</table>';
	}
}


// Page end
dol_fiche_end();

if(isset($mesg)) {
	dol_htmloutput_mesg($mesg);
}

llxFooter();
$db->close();
