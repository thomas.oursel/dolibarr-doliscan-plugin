<?php
/* Copyright (C) 2004-2017 Laurent Destailleur  <eldy@users.sourceforge.net>
 * Copyright (C) 2020 Éric Seigne <eric.seigne@cap-rel.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file    doliscan/admin/setup.php
 * \ingroup doliscan
 * \brief   Doliscan setup page.
 */
// require_once __DIR__ . '/../vendor/autoload.php';

// Load Dolibarr environment
$res = 0;
// Try main.inc.php into web root known defined into CONTEXT_DOCUMENT_ROOT (not always defined)
if (!$res && !empty($_SERVER["CONTEXT_DOCUMENT_ROOT"])) $res = @include $_SERVER["CONTEXT_DOCUMENT_ROOT"] . "/main.inc.php";
// Try main.inc.php into web root detected using web root calculated from SCRIPT_FILENAME
$tmp = empty($_SERVER['SCRIPT_FILENAME']) ? '' : $_SERVER['SCRIPT_FILENAME'];
$tmp2 = realpath(__FILE__);
$i = strlen($tmp) - 1;
$j = strlen($tmp2) - 1;
while ($i > 0 && $j > 0 && isset($tmp[$i]) && isset($tmp2[$j]) && $tmp[$i] == $tmp2[$j]) {
	$i--;
	$j--;
}
if (!$res && $i > 0 && file_exists(substr($tmp, 0, ($i + 1)) . "/main.inc.php")) $res = @include substr($tmp, 0, ($i + 1)) . "/main.inc.php";
if (!$res && $i > 0 && file_exists(dirname(substr($tmp, 0, ($i + 1))) . "/main.inc.php")) $res = @include dirname(substr($tmp, 0, ($i + 1))) . "/main.inc.php";
// Try main.inc.php using relative path
if (!$res && file_exists("../../main.inc.php")) $res = @include "../../main.inc.php";
if (!$res && file_exists("../../../main.inc.php")) $res = @include "../../../main.inc.php";
if (!$res) die("Include of main fails");

global $langs, $user;

// Libraries
// require DOL_DOCUMENT_ROOT . '/includes/autoload.php';
require_once DOL_DOCUMENT_ROOT . "/core/lib/admin.lib.php";
//require_once "../class/myclass.class.php";

//On charge les données de configuration des partenaires ...
if (file_exists('../partner.ini')) {
	$partner = @parse_ini_file('../partner.ini');
} else {
	$partner = @parse_ini_file('../partner.ini-dist');
}

dol_include_once('/doliscan/core/modules/modDoliscan.class.php');
dol_include_once('/doliscan/lib/doliscan.lib.php');

$modDoliscan = new modDoliscan($db);

// Translations
$langs->loadLangs(array("admin", "doliscan@doliscan"));

// Access control
if (!$user->admin) accessforbidden();

// Parameters
$action = GETPOST('action', 'alpha');
$backtopage = GETPOST('backtopage', 'alpha');

$value = GETPOST('value', 'alpha');

$arrayofparametersServer = array(
	'DOLISCAN_MAINSERVER'           => array('enabled' => 1),
);

$error = 0;
$setupnotempty = 0;

/*
 * Actions
 */
if ((float) DOL_VERSION >= 6) {
	include DOL_DOCUMENT_ROOT . '/core/actions_setmoduleoptions.inc.php';
}

// if ($action == 'setvalue' && $user->admin) {
// 	dol_syslog("DoliSCAN: setvalue");

// 	//$entity = getEntity('user');
// 	//$result=dolibarr_set_const($db, "PAYBOX_IBS_DEVISE",$_POST["PAYBOX_IBS_DEVISE"],'chaine',0,'',$conf->entity);

// 	if ($result >= 0) {
// 		$mesg = '<div class="ok">' . $langs->trans("SetupSaved") . '</div>';
// 	} else {
// 		dol_print_error($db);
// 	}
// }

// if ($action == 'setvalue_account' && $user->admin) {
// 	dol_syslog("DoliSCAN: setvalue_account");
// 	if ($result >= 0) {
// 		$mesg = '<div class="ok">' . $langs->trans("SetupSaved") . '</div>';
// 	} else {
// 		dol_print_error($db);
// 	}
// }

/*
 * View
 */

$form = new Form($db);

$dirmodels = array_merge(array('/'), (array) $conf->modules_parts['models']);

$page_name = "DoliscanSetup";
llxHeader('', $langs->trans($page_name));

// Subheader
$linkback = '<a href="' . ($backtopage ? $backtopage : DOL_URL_ROOT . '/admin/modules.php?restore_lastsearch_values=1') . '">' . $langs->trans("BackToModuleList") . '</a>';

print load_fiche_titre($langs->trans($page_name), $linkback, 'object_doliscan@doliscan');

// Configuration header
$head = doliscanAdminPrepareHead();
dol_fiche_head($head, 'server', '', -1, "doliscan@doliscan");

// Setup page goes here
echo '<span class="">' . $langs->trans("DoliscanSetupPageSrv") . '</span><br><br>';

if ($action == 'save') {
	$key = "DOLISCAN_MAINSERVER";
	$val = GETPOST($key);
	dol_syslog("DoliSCAN: save $key : $val");

	$result = dolibarr_set_const($db, "DOLISCAN_MAINSERVER", $val, 'chaine', 0, '', $conf->entity);
	if ($result == 1) {
		$mesg = '<div class="ok">' . $langs->trans("SetupSaved") . ": " . $val . '</div>';
	} else {
		$mesg = '<div class="error">Error : ' . " val=$val | res=$result | entity= " . getEntity('user')  . '</div>';
	}
}

if (!empty($arrayofparametersServer)) {
	print '<form method="POST" action="' . $_SERVER["PHP_SELF"] . '">';
	print '<input type="hidden" name="token" value="' . newToken() . '">';
	print '<input type="hidden" name="action" value="save">';

	print "<div id=\"initialisationForm\" class=\"\">";
	print '<table class="noborder centpercent">';
	print '<tr class="liste_titre"><td class="titlefield">' . $langs->trans("Parameter") . '</td><td>' . $langs->trans("Value") . '</td></tr>';


	//Val par défaut
	if (dsbackport_getDolGlobalString('DOLISCAN_MAINSERVER','') == "") {
		$conf->global->DOLISCAN_MAINSERVER = "https://doliscan.fr";
	}
	$tabServer = array(
		$langs->trans("customSelfHosted") => $conf->global->DOLISCAN_MAINSERVER,
		$langs->trans("officialBePaid") => "https://doliscan.fr",
		$langs->trans("testsForNoCost") => "https://doliscan.devtemp.fr"
	);

	print '<tr class="oddeven"><td>Serveur</td>';
	print '<td style="word-wrap: anywhere;"><select id="chooseServer">';
	foreach ($tabServer as $k => $v) {
		if (dsbackport_getDolGlobalString('DOLISCAN_MAINSERVER','') == $v) {
			$s = "selected";
		} else {
			$s = "";
		}
		print '<option value="' . $v . '" ' . $s . '>' . $k . '</option>';
	}
	print '</select></td></tr>';
	print "<script type='text/javascript' language='javascript'>
jQuery(document).ready(function() {
    $('#chooseServer').change(function () {
        var optionSelected = $(this).find('option:selected');
        var valueSelected  = optionSelected.val();
        var textSelected   = optionSelected.text();
        if(textSelected.includes('custom')) {
		    $('#DOLISCAN_MAINSERVER').removeAttr('readonly');
            $('#DOLISCAN_MAINSERVER').val('');
        }
        else {
            $('#DOLISCAN_MAINSERVER').val(valueSelected);
            $('#DOLISCAN_MAINSERVER').attr('readonly','readonly');
        }
    });
});
 </script>";


	foreach ($arrayofparametersServer as $key => $val) {
		print '<tr class="oddeven"><td>';
		$tooltiphelp = (($langs->trans($key . 'Tooltip') != $key . 'Tooltip') ? $langs->trans($key . 'Tooltip') : '');
		print $form->textwithpicto($langs->trans($key), $tooltiphelp);
		print '</td><td><input id="' . $key . '" name="' . $key . '"  class="flat ' . (empty($val['css']) ? 'minwidth200' : $val['css']) . '" value="' . ($conf->global->$key ?? '') . '"></td></tr>';
	}
	print '</table>';

	print "<p>" . $langs->trans("DoliscanSetupInfo1", $partner['tarifs']) . "</p>";
	echo '<span class="">' . $langs->trans("DoliscanSetupInfo4", "https://cap-rel.fr/") . '</span>';


	print '<div class="tabsAction">';

	print '<input type="submit" class="butAction" value="' . $langs->trans("Save") . '">';
	// print '<a  href="' . $_SERVER["PHP_SELF"] . '">' . $langs->trans("Save") . '</a>';

	print '</div>';

	print '</form>';
} else {
	if (!empty($arrayofparameters)) {
		$checked = "";
		if (dsbackport_getDolGlobalString('DOLISCAN_MAINACCOUNT_APIKEY','') != '') {
			$checked = "checked";
		}


		print "<div>
        <label for=\"terms_and_conditions\">" . $langs->trans("AcceptCGU") . "</label>
        <input type=\"checkbox\" id=\"terms_and_conditions\" value=\"1\" $checked onclick=\"if(this.checked) {jQuery('#initialisationForm').removeClass('ui-state-disabled')} else {jQuery('#initialisationForm').addClass('ui-state-disabled')}\" />
	    </div>";

		if ($checked) {
			print "<div id=\"initialisationForm\" class=\"\">";
		} else {
			print "<div id=\"initialisationForm\" class=\"ui-state-disabled\">";
		}
		print '<table class="noborder centpercent">';
		print '<tr class="liste_titre"><td class="titlefield">' . $langs->trans("Parameter") . '</td><td>' . $langs->trans("Value") . '</td></tr>';

		foreach ($arrayofparameters as $key => $val) {
			$setupnotempty++;

			print '<tr class="oddeven"><td>';
			$tooltiphelp = (($langs->trans($key . 'Tooltip') != $key . 'Tooltip') ? $langs->trans($key . 'Tooltip') : '');
			print $form->textwithpicto($langs->trans($key), $tooltiphelp);
			print '</td><td style="word-wrap: anywhere;">' . ($conf->global->$key ?? '') . '</td></tr>';
		}

		print '</table>';

		print '<div class="tabsAction">';

		if (dsbackport_getDolGlobalString('DOLISCAN_MAINACCOUNT_APIKEY','')) {
			print '<a class="butAction" href="' . $_SERVER["PHP_SELF"] . '?action=checkConnect">' . $langs->trans("CheckConnectToDoliSCAN") . '</a>';
		} else {
			print '<a class="butAction" href="' . $_SERVER["PHP_SELF"] . '?action=connect">' . $langs->trans("NewAccountDoliSCAN") . '</a>';
		}
		print '<a class="butAction" href="' . $_SERVER["PHP_SELF"] . '?action=edit">' . $langs->trans("ManualConfigDoliSCAN") . '</a>';

		print '</div>';
	} else {
		print '<br>' . $langs->trans("NothingToSetup");
	}
}

// Page end
dol_fiche_end();

if(isset($mesg)) {
	dol_htmloutput_mesg($mesg);
}

llxFooter();
$db->close();
