<?php
/* Copyright (C) 2004-2017 Laurent Destailleur  <eldy@users.sourceforge.net>
 * Copyright (C) 2020 Éric Seigne <eric.seigne@cap-rel.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file    doliscan/admin/setup.php
 * \ingroup doliscan
 * \brief   Doliscan setup page.
 */
// require_once __DIR__ . '/../vendor/autoload.php';

// Load Dolibarr environment
$res = 0;
// Try main.inc.php into web root known defined into CONTEXT_DOCUMENT_ROOT (not always defined)
if (!$res && !empty($_SERVER["CONTEXT_DOCUMENT_ROOT"])) {
	$res = @include $_SERVER["CONTEXT_DOCUMENT_ROOT"] . "/main.inc.php";
}

// Try main.inc.php into web root detected using web root calculated from SCRIPT_FILENAME
$tmp = empty($_SERVER['SCRIPT_FILENAME']) ? '' : $_SERVER['SCRIPT_FILENAME'];
$tmp2 = realpath(__FILE__);
$i = strlen($tmp) - 1;
$j = strlen($tmp2) - 1;
while ($i > 0 && $j > 0 && isset($tmp[$i]) && isset($tmp2[$j]) && $tmp[$i] == $tmp2[$j]) {
	$i--;
	$j--;
}
if (!$res && $i > 0 && file_exists(substr($tmp, 0, ($i + 1)) . "/main.inc.php")) {
	$res = @include substr($tmp, 0, ($i + 1)) . "/main.inc.php";
}

if (!$res && $i > 0 && file_exists(dirname(substr($tmp, 0, ($i + 1))) . "/main.inc.php")) {
	$res = @include dirname(substr($tmp, 0, ($i + 1))) . "/main.inc.php";
}

// Try main.inc.php using relative path
if (!$res && file_exists("../../main.inc.php")) {
	$res = @include "../../main.inc.php";
}

if (!$res && file_exists("../../../main.inc.php")) {
	$res = @include "../../../main.inc.php";
}

if (!$res) {
	die("Include of main fails");
}

global $langs, $user;

dol_include_once('/doliscan/core/modules/modDoliscan.class.php');
dol_include_once('/doliscan/lib/doliscan.lib.php');

$modDoliscan = new modDoliscan($db);

// Libraries
// require DOL_DOCUMENT_ROOT . '/includes/autoload.php';
require_once DOL_DOCUMENT_ROOT . "/core/lib/admin.lib.php";
require_once DOL_DOCUMENT_ROOT . '/expensereport/class/expensereport.class.php';

//dolibarr 13+
if (file_exists(DOL_DOCUMENT_ROOT . '/core/class/html.formexpensereport.class.php')) {
	require_once DOL_DOCUMENT_ROOT . '/core/class/html.formexpensereport.class.php';
}

//require_once "../class/myclass.class.php";

// Translations
$langs->loadLangs(array("admin", "doliscan@doliscan"));

// Access control
if (!$user->admin) {
	accessforbidden();
}

// Parameters
$action = GETPOST('action', 'alpha');
$backtopage = GETPOST('backtopage', 'alpha');

$value = GETPOST('value', 'alpha');

$error = 0;
$setupnotempty = 0;

//On recupere la liste des clés de config du serveur doliscan
$applicationKey = dsbackport_getDolGlobalString('DOLISCAN_MAINACCOUNT_APIKEY','');
$email = dsbackport_getDolGlobalString('DOLISCAN_MAINACCOUNT_EMAIL','');
$endpoint = dsbackport_getDolGlobalString('DOLISCAN_MAINSERVER','');

//Et les clés de config si on a déjà lancé ce module de configuration
// $doliscanConfig = json_decode(dsbackport_getDolGlobalString('DOLISCAN_GLOBAL_CONFIG',''));
$dolSlug = $dolFraisPerso = null;
if(dsbackport_getDolGlobalString('DOLISCAN_FRAISPERSO_SLUGS','') != '') {
	$dolSlug = json_decode(dsbackport_getDolGlobalString('DOLISCAN_FRAISPERSO_SLUGS','{}'));
}
if(dsbackport_getDolGlobalString('DOLISCAN_GLOBAL_FRAISPERSO','') != '') {
	$dolFraisPerso = json_decode(dsbackport_getDolGlobalString('DOLISCAN_GLOBAL_FRAISPERSO','{}'));
}

/*
 * Actions
 */

if ((float) DOL_VERSION >= 6) {
	include DOL_DOCUMENT_ROOT . '/core/actions_setmoduleoptions.inc.php';
}

// if ($action == 'setvalue' && $user->admin) {
// 	if ($result >= 0) {
// 		$mesg = '<div class="ok">' . $langs->trans("SetupSaved") . '</div>';
// 	} else {
// 		dol_print_error($db);
// 	}
// }

if ($action == 'update') {
	//On sauvegarde les associations slugPro doliscan <-> id fournisseur dolibarr
	//exemple $toSave[restauration] = 34;
	$toSave = array();
	foreach ($dolSlug as $slug => $label) {
		$dolkey = "DOLISCAN_PERSO_" . strtoupper($slug);
		$confValue = GETPOST($dolkey, 'aZ09');
		$toSave[$slug] = $confValue;
	}
	dol_syslog("DoliSCAN: DOLISCAN_GLOBAL_FRAISPERSO " . json_encode($toSave));
	$result = dolibarr_set_const($db, "DOLISCAN_GLOBAL_FRAISPERSO", json_encode($toSave), 'chaine', 0, '', $conf->entity);
	//Et on actualise la variable pour que l'affichage soit ok
	$dolFraisPerso = json_decode(dsbackport_getDolGlobalString('DOLISCAN_GLOBAL_FRAISPERSO','{}'));
	$mesg = '<div class="ok">' . $langs->trans("SetupSaved") . '</div>';
}

/*
 * View
 */

$form = new Form($db);

$dirmodels = array_merge(array('/'), (array) $conf->modules_parts['models']);

$page_name = "DoliscanSetup";
llxHeader('', $langs->trans($page_name));

// Subheader
$linkback = '<a href="' . ($backtopage ? $backtopage : DOL_URL_ROOT . '/admin/modules.php?restore_lastsearch_values=1') . '">' . $langs->trans("BackToModuleList") . '</a>';

print load_fiche_titre($langs->trans($page_name), $linkback, 'object_doliscan@doliscan');

// Configuration header
$head = doliscanAdminPrepareHead();
dol_fiche_head($head, 'configurationperso', '', -1, "doliscan@doliscan");

// Setup page goes here
echo '<span class="opacitymedium">' . $langs->trans("DoliscanConfigPage") . "<br />" . $langs->trans("DoliscanConfigPagePerso") . '</span><br><br>';

if ($action == 'edit') {
	//Note: On re-telecharge la liste des fois qu'on ait une modif côté serveur (c'est le cas le 8/04/2021 pour le carburant)
	$dolSlug = array();
	// if ($dolSlug == "") {
	dol_syslog("DoliSCAN: Download configuration keys from doliscan ...");
	$param = [ 'email' => $email ];
	$result = getURLContent($endpoint . '/api/config/typeFraisPerso', 'GET', json_encode($param), 1, doliSCANApiCommonHeader($applicationKey), ['http','https'], 2);
	if (isset($result['curl_error_no'])) {
		handleDoliscanTimeoutBlacklist($result);
	}

	if (is_array($result) && $result['http_code'] == 200 && isset($result['content'])) {
		$data = json_decode($result['content']);
		// dol_syslog("DoliSCAN: Request response body :  " . json_encode($data));
		$mesg = '<div class="ok">' . $langs->trans("GetTypeFraisPersoOK") . '</div>';
		foreach ($data as $value) {
			$dolSlug[$value->slug] = $value->label;
		}
	} else {
		$error++;
		$mesg = '<div class="error">' . $langs->trans("GetTypeFraisPersoError") . '</div>';
		setEventMessages($mesg, [], 'errors');
	}
	$action = '';
	dol_syslog("DoliSCAN: end of download... ");

	// }
	print '<form method="POST" action="' . $_SERVER["PHP_SELF"] . '">';
	print '<input type="hidden" name="token" value="' . newToken() . '">';
	print '<input type="hidden" name="action" value="update">';

	print '<table class="noborder centpercent">';
	print '<tr class="liste_titre"><td class="titlefield">' . $langs->trans("DoliSCAN") . '</td><td>' . $langs->trans("Dolibarr") . '</td></tr>';

	// pour plus tard quand on pourra factoriser
	// if ($dolSlug == "") {
	//     $dolSlug = array();
	// }

	dol_syslog("DoliSCAN: DolSLUG = " . json_encode($dolSlug));
	foreach ($dolSlug as $slug => $label) {
		print '<tr class="oddeven"><td>';
		// $tooltiphelp = (($langs->trans($key . 'Tooltip') != $key . 'Tooltip') ? $langs->trans($key . 'Tooltip') : '');
		// print $form->textwithpicto($langs->trans($key), $tooltiphelp);
		// print '</td><td><input name="' . $key . '"  class="flat ' . (empty($val['css']) ? 'minwidth200' : $val['css']) . '" value="' . ($conf->global->$key ?? '') . '"></td></tr>';
		print $label;
		print '</td><td>';
		//La liste des fournisseurs
		$dolkey = "DOLISCAN_PERSO_" . strtoupper($slug);
		$id = $dolFraisPerso->$slug;
		//dolibarr < 13
		if (function_exists('select_type_fees_id')) {
			select_type_fees_id($id, $dolkey, 1);
		} else {
			//dolibarr 13+
			$formexpensereport = new FormExpenseReport($db);
			print              $formexpensereport->selectTypeExpenseReport($id, $dolkey, 1);
		}
		print '</td></tr>';
	}

	$result = dolibarr_set_const($db, "DOLISCAN_FRAISPERSO_SLUGS", json_encode($dolSlug), 'chaine', 0, '', $conf->entity);
	print '</table>';

	print '<br><div class="center">';
	print '<input class="button" type="submit" value="' . $langs->trans("Save") . '">';
	print '</div>';

	print '</form>';
	print '<br>';
} else {
	if (!empty($dolSlug)) {
		print '<table class="noborder centpercent">';
		print '<tr class="liste_titre"><td class="titlefield">' . $langs->trans("DoliSCAN") . '</td><td>' . $langs->trans("Dolibarr") . '</td></tr>';

		foreach ($dolSlug as $slug => $label) {
			// dol_syslog("DoliSCAN: DolSLUG 2 = " . $label . " || " . $slug);
			$id = $dolFraisPerso->$slug;
			print '<tr class="oddeven">';
			print "<td>$label</td>";
			print '<td>' . display_frais($id) . '</td></tr>';
			$setupnotempty++;
		}

		print '</table>';

		print '<div class="tabsAction">';

		// if (dsbackport_getDolGlobalString('DOLISCAN_MAINACCOUNT_APIKEY','')) {
		//     print '<a class="butAction" href="' . $_SERVER["PHP_SELF"] . '?action=checkConnect">' . $langs->trans("CheckConnectToDoliSCAN") . '</a>';
		// } else {
		//     print '<a class="butAction" href="' . $_SERVER["PHP_SELF"] . '?action=connect">' . $langs->trans("NewAccountDoliSCAN") . '</a>';
		// }
		print '<a class="butAction" href="' . $_SERVER["PHP_SELF"] . '?action=edit">' . $langs->trans("Modify") . '</a>';

		print '</div>';
	} else {
		print '<a class="butAction" href="' . $_SERVER["PHP_SELF"] . '?action=edit">' . $langs->trans("Initialiser") . '</a>';
	}
}

// Page end
dol_fiche_end();

if(isset($mesg)) {
	dol_htmloutput_mesg($mesg);
}

llxFooter();
$db->close();
