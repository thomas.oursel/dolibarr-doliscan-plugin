<?php
/* Copyright (C) 2004-2017 Laurent Destailleur  <eldy@users.sourceforge.net>
 * Copyright (C) 2023 Éric Seigne <eric.seigne@cap-rel.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file    doliscan/admin/setup.php
 * \ingroup doliscan
 * \brief   Doliscan setup page.
 */
// require_once __DIR__ . '/../vendor/autoload.php';

// Load Dolibarr environment
$res = 0;
// Try main.inc.php into web root known defined into CONTEXT_DOCUMENT_ROOT (not always defined)
if (!$res && !empty($_SERVER["CONTEXT_DOCUMENT_ROOT"])) {
	$res = @include $_SERVER["CONTEXT_DOCUMENT_ROOT"] . "/main.inc.php";
}

// Try main.inc.php into web root detected using web root calculated from SCRIPT_FILENAME
$tmp = empty($_SERVER['SCRIPT_FILENAME']) ? '' : $_SERVER['SCRIPT_FILENAME'];
$tmp2 = realpath(__FILE__);
$i = strlen($tmp) - 1;
$j = strlen($tmp2) - 1;
while ($i > 0 && $j > 0 && isset($tmp[$i]) && isset($tmp2[$j]) && $tmp[$i] == $tmp2[$j]) {
	$i--;
	$j--;
}
if (!$res && $i > 0 && file_exists(substr($tmp, 0, ($i + 1)) . "/main.inc.php")) {
	$res = @include substr($tmp, 0, ($i + 1)) . "/main.inc.php";
}

if (!$res && $i > 0 && file_exists(dirname(substr($tmp, 0, ($i + 1))) . "/main.inc.php")) {
	$res = @include dirname(substr($tmp, 0, ($i + 1))) . "/main.inc.php";
}

// Try main.inc.php using relative path
if (!$res && file_exists("../../main.inc.php")) {
	$res = @include "../../main.inc.php";
}

if (!$res && file_exists("../../../main.inc.php")) {
	$res = @include "../../../main.inc.php";
}

if (!$res) {
	die("Include of main fails");
}

global $langs, $user;

// Libraries
// require DOL_DOCUMENT_ROOT . '/includes/autoload.php';
require_once DOL_DOCUMENT_ROOT . "/core/lib/admin.lib.php";
require_once DOL_DOCUMENT_ROOT . '/compta/bank/class/account.class.php';
//require_once "../class/myclass.class.php";

dol_include_once('/doliscan/core/modules/modDoliscan.class.php');
dol_include_once('/doliscan/lib/doliscan.lib.php');

$modDoliscan = new modDoliscan($db);


// Translations
$langs->loadLangs(array("admin", "doliscan@doliscan"));

// Access control
if (!$user->admin) {
	accessforbidden();
}


// Parameters
$action = GETPOST('action', 'alpha');
$backtopage = GETPOST('backtopage', 'alpha');

$value = GETPOST('value', 'alpha');

$error = 0;
$setupnotempty = 0;

//On recupere la liste des clés de config du serveur doliscan
$applicationKey = dsbackport_getDolGlobalString('DOLISCAN_MAINACCOUNT_APIKEY','');
$email = dsbackport_getDolGlobalString('DOLISCAN_MAINACCOUNT_EMAIL','');
$endpoint = dsbackport_getDolGlobalString('DOLISCAN_MAINSERVER','');

//Et les clés de config si on a déjà lancé ce module de configuration
// $doliscanConfig = json_decode(dsbackport_getDolGlobalString('DOLISCAN_GLOBAL_CONFIG',''));
$dolSlug = $dolBanque = [];
if(dsbackport_getDolGlobalString('DOLISCAN_BANQUE_SLUGS','') != '') {
	$dolSlug = json_decode(dsbackport_getDolGlobalString('DOLISCAN_BANQUE_SLUGS','{}'));
}
if(dsbackport_getDolGlobalString('DOLISCAN_GLOBAL_BANQUE','') != '') {
	$dolBanque = json_decode(dsbackport_getDolGlobalString('DOLISCAN_GLOBAL_BANQUE','{}'));
}

/*
 * Actions
 */

if ((float) DOL_VERSION >= 6) {
	include DOL_DOCUMENT_ROOT . '/core/actions_setmoduleoptions.inc.php';
}

// if ($action == 'setvalue' && $user->admin) {
// 	if ($result >= 0) {
// 		$mesg = '<div class="ok">' . $langs->trans("SetupSaved") . '</div>';
// 	} else {
// 		dol_print_error($db);
// 	}
// }

if ($action == 'update') {
	//On sauvegarde les associations slugPro doliscan <-> id fournisseur dolibarr
	//exemple $toSave[restauration] = 34;
	$toSave = array();
	//Attention on a un objet un peu plus complexe que jusqu'à présent
	foreach ($dolSlug as $slug => $label) {
		$obj = new stdClass;
		$dolkey = "DOLISCAN_BANQUE_COMPTE_" . strtoupper($slug);
		$confValue = GETPOST($dolkey, 'aZ09');
		$obj->idbanque = $confValue;

		$dolkey = "DOLISCAN_BANQUE_PAIEMENT_" . strtoupper($slug);
		$confValue = GETPOST($dolkey, 'aZ09');
		$obj->idpaiement = $confValue;

		$toSave[$slug] = $obj;
	}
	// dol_syslog("DoliSCAN: On sauvegarde :  " . json_encode($toSave));

	$result = dolibarr_set_const($db, "DOLISCAN_GLOBAL_BANQUE", json_encode($toSave), 'chaine', 0, '', $conf->entity);
	//Et on actualise la variable pour que l'affichage soit ok
	$dolBanque = json_decode(dsbackport_getDolGlobalString('DOLISCAN_GLOBAL_BANQUE','{}'));
	$mesg = '<div class="ok">' . $langs->trans("SetupSaved") . '</div>';
}

/*
 * View
 */

$form = new Form($db);

$dirmodels = array_merge(array('/'), (array) $conf->modules_parts['models']);

$page_name = "DoliscanSetup";
llxHeader('', $langs->trans($page_name));

// Subheader
$linkback = '<a href="' . ($backtopage ? $backtopage : DOL_URL_ROOT . '/admin/modules.php?restore_lastsearch_values=1') . '">' . $langs->trans("BackToModuleList") . '</a>';

print load_fiche_titre($langs->trans($page_name), $linkback, 'object_doliscan@doliscan');

// Configuration header
$head = doliscanAdminPrepareHead();
dol_fiche_head($head, 'configurationtrigger', '', -1, "doliscan@doliscan");

// Setup page goes here
echo '<span class="opacitymedium">' . $langs->trans("DoliscanConfigPageTriggers") . '</span><br><br>';

print '<table class="noborder centpercent">';
print '<tr class="oddeven">';
print '<td>'.$langs->trans("doliscanAutoCreateUsers").'</td>';
print '<td align="center" width="20">&nbsp;</td>';

print '<td align="center" width="100">';
if ($conf->use_javascript_ajax) {
	print ajax_constantonoff('DOLISCAN_AUTO_CREATE_NEW_USER');
} else {
	if (dsbackport_getDolGlobalString('DOLISCAN_AUTO_CREATE_NEW_USER','') == '') {
		print '<a href="'.$_SERVER['PHP_SELF'].'?action=set_DOLISCAN_AUTO_CREATE_NEW_USER">'.img_picto($langs->trans("Disabled"), 'off').'</a>';
	} else {
		print '<a href="'.$_SERVER['PHP_SELF'].'?action=del_DOLISCAN_AUTO_CREATE_NEW_USER">'.img_picto($langs->trans("Enabled"), 'on').'</a>';
	}
}
print '</td></tr>';


print '<tr class="oddeven">';
print '<td>'.$langs->trans("doliscanEnabledForExternalUsers").'</td>';
print '<td align="center" width="20">&nbsp;</td>';
print '<td align="center" width="100">';
if ($conf->use_javascript_ajax) {
	print ajax_constantonoff('DOLISCAN_ENABLED_FOR_EXTERNAL_USERS');
} else {
	if (dsbackport_getDolGlobalString('DOLISCAN_ENABLED_FOR_EXTERNAL_USERS','') == '') {
		print '<a href="'.$_SERVER['PHP_SELF'].'?action=set_DOLISCAN_ENABLED_FOR_EXTERNAL_USERS">'.img_picto($langs->trans("Disabled"), 'off').'</a>';
	} else {
		print '<a href="'.$_SERVER['PHP_SELF'].'?action=del_DOLISCAN_ENABLED_FOR_EXTERNAL_USERS">'.img_picto($langs->trans("Enabled"), 'on').'</a>';
	}
}
print '</td></tr>';


print '<tr class="oddeven">';
print '<td>'.$langs->trans("doliscanAutoInvoicePaimentEnables").'</td>';
print '<td align="center" width="20">&nbsp;</td>';

print '<td align="center" width="100">';
if ($conf->use_javascript_ajax) {
	print ajax_constantonoff('DOLISCAN_PRO_AUTO_PAIEMENT_ENABLED');
} else {
	if (dsbackport_getDolGlobalString('DOLISCAN_PRO_AUTO_PAIEMENT_ENABLED','') == '') {
		print '<a href="'.$_SERVER['PHP_SELF'].'?action=set_DOLISCAN_PRO_AUTO_PAIEMENT_ENABLED">'.img_picto($langs->trans("Disabled"), 'off').'</a>';
	} else {
		print '<a href="'.$_SERVER['PHP_SELF'].'?action=del_DOLISCAN_PRO_AUTO_PAIEMENT_ENABLED">'.img_picto($langs->trans("Enabled"), 'on').'</a>';
	}
}
print '</td></tr>';


print '<tr class="oddeven">';
print '<td>'.$langs->trans("doliscanAutoInvoiceProductsFromScanInvoicesSettings").'</td>';
print '<td align="center" width="20">&nbsp;</td>';

print '<td align="center" width="100">';
if ($conf->use_javascript_ajax) {
	print ajax_constantonoff('DOLISCAN_USE_DEFAULT_PRODUCT_FROM_SCANINVOICES');
} else {
	if (dsbackport_getDolGlobalString('DOLISCAN_USE_DEFAULT_PRODUCT_FROM_SCANINVOICES','') == '') {
		print '<a href="'.$_SERVER['PHP_SELF'].'?action=set_DOLISCAN_USE_DEFAULT_PRODUCT_FROM_SCANINVOICES">'.img_picto($langs->trans("Disabled"), 'off').'</a>';
	} else {
		print '<a href="'.$_SERVER['PHP_SELF'].'?action=del_DOLISCAN_USE_DEFAULT_PRODUCT_FROM_SCANINVOICES">'.img_picto($langs->trans("Enabled"), 'on').'</a>';
	}
}
print '</td></tr>';


print '<tr class="oddeven">';
print '<td>'.$langs->trans("doliscanProjectTagFromLabel").'</td>';
print '<td align="center" width="20">&nbsp;</td>';

print '<td align="center" width="100">';
if ($conf->use_javascript_ajax) {
	print ajax_constantonoff('DOLISCAN_USE_PROJECT_LABEL');
} else {
	if (dsbackport_getDolGlobalString('DOLISCAN_USE_PROJECT_LABEL','') == '') {
		print '<a href="'.$_SERVER['PHP_SELF'].'?action=set_DOLISCAN_USE_PROJECT_LABEL">'.img_picto($langs->trans("Disabled"), 'off').'</a>';
	} else {
		print '<a href="'.$_SERVER['PHP_SELF'].'?action=del_DOLISCAN_USE_PROJECT_LABEL">'.img_picto($langs->trans("Enabled"), 'on').'</a>';
	}
}
print '</td></tr>';


print '<tr class="oddeven">';
print '<td>'.$langs->trans("doliscanProjectHideClosed").'</td>';
print '<td align="center" width="20">&nbsp;</td>';

print '<td align="center" width="100">';
if ($conf->use_javascript_ajax) {
	print ajax_constantonoff('DOLISCAN_HIDE_CLOSED_PROJECTS');
} else {
	if (dsbackport_getDolGlobalString('DOLISCAN_HIDE_CLOSED_PROJECTS','') == '') {
		print '<a href="'.$_SERVER['PHP_SELF'].'?action=set_DOLISCAN_HIDE_CLOSED_PROJECTS">'.img_picto($langs->trans("Disabled"), 'off').'</a>';
	} else {
		print '<a href="'.$_SERVER['PHP_SELF'].'?action=del_DOLISCAN_HIDE_CLOSED_PROJECTS">'.img_picto($langs->trans("Enabled"), 'on').'</a>';
	}
}
print '</td></tr>';

print '<tr class="oddeven">';
print '<td>'.$langs->trans("doliscanRefreshDraft").'</td>';
print '<td align="center" width="20">&nbsp;</td>';
print '<td align="center" width="100">';
if ($conf->use_javascript_ajax) {
    print ajax_constantonoff('DOLISCAN_REFRESH_DRAFT');
} else {
    if (dsbackport_getDolGlobalString('DOLISCAN_REFRESH_DRAFT','') == '') {
        print '<a href="'.$_SERVER['PHP_SELF'].'?action=set_DOLISCAN_REFRESH_DRAFT">'.img_picto($langs->trans("Disabled"), 'off').'</a>';
    } else {
        print '<a href="'.$_SERVER['PHP_SELF'].'?action=del_DOLISCAN_REFRESH_DRAFT">'.img_picto($langs->trans("Enabled"), 'on').'</a>';
    }
}
print '</td></tr>';

// Page end
dol_fiche_end();

if(isset($mesg)) {
	dol_htmloutput_mesg($mesg);
}

llxFooter();
$db->close();
