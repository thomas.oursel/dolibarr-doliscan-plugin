# DOLISCAN POUR [DOLIBARR ERP CRM](https://www.dolibarr.org)

## Fonctionnalités

DoliSCAN est un assistant de saisie dédié aux notes de Frais. Il est composé d'une application à installer sur votre Smartphone et d'un service en ligne lequel est connecté à votre dolibarr par le biais de ce module (plugin).

Ce module est totalement documenté sur le [site doliscan.fr](https://www.doliscan.fr/fr/plugins/dolibarr)

<!--
![Screenshot doliscan](img/screenshot_doliscan.png?raw=true "Doliscan"){imgmd}
-->

## Traductions


La plate-forme weblate héberge maintenant les traductions de ce module : https://hosted.weblate.org/projects/doliscan/

## Installation

### À partir d'un fichier ZIP et de l'interface

- Ouvrez votre navigateur et authentifiez vous dans dolibarr avec le compte administrateur, puis allez sur Accueil > Configuration > Modules/Applications > Déployer/Installer un module externe

En cas de problème d'installation vérifiez les points suivants:

- Dans le répertoire d'installation de votre Dolibarr, modifiez le fichier ```htdocs/conf/conf.php``` et vérifiez que les deux lignes suivantes ne sont pas en commentaire:

    ```php
    //$dolibarr_main_url_root_alt ...
    //$dolibarr_main_document_root_alt ...
    ```

- Dé-commentez-les si nécessaire (en supprimant les ```//``` de début de ligne) et affectez les bonnes valeurs correspondantes à votre installation de Dolibarr

    Par exemple :

    - UNIX:
        ```php
        $dolibarr_main_url_root_alt = '/custom';
        $dolibarr_main_document_root_alt = '/var/www/Dolibarr/htdocs/custom';
        ```

    - Windows:
        ```php
        $dolibarr_main_url_root_alt = '/custom';
        $dolibarr_main_document_root_alt = 'C:/My Web Sites/Dolibarr/htdocs/custom';
        ```

### À partir du dépôt de code source GIT:

Note: vérifiez les tags si vous voulez récupérer une version stable particulière ... et n'oubliez pas que prendre le gratuit en oubliant de financer le
libre n'est pas terrible pour la pérénité d'un projet ... vous voulez soutenir la r&d ça tombe bien nous avons un programme pour ça: https://cap-rel.fr/services/soutien-rd/

Le module est proposé sur DoliStore qui permet en plus de participer au financement du projet Dolibarr ... https://www.dolistore.com/fr/modules/1353-DoliSCAN---Notes-de-frais.html

### Dernière étape

À partir de votre navigateur web:

  - Connectez vous en tant qu'administrateur à votre Dolibarr
  - Allez sur la page Accueil > Configuration > Modules/Applications
  - Vous devriez voir le module DoliSCAN

## Licence

### Main code

Tout le code est sous licence GPLv3. Regardez le fichier COPYING pour plus d'informations.

### Documentation

Toute la documentation est sous licence CC-BY-SA. Vous la trouverez sur le [site doliscan.fr](https://www.doliscan.fr/fr/plugins/dolibarr)

