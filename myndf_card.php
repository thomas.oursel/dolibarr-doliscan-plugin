<?php
/* Copyright (C) 2017 Laurent Destailleur  <eldy@users.sourceforge.net>
 * Copyright (C) 2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

/**
 *       \file       myndf_card.php
 *        \ingroup    doliscan
 *        \brief      Page to create/edit/view myndf
 */

//if (! defined('NOREQUIREDB'))              define('NOREQUIREDB','1');                    // Do not create database handler $db
//if (! defined('NOREQUIREUSER'))            define('NOREQUIREUSER','1');                // Do not load object $user
//if (! defined('NOREQUIRESOC'))             define('NOREQUIRESOC','1');                // Do not load object $mysoc
//if (! defined('NOREQUIRETRAN'))            define('NOREQUIRETRAN','1');                // Do not load object $langs
//if (! defined('NOSCANGETFORINJECTION'))    define('NOSCANGETFORINJECTION','1');        // Do not check injection attack on GET parameters
//if (! defined('NOSCANPOSTFORINJECTION'))   define('NOSCANPOSTFORINJECTION','1');        // Do not check injection attack on POST parameters
//if (! defined('NOCSRFCHECK'))              define('NOCSRFCHECK','1');                    // Do not check CSRF attack (test on referer + on token if option MAIN_SECURITY_CSRF_WITH_TOKEN is on).
//if (! defined('NOTOKENRENEWAL'))           define('NOTOKENRENEWAL','1');                // Do not roll the Anti CSRF token (used if MAIN_SECURITY_CSRF_WITH_TOKEN is on)
//if (! defined('NOSTYLECHECK'))             define('NOSTYLECHECK','1');                // Do not check style html tag into posted data
//if (! defined('NOREQUIREMENU'))            define('NOREQUIREMENU','1');                // If there is no need to load and show top and left menu
//if (! defined('NOREQUIREHTML'))            define('NOREQUIREHTML','1');                // If we don't need to load the html.form.class.php
//if (! defined('NOREQUIREAJAX'))            define('NOREQUIREAJAX','1');                 // Do not load ajax.lib.php library
//if (! defined("NOLOGIN"))                  define("NOLOGIN",'1');                        // If this page is public (can be called outside logged session). This include the NOIPCHECK too.
//if (! defined('NOIPCHECK'))                define('NOIPCHECK','1');                    // Do not check IP defined into conf $dolibarr_main_restrict_ip
//if (! defined("MAIN_LANG_DEFAULT"))        define('MAIN_LANG_DEFAULT','auto');                    // Force lang to a particular value
//if (! defined("MAIN_AUTHENTICATION_MODE")) define('MAIN_AUTHENTICATION_MODE','aloginmodule');        // Force authentication handler
//if (! defined("NOREDIRECTBYMAINTOLOGIN"))  define('NOREDIRECTBYMAINTOLOGIN',1);        // The main.inc.php does not make a redirect if not logged, instead show simple error message
//if (! defined("FORCECSP"))                 define('FORCECSP','none');                    // Disable all Content Security Policies

// Load Dolibarr environment
$res = 0;
// Try main.inc.php into web root known defined into CONTEXT_DOCUMENT_ROOT (not always defined)
if (!$res && !empty($_SERVER["CONTEXT_DOCUMENT_ROOT"])) {
	$res = @include $_SERVER["CONTEXT_DOCUMENT_ROOT"] . "/main.inc.php";
}

// Try main.inc.php into web root detected using web root calculated from SCRIPT_FILENAME
$tmp = empty($_SERVER['SCRIPT_FILENAME']) ? '' : $_SERVER['SCRIPT_FILENAME'];
$tmp2 = realpath(__FILE__);
$i = strlen($tmp) - 1;
$j = strlen($tmp2) - 1;
while ($i > 0 && $j > 0 && isset($tmp[$i]) && isset($tmp2[$j]) && $tmp[$i] == $tmp2[$j]) {
	$i--;
	$j--;
}
if (!$res && $i > 0 && file_exists(substr($tmp, 0, ($i + 1)) . "/main.inc.php")) {
	$res = @include substr($tmp, 0, ($i + 1)) . "/main.inc.php";
}

if (!$res && $i > 0 && file_exists(dirname(substr($tmp, 0, ($i + 1))) . "/main.inc.php")) {
	$res = @include dirname(substr($tmp, 0, ($i + 1))) . "/main.inc.php";
}

// Try main.inc.php using relative path
if (!$res && file_exists("../main.inc.php")) {
	$res = @include "../main.inc.php";
}

if (!$res && file_exists("../../main.inc.php")) {
	$res = @include "../../main.inc.php";
}

if (!$res && file_exists("../../../main.inc.php")) {
	$res = @include "../../../main.inc.php";
}

if (!$res) {
	die("Include of main fails");
}

require_once DOL_DOCUMENT_ROOT . '/core/class/html.formcompany.class.php';
require_once DOL_DOCUMENT_ROOT . '/core/class/html.formfile.class.php';
require_once DOL_DOCUMENT_ROOT . '/core/class/html.formprojet.class.php';

dol_include_once('/doliscan/class/myndf.class.php');
dol_include_once('/doliscan/lib/doliscan_myndf.lib.php');
dol_include_once('/doliscan/lib/doliscan.lib.php');

// load doliscan libraries
dol_include_once('/doliscan/class/myaccount.class.php');
// require_once __DIR__ . '/vendor/autoload.php';

// Load translation files required by the page
$langs->loadLangs(array("doliscan@doliscan", "other"));

// Get parameters
$id = GETPOSTINT('id');
$ref = GETPOST('ref', 'alpha');
$action = GETPOST('action', 'aZ09');
$confirm = GETPOST('confirm', 'alpha');
$cancel = GETPOST('cancel', 'aZ09');
$contextpage = GETPOST('contextpage', 'aZ') ? GETPOST('contextpage', 'aZ') : 'myndfcard'; // To manage different context of search
$backtopage = GETPOST('backtopage', 'alpha');
$backtopageforcancel = GETPOST('backtopageforcancel', 'alpha');
//$lineid   = GETPOST('lineid', 'int');

// Initialize technical objects
$object = new MyNDF($db);
$extrafields = new ExtraFields($db);
$diroutputmassaction = $conf->doliscan->dir_output . '/temp/massgeneration/' . $user->id;
$hookmanager->initHooks(array('myndfcard', 'globalcard')); // Note that conf->hooks_modules contains array

// Fetch optionals attributes and labels
$extrafields->fetch_name_optionals_label($object->table_element);

$search_array_options = $extrafields->getOptionalsFromPost($object->table_element, '', 'search_');

// Initialize array of search criterias
$search_all = trim(GETPOST("search_all", 'alpha'));
$search = array();
foreach ($object->fields as $key => $val) {
	if (GETPOST('search_' . $key, 'alpha')) {
		$search[$key] = GETPOST('search_' . $key, 'alpha');
	}
}

if (empty($action) && empty($id) && empty($ref)) {
	$action = 'view';
}

// Load object
include DOL_DOCUMENT_ROOT . '/core/actions_fetchobject.inc.php'; // Must be include, not include_once.
require_once DOL_DOCUMENT_ROOT . '/expensereport/class/expensereport.class.php';

$permissiontoread = true; //$user->rights->doliscan->myndf->read;
$permissiontoadd = false; //$user->rights->doliscan->myndf->write; // Used by the include of actions_addupdatedelete.inc.php and actions_lineupdown.inc.php
$permissiontodelete = (isset($object->status) && $object->status == $object::STATUS_DRAFT);
$permissionnote = false; //$user->rights->doliscan->myndf->write; // Used by the include of actions_setnotes.inc.php
$permissiondellink = true; //$user->rights->doliscan->myndf->write; // Used by the include of actions_dellink.inc.php
$upload_dir = $conf->doliscan->multidir_output[isset($object->entity) ? $object->entity : 1];

// Security check - Protection if external user
//if ($user->socid > 0) accessforbidden();
//if ($user->socid > 0) $socid = $user->socid;
//$isdraft = (($object->statut == $object::STATUS_DRAFT) ? 1 : 0);
//$result = restrictedArea($user, 'doliscan', $object->id, '', '', 'fk_soc', 'rowid', $isdraft);

//if (!$permissiontoread) accessforbidden();
//On recupere la liste des clés de config du serveur doliscan
$email = dsbackport_getDolGlobalString('DOLISCAN_MAINACCOUNT_EMAIL','');
$endpoint = dsbackport_getDolGlobalString('DOLISCAN_MAINSERVER','');
// $doliscanConfig = json_decode(dsbackport_getDolGlobalString('DOLISCAN_GLOBAL_CONFIG',''));
// $dolSlug = json_decode(dsbackport_getDolGlobalString('DOLISCAN_FRAISPRO_SLUGS',''));
// $dolFraisPro = json_decode(dsbackport_getDolGlobalString('DOLISCAN_GLOBAL_FRAISPRO',''));

$account = new MyAccount($db);
$accountRes = $account->fetchFirst($user->id);
$userLogin = $accountRes->ds_login;
$userAPI = $accountRes->ds_api;


/*
 * Actions
 */

$parameters = array();
$reshook = $hookmanager->executeHooks('doActions', $parameters, $object, $action); // Note that $action and $object may have been modified by some hooks
if ($reshook < 0) {
	setEventMessages($hookmanager->error, $hookmanager->errors, 'errors');
}

if (GETPOST('action') == "getDetails") {
}

//Importer la note de frais ... on lance le festival !
if (GETPOST('action') == "import") {
	$u = $user;
	//mais comme on peut eventuellement importer la ndf d'un autre utilisateur ... on utilise le fk_user de la ndf
	if ($user->admin) {
		$ndf = new MyNDF($db);
		$ndf->fetch($id);
		$u = new User($db);
		$u->fetch($ndf->fk_user);
	}

	$html = importNDF($id, $u);

	$title = $langs->trans("MyNDF");
	$help_url = '';
	llxHeader('', $title, $help_url);

	$head = myndfPrepareHead($object);
	dol_fiche_head("$head", 'card', $langs->trans("MyNDF"), -1, $object->picto);
	$linkback = '<a href="' . dol_buildpath('/doliscan/myndf_list.php', 1) . '?restore_lastsearch_values=1' . (!empty($socid) ? '&socid=' . $socid : '') . '">' . $langs->trans("BackToList") . '</a>';

	dol_banner_tab($object, 'ref', $linkback, 1, 'ref', 'ref', '');

	print $html;

	print '<div class="clearboth"></div>';

	dol_fiche_end();
	llxFooter();
	$db->close();
	exit;
}

if (empty($reshook)) {
	$error = 0;

	$backurlforlist = dol_buildpath('/doliscan/myndf_list.php', 1);

	if (empty($backtopage) || ($cancel && empty($id))) {
		if (empty($backtopage) || ($cancel && strpos($backtopage, '__ID__'))) {
			if (empty($id) && (($action != 'add' && $action != 'create') || $cancel)) {
				$backtopage = $backurlforlist;
			} else {
				$backtopage = dol_buildpath('/doliscan/myndf_card.php', 1) . '?id=' . ($id > 0 ? $id : '__ID__');
			}
		}
	}
	$triggermodname = 'DOLISCAN_MYNDF_MODIFY'; // Name of trigger action code to execute when we modify record

	// Actions cancel, add, update, update_extras, confirm_validate, confirm_delete, confirm_deleteline, confirm_clone, confirm_close, confirm_setdraft, confirm_reopen
	include DOL_DOCUMENT_ROOT . '/core/actions_addupdatedelete.inc.php';

	// Actions when linking object each other
	include DOL_DOCUMENT_ROOT . '/core/actions_dellink.inc.php';

	// Actions when printing a doc from card
	include DOL_DOCUMENT_ROOT . '/core/actions_printing.inc.php';

	// Action to move up and down lines of object
	//include DOL_DOCUMENT_ROOT.'/core/actions_lineupdown.inc.php';

	// Action to build doc
	include DOL_DOCUMENT_ROOT . '/core/actions_builddoc.inc.php';

	// Actions to send emails
	$triggersendname = 'MYNDF_SENTBYMAIL';
	$autocopy = 'MAIN_MAIL_AUTOCOPY_MYNDF_TO';
	$trackid = 'myndf' . $object->id;
	include DOL_DOCUMENT_ROOT . '/core/actions_sendmails.inc.php';
}

/*
 * View
 *
 * Put here all code to build page
 */

$form = new Form($db);
$formfile = new FormFile($db);
$formproject = new FormProjets($db);

$title = $langs->trans("MyNDF");
$help_url = '';
llxHeader('', $title, $help_url);

// Example : Adding jquery code
print '<script type="text/javascript" language="javascript">
jQuery(document).ready(function() {
	function init_myfunc()
	{
		jQuery("#myid").removeAttr(\'disabled\');
		jQuery("#myid").attr(\'disabled\',\'disabled\');
	}
	init_myfunc();
	jQuery("#mybutton").click(function() {
		init_myfunc();
	});
});
</script>';

// Part to create
if ($action == 'create') {
	print load_fiche_titre($langs->trans("NewObject", $langs->transnoentitiesnoconv("MyNDF")), '', 'object_' . $object->picto);

	print '<form method="POST" action="' . $_SERVER["PHP_SELF"] . '">';
	print '<input type="hidden" name="token" value="' . newToken() . '">';
	print '<input type="hidden" name="action" value="add">';
	if ($backtopage) {
		print '<input type="hidden" name="backtopage" value="' . $backtopage . '">';
	}

	if ($backtopageforcancel) {
		print '<input type="hidden" name="backtopageforcancel" value="' . $backtopageforcancel . '">';
	}

	dol_fiche_head(array(), '');

	// Set some default values
	//if (! GETPOSTISSET('fieldname')) $_POST['fieldname'] = 'myvalue';

	print '<table class="border centpercent tableforfieldcreate">' . "\n";

	// Common attributes
	include DOL_DOCUMENT_ROOT . '/core/tpl/commonfields_add.tpl.php';

	// Other attributes
	include DOL_DOCUMENT_ROOT . '/core/tpl/extrafields_add.tpl.php';

	print '</table>' . "\n";

	dol_fiche_end();

	print '<div class="center">';
	print '<input type="submit" class="button" name="add" value="' . dol_escape_htmltag($langs->trans("Create")) . '">';
	print '&nbsp; ';
	print '<input type="' . ($backtopage ? "submit" : "button") . '" class="button" name="cancel" value="' . dol_escape_htmltag($langs->trans("Cancel")) . '"' . ($backtopage ? '' : ' onclick="javascript:history.go(-1)"') . '>'; // Cancel for create does not post form if we don't know the backtopage
	print '</div>';

	print '</form>';

	//dol_set_focus('input[name="ref"]');
}

// Part to edit record
if (($id || $ref) && $action == 'edit') {
	print load_fiche_titre($langs->trans("MyNDF"), '', 'object_' . $object->picto);

	print '<form method="POST" action="' . $_SERVER["PHP_SELF"] . '">';
	print '<input type="hidden" name="token" value="' . newToken() . '">';
	print '<input type="hidden" name="action" value="update">';
	print '<input type="hidden" name="id" value="' . $object->id . '">';
	if ($backtopage) {
		print '<input type="hidden" name="backtopage" value="' . $backtopage . '">';
	}

	if ($backtopageforcancel) {
		print '<input type="hidden" name="backtopageforcancel" value="' . $backtopageforcancel . '">';
	}

	dol_fiche_head();

	print '<table class="border centpercent tableforfieldedit">' . "\n";

	// Common attributes
	include DOL_DOCUMENT_ROOT . '/core/tpl/commonfields_edit.tpl.php';

	// Other attributes
	include DOL_DOCUMENT_ROOT . '/core/tpl/extrafields_edit.tpl.php';

	print '</table>';

	dol_fiche_end();

	print '<div class="center"><input type="submit" class="button" name="save" value="' . $langs->trans("Save") . '">';
	print ' &nbsp; <input type="submit" class="button" name="cancel" value="' . $langs->trans("Cancel") . '">';
	print '</div>';

	print '</form>';
}

// Part to show record
if ($object->id > 0 && (empty($action) || ($action != 'edit' && $action != 'create'))) {
	// $res = $object->fetch_optionals();

	$head = myndfPrepareHead($object);
	dol_fiche_head("$head", 'card', $langs->trans("MyNDF"), -1, $object->picto);

	$formconfirm = '';

	// Confirmation to delete
	if ($action == 'delete') {
		$formconfirm = $form->formconfirm($_SERVER["PHP_SELF"] . '?id=' . $object->id, $langs->trans('DeleteMyNDF'), $langs->trans('ConfirmDeleteObject'), 'confirm_delete', '', 0, 1);
	}
	// Confirmation to delete line
	// if ($action == 'deleteline') {
	// 	$formconfirm = $form->formconfirm($_SERVER["PHP_SELF"] . '?id=' . $object->id . '&lineid=' . $lineid, $langs->trans('DeleteLine'), $langs->trans('ConfirmDeleteLine'), 'confirm_deleteline', '', 0, 1);
	// }
	// Clone confirmation
	// if ($action == 'clone') {
	// 	// Create an array for form
	// 	$formquestion = array();
	// 	$formconfirm = $form->formconfirm($_SERVER["PHP_SELF"] . '?id=' . $object->id, $langs->trans('ToClone'), $langs->trans('ConfirmCloneAsk', $object->ref), 'confirm_clone', $formquestion, 'yes', 1);
	// }

	// Confirmation of action xxxx
	// if ($action == 'xxx') {
	// 	$formquestion = array();
	// 	/*
	// 	$forcecombo=0;
	// 	if ($conf->browser->name == 'ie') $forcecombo = 1;    // There is a bug in IE10 that make combo inside popup crazy
	// 	$formquestion = array(
	// 	// 'text' => $langs->trans("ConfirmClone"),
	// 	// array('type' => 'checkbox', 'name' => 'clone_content', 'label' => $langs->trans("CloneMainAttributes"), 'value' => 1),
	// 	// array('type' => 'checkbox', 'name' => 'update_prices', 'label' => $langs->trans("PuttingPricesUpToDate"), 'value' => 1),
	// 	// array('type' => 'other',    'name' => 'idwarehouse',   'label' => $langs->trans("SelectWarehouseForStockDecrease"), 'value' => $formproduct->selectWarehouses(GETPOST('idwarehouse')?GETPOST('idwarehouse'):'ifone', 'idwarehouse', '', 1, 0, 0, '', 0, $forcecombo))
	// 	);
	// 	 */
	// 	$formconfirm = $form->formconfirm($_SERVER["PHP_SELF"] . '?id=' . $object->id, $langs->trans('XXX'), $text, 'confirm_xxx', $formquestion, 0, 1, 220);
	// }

	// Call Hook formConfirm
	// $parameters = array('formConfirm' => $formconfirm, 'lineid' => $lineid);
	// $reshook = $hookmanager->executeHooks('formConfirm', $parameters, $object, $action); // Note that $action and $object may have been modified by hook
	// if (empty($reshook)) {
	// 	$formconfirm .= $hookmanager->resPrint;
	// } elseif ($reshook > 0) {
	// 	$formconfirm = $hookmanager->resPrint;
	// }

	// Print form confirm
	print $formconfirm;

	// Object card
	// ------------------------------------------------------------
	$linkback = '<a href="' . dol_buildpath('/doliscan/myndf_list.php', 1) . '?restore_lastsearch_values=1' . (!empty($socid) ? '&socid=' . $socid : '') . '">' . $langs->trans("BackToList") . '</a>';

	$morehtmlref = '<div class="refidno">';
	$morehtmlref .= '</div>';

	dol_banner_tab($object, 'ref', $linkback, 1, 'ref', 'ref', $morehtmlref);

	print '<div class="fichecenter">';
	// print '<div class="fichehalfleft">';
	print '<div class="underbanner clearboth"></div>';
	print '<table class="border centpercent">' . "\n";

	// Common attributes
	//$keyforbreak='fieldkeytoswitchonsecondcolumn';    // We change column just before this field
	include DOL_DOCUMENT_ROOT . '/core/tpl/commonfields_view.tpl.php';

	// Other attributes. Fields from hook formObjectOptions and Extrafields.
	include DOL_DOCUMENT_ROOT . '/core/tpl/extrafields_view.tpl.php';

	print '</table>';
	// print '</div>';
	print '</div>';

	print '<div class="clearboth"></div>';

	dol_fiche_end();

	/*
	 * Lines
	 */

	if (!empty($object->table_element_line)) {
		// Show object lines
		$result = $object->getLinesArray();

		print '	<form name="addproduct" id="addproduct" action="' . $_SERVER["PHP_SELF"] . '?id=' . $object->id . (($action != 'editline') ? '#addline' : '#line_' . GETPOST('lineid', 'int')) . '" method="POST">
		<input type="hidden" name="token" value="' . newToken() . '">
		<input type="hidden" name="action" value="' . (($action != 'editline') ? 'addline' : 'updateline') . '">
		<input type="hidden" name="mode" value="">
		<input type="hidden" name="id" value="' . $object->id . '">
		';

		if (!empty($conf->use_javascript_ajax) && $object->status == 0) {
			include DOL_DOCUMENT_ROOT . '/core/tpl/ajaxrow.tpl.php';
		}

		print '<div class="div-table-responsive-no-min">';
		if (!empty($object->lines) || ($object->status == $object::STATUS_DRAFT && $permissiontoadd && $action != 'selectlines' && $action != 'editline')) {
			print '<table id="tablelines" class="noborder noshadow" width="100%">';
		}

		if (!empty($object->lines)) {
			$object->printObjectLines($action, $mysoc, null, GETPOST('lineid', 'int'), 1);
		}

		// Form to add new line
		// if ($object->status == 0 && $permissiontoadd && $action != 'selectlines') {
		// 	if ($action != 'editline') {
		// 		// Add products/services form
		// 		$object->formAddObjectLine(1, $mysoc, $soc);

		// 		$parameters = array();
		// 		$reshook = $hookmanager->executeHooks('formAddObjectLine', $parameters, $object, $action); // Note that $action and $object may have been modified by hook
		// 	}
		// }

		if (!empty($object->lines) || ($object->status == $object::STATUS_DRAFT && $permissiontoadd && $action != 'selectlines' && $action != 'editline')) {
			print '</table>';
		}
		print '</div>';

		print "</form>\n";
	}

	// Buttons for actions

	if ($action != 'presend' && $action != 'editline') {
		print '<div class="tabsAction">' . "\n";
		$parameters = array();
		$reshook = $hookmanager->executeHooks('addMoreActionsButtons', $parameters, $object, $action); // Note that $action and $object may have been modified by hook
		if ($reshook < 0) {
			setEventMessages($hookmanager->error, $hookmanager->errors, 'errors');
		}

		if (empty($reshook)) {
			// Import
			if ($object->status == $object::STATUS_DRAFT) {
				print '<a class="butAction" href="' . $_SERVER["PHP_SELF"] . '?id=' . $object->id . '&action=import&mode=init#formmailbeforetitle&token=' . newToken() . '">' . $langs->trans('ImportToDolibarr') . '</a>' . "\n";
			}

			// Back to draft
			if ($object->status == $object::STATUS_VALIDATED) {
				if ($permissiontoadd) {
					print '<a class="butAction" href="' . $_SERVER['PHP_SELF'] . '?id=' . $object->id . '&action=confirm_setdraft&confirm=yes&token=' . newToken() . '">' . $langs->trans("SetToDraft") . '</a>';
				}
			}

			// Modify
			// if ($permissiontoadd) {
			//     print '<a class="butAction" href="' . $_SERVER["PHP_SELF"] . '?id=' . $object->id . '&action=edit">' . $langs->trans("Modify") . '</a>' . "\n";
			// } else {
			//     print '<a class="butActionRefused classfortooltip" href="#" title="' . dol_escape_htmltag($langs->trans("NotEnoughPermissions")) . '">' . $langs->trans('Modify') . '</a>' . "\n";
			// }

			// Validate
			if ($object->status == $object::STATUS_DRAFT) {
				if ($permissiontoadd) {
					if (empty($object->table_element_line) || (is_array($object->lines) && count($object->lines) > 0)) {
						print '<a class="butAction" href="' . $_SERVER['PHP_SELF'] . '?id=' . $object->id . '&action=confirm_validate&confirm=yes&token=' . newToken() . '">' . $langs->trans("Validate") . '</a>';
					} else {
						$langs->load("errors");
						print '<a class="butActionRefused" href="" title="' . $langs->trans("ErrorAddAtLeastOneLineFirst") . '">' . $langs->trans("Validate") . '</a>';
					}
				}
			}

			// Clone
			// if ($permissiontoadd) {
			//     print '<a class="butAction" href="' . $_SERVER['PHP_SELF'] . '?id=' . $object->id . '&socid=' . $object->socid . '&action=clone&object=myndf">' . $langs->trans("ToClone") . '</a>' . "\n";
			// }

			/*
			if ($permissiontoadd)
			{
			if ($object->status == $object::STATUS_ENABLED)
			{
			print '<a class="butActionDelete" href="'.$_SERVER["PHP_SELF"].'?id='.$object->id.'&action=disable">'.$langs->trans("Disable").'</a>'."\n";
			}
			else
			{
			print '<a class="butAction" href="'.$_SERVER["PHP_SELF"].'?id='.$object->id.'&action=enable">'.$langs->trans("Enable").'</a>'."\n";
			}
			}
			if ($permissiontoadd)
			{
			if ($object->status == $object::STATUS_VALIDATED)
			{
			print '<a class="butActionDelete" href="'.$_SERVER["PHP_SELF"].'?id='.$object->id.'&action=close">'.$langs->trans("Cancel").'</a>'."\n";
			}
			else
			{
			print '<a class="butAction" href="'.$_SERVER["PHP_SELF"].'?id='.$object->id.'&action=reopen">'.$langs->trans("Re-Open").'</a>'."\n";
			}
			}
			 */

			// Delete (need delete permission, or if draft, just need create/modify permission)
			if ($permissiontodelete || ($object->status == $object::STATUS_DRAFT)) {
				print '<a class="butActionDelete" href="' . $_SERVER["PHP_SELF"] . '?id=' . $object->id . '&action=delete&token=' . newToken() . '">' . $langs->trans('Delete') . '</a>' . "\n";
			} else {
				print '<a class="butActionRefused classfortooltip" href="#" title="' . dol_escape_htmltag($langs->trans("NotEnoughPermissions")) . '">' . $langs->trans('Delete') . '</a>' . "\n";
			}
		}
		print '</div>' . "\n";
	}

	// Select mail models is same action as presend
	if (GETPOST('modelselected')) {
		$action = 'presend';
	}

	if ($action != 'presend') {
		// print '<div class="fichecenter">';
		// print '<a name="builddoc"></a>'; // ancre

		// $includedocgeneration = 0;

		// // Documents
		// // if ($includedocgeneration) {
		// //     $objref = dol_sanitizeFileName($object->ref);
		// //     $relativepath = $objref . '/' . $objref . '.pdf';
		// //     $filedir = $conf->doliscan->dir_output . '/' . $object->element . '/' . $objref;
		// //     $urlsource = $_SERVER["PHP_SELF"] . "?id=" . $object->id;
		// //     $genallowed = $user->rights->doliscan->myndf->read;    // If you can read, you can build the PDF to read content
		// //     $delallowed = $user->rights->doliscan->myndf->write;    // If you can create/edit, you can remove a file on card
		// //     print $formfile->showdocuments('doliscan:MyNDF', $object->element . '/' . $objref, $filedir, $urlsource, $genallowed, $delallowed, $object->model_pdf, 1, 0, 0, 28, 0, '', '', '', $langs->defaultlang);
		// // }

		// // Show links to link elements
		// // $linktoelem = $form->showLinkToObjectBlock($object, null, array('myndf'));
		// // $somethingshown = $form->showLinkedObjectBlock($object, $linktoelem);

		// // print '</div><div class="fichehalfright"><div class="ficheaddleft">';

		// // $MAXEVENT = 10;

		// // $morehtmlright = '<a href="' . dol_buildpath('/doliscan/myndf_agenda.php', 1) . '?id=' . $object->id . '">';
		// // $morehtmlright .= $langs->trans("SeeAll");
		// // $morehtmlright .= '</a>';

		// // List of actions on element
		// // include_once DOL_DOCUMENT_ROOT . '/core/class/html.formactions.class.php';
		// // $formactions = new FormActions($db);
		// // $somethingshown = $formactions->showactions($object, $object->element, (is_object($object->thirdparty) ? $object->thirdparty->id : 0), 1, '', $MAXEVENT, '', $morehtmlright);

		// if ($id > 0 || !empty($ref)) {
		// 	$upload_dir = $conf->doliscan->multidir_output[$object->entity ? $object->entity : $conf->entity] . "/myndf/" . dol_sanitizeFileName($object->ref);
		// }

		// $filearray = dol_dir_list($upload_dir, "files", 0, '', '(\.meta|_preview.*\.png)$', $sortfield, (strtolower($sortorder) == 'desc' ? SORT_DESC : SORT_ASC), 1);

		// $modulepart = 'doliscan';
		// //$permission = $user->rights->doliscan->myndf->write;
		// $permission = 1;
		// //$permtoedit = $user->rights->doliscan->myndf->write;
		// $permtoedit = 1;
		// $param = '&id=' . $object->id;

		// //$relativepathwithnofile='myndf/' . dol_sanitizeFileName($object->id).'/';
		// $relativepathwithnofile = 'myndf/' . dol_sanitizeFileName($object->ref) . '/';

		// $formfile = new FormFile($db);

		// // We define var to enable the feature to add prefix of uploaded files.
		// // Caller of this include can make
		// // $savingdocmask=dol_sanitizeFileName($object->ref).'-__file__';
		// if (!isset($savingdocmask) || (dsbackport_getDolGlobalString('MAIN_DISABLE_SUGGEST_REF_AS_PREFIX','') != '')) {
		// 	$savingdocmask = '';
		// 	if (dsbackport_getDolGlobalString('MAIN_DISABLE_SUGGEST_REF_AS_PREFIX','') == '') {
		// 		//var_dump($modulepart);
		// 		if (in_array($modulepart, array(
		// 			'facture_fournisseur',
		// 			'commande_fournisseur',
		// 			'facture',
		// 			'commande',
		// 			'propal',
		// 			'supplier_proposal',
		// 			'ficheinter',
		// 			'contract',
		// 			'expedition',
		// 			'project',
		// 			'project_task',
		// 			'expensereport',
		// 			'tax',
		// 			'tax-vat',
		// 			'produit',
		// 			'product_batch',
		// 			'bom',
		// 			'mrp',
		// 		))) {
		// 			$savingdocmask = dol_sanitizeFileName($object->ref) . '-__file__';
		// 		}
		// 		/*if (in_array($modulepart,array('member')))
		// 		{
		// 		$savingdocmask=$object->login.'___file__';
		// 		}*/
		// 	}
		// }

		// // List of document
		// $formfile->list_of_documents(
		// 	$filearray,
		// 	$object,
		// 	$modulepart,
		// 	$param,
		// 	0,
		// 	$relativepathwithnofile, // relative path with no file. For example "0/1"
		// 	$permission,
		// 	0,
		// 	'',
		// 	0,
		// 	'',
		// 	'',
		// 	0,
		// 	$permtoedit,
		// 	$upload_dir,
		// 	$sortfield,
		// 	$sortorder,
		// 	$disablemove
		// );

		// print "<br>";

		// //List of links
		// print '</div></div>';
	}

	//Select mail models is same action as presend
	if (GETPOST('modelselected')) {
		$action = 'presend';
	}

	// Presend form
	$modelmail = 'myndf';
	$defaulttopic = 'InformationMessage';
	$diroutput = $conf->doliscan->dir_output;
	$trackid = 'myndf' . $object->id;

	include DOL_DOCUMENT_ROOT . '/core/tpl/card_presend.tpl.php';
}

// End of page
llxFooter();
$db->close();
