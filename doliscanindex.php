<?php
/* Copyright (C) 2001-2005 Rodolphe Quiedeville <rodolphe@quiedeville.org>
 * Copyright (C) 2004-2015 Laurent Destailleur  <eldy@users.sourceforge.net>
 * Copyright (C) 2005-2012 Regis Houssin        <regis.houssin@inodbox.com>
 * Copyright (C) 2015      Jean-François Ferry	<jfefe@aternatik.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

/**
 *	\file       doliscan/doliscanindex.php
 *	\ingroup    doliscan
 *	\brief      Home page of doliscan top menu
 */

// Load Dolibarr environment
$res = 0;
// Try main.inc.php into web root known defined into CONTEXT_DOCUMENT_ROOT (not always defined)
if (!$res && !empty($_SERVER["CONTEXT_DOCUMENT_ROOT"])) $res = @include $_SERVER["CONTEXT_DOCUMENT_ROOT"] . "/main.inc.php";
// Try main.inc.php into web root detected using web root calculated from SCRIPT_FILENAME
$tmp = empty($_SERVER['SCRIPT_FILENAME']) ? '' : $_SERVER['SCRIPT_FILENAME'];
$tmp2 = realpath(__FILE__);
$i = strlen($tmp) - 1;
$j = strlen($tmp2) - 1;
while ($i > 0 && $j > 0 && isset($tmp[$i]) && isset($tmp2[$j]) && $tmp[$i] == $tmp2[$j]) {
	$i--;
	$j--;
}
if (!$res && $i > 0 && file_exists(substr($tmp, 0, ($i + 1)) . "/main.inc.php")) $res = @include substr($tmp, 0, ($i + 1)) . "/main.inc.php";
if (!$res && $i > 0 && file_exists(dirname(substr($tmp, 0, ($i + 1))) . "/main.inc.php")) $res = @include dirname(substr($tmp, 0, ($i + 1))) . "/main.inc.php";
// Try main.inc.php using relative path
if (!$res && file_exists("../main.inc.php")) $res = @include "../main.inc.php";
if (!$res && file_exists("../../main.inc.php")) $res = @include "../../main.inc.php";
if (!$res && file_exists("../../../main.inc.php")) $res = @include "../../../main.inc.php";
if (!$res) die("Include of main fails");

require_once DOL_DOCUMENT_ROOT . '/core/class/html.formfile.class.php';

dol_include_once('/doliscan/lib/doliscan.lib.php');

// Load translation files required by the page
$langs->loadLangs(array("doliscan@doliscan"));

$action = GETPOST('action', 'alpha');


// Security check
//if (! $user->rights->doliscan->myobject->read) accessforbidden();
$socid = GETPOSTINT('socid');
if (isset($user->socid) && $user->socid > 0) {
	$action = '';
	$socid = $user->socid;
}

$max = 5;
$now = dol_now();


/*
 * Actions
 */

// None


/*
 * View
 */

$form = new Form($db);
$formfile = new FormFile($db);

dol_include_once('/doliscan/core/modules/modDoliscan.class.php');
dol_include_once('/doliscan/lib/doliscan.lib.php');
$modDoliscan = new modDoliscan($db);

llxHeader("", $langs->trans("DoliscanArea"));

print load_fiche_titre($langs->trans("DoliscanArea"), '', 'doliscan.png@doliscan');

print '<div class="fichecenter"><div class="fichethirdleft">';


/* BEGIN MODULEBUILDER DRAFT MYOBJECT */
// Draft MyObject
if (empty($conf->doliscan->enabled)) accessforbidden('Module not enabled');


$langs->loadLangs(array("doliscan@doliscan", "other"));

//Administrateur : on indique le nombre de comptes DoliSCAN et on les affiche
if ($user->admin) {
	if (!empty($conf->projet->enabled)) {
		$syncProjetMessage = "";

		if (GETPOST('action') == 'syncprojects' || dsbackport_getDolGlobalString('DOLISCAN_PROJECT_NEED_PUSH',-1) > 0) {
			print doliSCANpushProjects();
		} else {
			$syncProjetMessage = '<a href="doliscanindex.php?action=syncprojects">' . $langs->trans("DoliSCANSyncNow") . '</a>';
		}
	}


	// if ($user->rights->doliscan->read)
	// {

	$num  = 0;
	$num2 = 0;
	$sqlJoin = "as d," . MAIN_DB_PREFIX . "user as u WHERE d.fk_user=u.rowid AND u.entity IN (" . getEntity('user') . ")";

	$sql = "SELECT * FROM " . MAIN_DB_PREFIX . "doliscan_myaccount " . $sqlJoin;

	$resql = $db->query($sql);
	if ($resql) {
		$num = $db->num_rows($resql);
	}

	$dateLastSync = "";
	$numWait = 0;
	$sql2 = "SELECT * FROM " . MAIN_DB_PREFIX . "doliscan_myndf " . $sqlJoin . " AND d.status='0' ORDER BY date_creation DESC";
	$resql2 = $db->query($sql2);
	if ($resql2) {
		$obj = $db->fetch_object($resql2);
		$numWait = $db->num_rows($resql2);
		$dateLastSync = dsbackport_getDolGlobalString('DOLISCAN_LAST_SYNC','');
	}

	$dateLastImport = "";
	$numSync = 0;
	$sql3 = "SELECT * FROM " . MAIN_DB_PREFIX . "doliscan_myndf " . $sqlJoin . " AND d.status='1' ORDER BY date_creation DESC";
	$resql3 = $db->query($sql3);
	if ($resql3) {
		$obj = $db->fetch_object($resql3);
		$numSync = $db->num_rows($resql3);
		$dateLastImport = $obj->date_creation;
	}

	print '<table class="noborder centpercent">';
	print '<tr class="liste_titre">';
	print '  <th colspan="2">' . $langs->trans("DoliSCANStats") . '</th>';
	print '</tr>';
	print '<tr class="oddeven">';
	print '  <td class="nowrap">' . $langs->trans("DoliSCANnbAccounts") . '</td>';
	print '  <td class="right" class="nowrap">' . $num . '</td>';
	print '</tr>';
	print '<tr class="oddeven">';
	print '  <td class="nowrap">' . $langs->trans("DoliSCANnbNDFwaiting") . '</td>';
	print '  <td class="right" class="nowrap">' . $numWait . '</td>';
	print '</tr>';
	print '<tr class="oddeven">';
	print '  <td class="nowrap">' . $langs->trans("DoliSCANnbNDFsynced") . '</td>';
	print '  <td class="right" class="nowrap">' . $numSync . '</td>';
	print '</tr>';
	print '<tr class="oddeven">';
	print '  <td class="nowrap">' . $langs->trans("DoliSCANnbNDFlastDateSync") . '</td>';
	print '  <td class="right" class="nowrap">' . dol_print_date($dateLastSync) . '</td>';
	print '</tr>';
	print '<tr class="oddeven">';
	print '  <td class="nowrap">' . $langs->trans("DoliSCANnbNDFlastDateImport") . '</td>';
	print '  <td class="right" class="nowrap">' . dol_print_date($dateLastImport) . '</td>';
	print '</tr>';
	print '<tr class="oddeven">';
	print '  <td class="nowrap">' . $langs->trans("DoliSCANPluginVersion") . '</td>';
	print '  <td class="right" class="nowrap">' . $modDoliscan->version . '</td>';
	print '</tr>';

	if (!empty($conf->projet->enabled)) {
		print '<tr class="oddeven">';
		print '  <td class="nowrap">' . $langs->trans("DoliSCANsyncProjects") . '</td>';
		print '  <td class="right" class="nowrap">'.$syncProjetMessage.'</td>';
		print '</tr>';
	}

	if ($user->admin) {
		$syncDocumentsMessage = "";

		if (GETPOST('action') == 'syncdocuments') {
			dol_include_once('/doliscan/lib/doliscan_myndf.lib.php');
			print resyncAllDocuments();
		}

		$syncDocumentsMessage = '<a href="doliscanindex.php?action=syncdocuments">' . $langs->trans("DoliSCANSyncDocumentsNow") . '</a>';

		print '<tr class="oddeven">';
		print '  <td class="nowrap">' . $langs->trans("DoliSCANsyncDocuments") . '</td>';
		print '  <td class="right" class="nowrap">'.$syncDocumentsMessage.'</td>';
		print '</tr>';
	}

	print "</table><br>";

	$db->free($resql);
}



print '</div></div></div>';

// End of page
llxFooter();
$db->close();
