<?php
/* Copyright (C) 2020 Éric Seigne <eric.seigne@cap-rel.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file    doliscan/lib/doliscan.lib.php
 * \ingroup doliscan
 * \brief   Library files with common functions for Doliscan
 */

require_once DOL_DOCUMENT_ROOT . '/core/lib/admin.lib.php';
require_once DOL_DOCUMENT_ROOT . '/core/lib/geturl.lib.php';
if (!empty($conf->projet->enabled)) {
	require_once DOL_DOCUMENT_ROOT.'/projet/class/project.class.php';
}


//check module version vs last init version in database
dol_include_once('/doliscan/core/modules/modDoliscan.class.php');
if (isset($db)) {
	$tmpmodule = new modDoliscan($db);
	if ($tmpmodule->version != dsbackport_getDolGlobalString('DOLISCAN_MODULE_VERSION','')) {
		setEventMessages($langs->trans("ErrorDoliscanModuleVersionDatabase"), [], 'errors');
	}
}

/**
 * Prepare admin pages header
 *
 * @return array
 */
function doliscanAdminPrepareHead()
{
	global $langs, $conf;

	$langs->loadLangs(array("doliscan@doliscan", 'banks', 'categories', 'accountancy', 'compta'));

	$h = 0;
	$head = array();

	// if ($conf->multicompany->enabled) {
	//     $head[$h][0] = dol_buildpath("/doliscan/admin/setup.php", 1);
	//     $head[$h][1] = ($h + 1) . ". " . $langs->trans("0MultiEntity");
	//     $head[$h][2] = 'multientity';
	//     $h++;
	// }

	$head[$h][0] = dol_buildpath("/doliscan/admin/configurationserveur.php", 1);
	$head[$h][1] = ($h + 1) . ". " . $langs->trans("1Server");
	$head[$h][2] = 'server';
	$h++;
	$head[$h][0] = dol_buildpath("/doliscan/admin/configurationadmin.php", 1);
	$head[$h][1] = ($h + 1) . ". " . $langs->trans("2Admin");
	$head[$h][2] = 'admin';
	$h++;
	$head[$h][0] = dol_buildpath("/doliscan/admin/configurationpro.php", 1);
	$head[$h][1] = ($h + 1) . ". " . $langs->trans("2SuppliersLinks");
	$head[$h][2] = 'configurationpro';
	$h++;
	$head[$h][0] = dol_buildpath("/doliscan/admin/configurationperso.php", 1);
	$head[$h][1] = ($h + 1) . ". " . $langs->trans("3FeesType");
	$head[$h][2] = 'configurationperso';
	$h++;
	$head[$h][0] = dol_buildpath("/doliscan/admin/configurationbanque.php", 1);
	$head[$h][1] = ($h + 1) . ". " . $langs->trans("4PaymentsType");
	$head[$h][2] = 'configurationbanque';
	$h++;
	$head[$h][0] = dol_buildpath("/doliscan/admin/configurationtriggers.php", 1);
	$head[$h][1] = ($h + 1) . ". " . $langs->trans("5Triggers");
	$head[$h][2] = 'configurationtrigger';
	$h++;
	$head[$h][0] = dol_buildpath("/doliscan/admin/configurationcarbu.php", 1);
	$head[$h][1] = ($h + 1) . ". " . $langs->trans("6Carbu");
	$head[$h][2] = 'configurationcarbu';
	$h++;
	$head[$h][0] = dol_buildpath("/doliscan/admin/about.php", 1);
	$head[$h][1] = $langs->trans("5end");
	$head[$h][2] = 'about';
	$h++;

	// Show more tabs from modules
	// Entries must be declared in modules descriptor with line
	//$this->tabs = array(
	//    'entity:+tabname:Title:@doliscan:/doliscan/mypage.php?id=__ID__'
	//); // to add new tab
	//$this->tabs = array(
	//    'entity:-tabname:Title:@doliscan:/doliscan/mypage.php?id=__ID__'
	//); // to remove a tab
	complete_head_from_modules($conf, $langs, null, $head, $h, 'doliscan');

	return $head;
}

function display_company($socid)
{
	global $db;
	dol_syslog("DoliSCAN: Display Company id $socid ...");

	if(empty($socid)) {
		return "Undef";
	}

	$sql = "SELECT * FROM " . MAIN_DB_PREFIX . "societe WHERE rowid = " . (int) $socid;
	// print $sql;
	$result = $db->query($sql);
	if ($result) {
		while ($objp = $db->fetch_object($result)) {
			return "<a href=\"" . dol_buildpath("/societe/card.php", 1) . "?socid=$socid" . "\">$objp->nom</a>";
		}
	}
}

function display_frais($id)
{
	dol_syslog("DoliSCAN: Display Frais $id ...");
	global $db;
	global $langs;
	$langs->loadLangs(array("trips"));

	if(empty($id)) {
		return "Undef";
	}

	// Select des infos sur le type fees
	$sql = "SELECT * FROM " . MAIN_DB_PREFIX . "c_type_fees WHERE id = " . (int) $id;
	$resql = $db->query($sql);
	$label = "";
	if ($resql) {
		if ($objp_fees = $db->fetch_object($resql)) {
			$label = $langs->trans($objp_fees->code);
			if ($label == "" || $label == $objp_fees->code) {
				$label = $langs->trans($objp_fees->label);
			}

			if ($label == "") {
				$label = $objp_fees->code;
			}
		}
	}
	dol_syslog("DoliSCAN:  -> " . json_encode($label), LOG_DEBUG);
	return $label;
}

function display_banque_et_paiement($banqueid, $paiementid)
{
	dol_syslog("DoliSCAN: display_banque_et_paiement ...");
	global $db;
	global $langs;

	$label = "";

	//Moyens de paiements
	$sqlp = "SELECT libelle FROM " . MAIN_DB_PREFIX . "c_paiement WHERE id = '" . $paiementid . "'";
	$resp = $db->query($sqlp);
	if ($resp) {
		if ($objp = $db->fetch_object($resp)) {
			$label .= $langs->trans($objp->libelle);
		}
	}
	// dol_syslog("DoliSCAN: display_banque_et_paiement -> " . json_encode($label));

	// Comptes bancaires:
	$sqlc = "SELECT label FROM " . MAIN_DB_PREFIX . "bank_account WHERE rowid = '" . $banqueid . "'";
	// return $sql;
	$resc = $db->query($sqlc);
	if ($resc) {
		if ($objc = $db->fetch_object($resc)) {
			$label .= " " . $objc->label;
		}
	}
	// dol_syslog("DoliSCAN: display_banque_et_paiement -> " . json_encode($label));
	return $label;
}

function doliSCANuserAgent()
{
	global $conf, $db, $modDoliscan;
	if ($modDoliscan->version === null) {
		dol_include_once('/doliscan/core/modules/modDoliscan.class.php');
		$modDoliscan = new modDoliscan($db);
	}

	$uuid = dolibarr_get_const($db, "DOLISCANUUID", 0);
	if ($uuid == "") {
		$uuid = uniqid();
		$result = dolibarr_set_const($db, "DOLISCANUUID", $uuid, 'chaine', 0, '', 0);
		dol_syslog("DoliSCAN: set server UUID $uuid");
	}

	return 'dolibarr/' . $conf->global->MAIN_INFO_SOCIETE_NOM . "(" . $modDoliscan->version . ") [" . $uuid . "]";
}

/**
 * Creation d'un fournisseur
 *
 * @param  mixed $f
 * @return void
 */
function doliSCANcreateFournisseur($nom)
{
	global $db, $user;
	$s = new Societe($db);

	$s->name = $nom;
	$s->email = '';
	$s->country_id = '';
	$s->client = 0;
	$s->tva_assuj = 1;
	$s->fournisseur = 1;
	$s->code_client = -1;
	$s->code_fournisseur = -1;
	$s->tva_intra = '';
	$s->address = "Generic Supplier";
	$s->zip = "000000";
	$s->town = "";

	$db->begin();
	$result = $s->create($user);
	if ($result <= 0) {
		$db->rollback();
		dol_syslog("doliSCANcreateFournisseur Erreur : " . $s->error);
	} else {
		dol_syslog("doliSCANcreateFournisseur OK : " . $result);
		$db->commit();
	}
	return $result;
}

/**
 * factorisation du code de creation des entêtes
 *
 * @param   $userkey     userkey
 * @param   $withBearer  withBearer
 * @param   $isJson      isJson
 *
 * @return  [type]             [return description]
 */
function doliSCANApiCommonHeader($userkey = '', $withBearer = true, $isJson = true)
{
	global $conf;
	$curlHeaders = [
		'User-Agent: ' . doliSCANuserAgent(),
		'Accept: ' . 'application/json'
	];
	if ($withBearer && $userkey != '') {
		array_push($curlHeaders, 'Authorization: ' . 'Bearer '.$userkey);
	}
	if ($isJson) {
		array_push($curlHeaders, 'Content-Type: ' . 'application/json');
	}
	return $curlHeaders;
}

/**
 * push all active projects to doliscan
 *
 * @return  [type]  [return description]
 */
function doliSCANpushProjects()
{
	global $user, $conf, $db, $langs, $mysoc;

	$applicationKey = dsbackport_getDolGlobalString('DOLISCAN_MAINACCOUNT_APIKEY','');
	$email = dsbackport_getDolGlobalString('DOLISCAN_MAINACCOUNT_EMAIL','');
	$endpoint = dsbackport_getDolGlobalString('DOLISCAN_MAINSERVER','');

	$listOfProjects = [];
	$duplicates = [];

	//la liste des projets ouverts en cours
	$sql = "SELECT rowid FROM ".MAIN_DB_PREFIX."projet";
	$sql .= " WHERE entity IN (".getEntity('project').")";
	$sql .= " AND fk_statut = '" . Project::STATUS_VALIDATED . "'";

	//print "<p>SQL : $sql</p>";
	$mesg = "<p>Sync projects:<ul>";
	$resql = $db->query($sql);

	//List of object to send to doliscan
	$tags = array();
	$project = new Project($db);
	$allusers = array(); //liste de tous les utilisateurs concernés

	$doliscanusers = array();
	$sql2 = "SELECT ds_login as email FROM " . MAIN_DB_PREFIX . "doliscan_myaccount";
	$resql2 = $db->query($sql2);
	if ($resql2) {
		$num_rows = $db->num_rows($resql2);
		for ($i = 0; $i < $num_rows; $i++) {
			$obj = $db->fetch_object($resql2);
			$doliscanusers[] = $obj->email;
		}
	}

	// $mesg .= "<p>DoliScanUsers = " . json_encode($doliscanusers) . "</p>";

	if ($resql) {
		$num_rows = $db->num_rows($resql);
		if ($num_rows == 0) {
			$mesg .= $langs->trans("DoliSscanThreIsNoProject");
		}

		for ($i = 0; $i < $num_rows; $i++) {
			// $mesg .= "<p>Sync project ... $i</p>";
			$obj = $db->fetch_object($resql);
			$id = $obj->rowid;
			$res = $project->fetch($id);
			if ($res) {
				//uniquement s'il a un mot clé doliscan
				$doliCode = trim($project->array_options['options_doliscan_project_code']);
				if (empty($doliCode) && dsbackport_getDolGlobalString('DOLISCAN_USE_PROJECT_LABEL','') != '') {
					//TODO risque de collision avec des projets qui ont la même racine
					$doliCode = substr($project->title, 0, 96);
				}
				if(in_array($doliCode,$listOfProjects)) {
					$duplicates[] = $doliCode;
					continue;
				} else {
					$listOfProjects[] = $doliCode;
				}
				if (!empty($doliCode)) {
					$mesg .= "<li>Projet : <i>" . $project->title . "</i>, nom de code DoliSCAN <b>" . $doliCode . "</b></li>";
					//$mesg .= json_encode($project);
					$contactlist = $project->liste_contact(-1, 'internal');
					//$mesg .= "<p>Partcipants : " . json_encode($contactlist) . "</p>";
					//List of users who can view that project
					$users = array();
					$mesg .= "Accessible par<ul>";
					if ($project->public == 1) {
						$users = $doliscanusers;
						$mesg .= "<li>".implode(',', $users)."</li>";
					} else {
						foreach ($contactlist as $contact) {
							//$mesg .= "<p>recherche si " . json_encode($contact['id']) . " est dans la liste</p>";
							if (in_array($contact['email'], $doliscanusers)) {
								$users[] = $contact['email'];
								$mesg .= "<li>".$contact['email']."</li>";
								$allusers[] = $contact['email'];
							}
						}
					}
					$mesg .= "</ul>";

					$start = $db->idate($project->date_start);
					$end = $db->idate($project->date_end);
					//project closed ?
					if ($project->status == Project::STATUS_CLOSED && dsbackport_getDolGlobalString('DOLISCAN_HIDE_CLOSED_PROJECTS','') != '') {
						//force end date to be "closed" on app
						$end = date('Y-m-d', strtotime("-1 days"));
					}
					$mesg .= "Dates: du " . $start . " au ..." . $end;

					//$mesg .= json_encode($contactlist);
					$t = [
						'code'     => $project->ref,
						'label'    => $doliCode,
						'start'    => $start,
						'end'      => $end,
						'users_ro' => $users
					];
					$tags[] = $t;

					// $mesg .= "<li>$obj->ref : $obj->title</li>";
				} else {
					$mesg .= "<li>Projet NON synchronisé : <i>" . $project->title . "</i>, nom de code DoliSCAN <b>" . $doliCode . "</b></li>";
				}
			} else {
				$mesg .= "<p> Error fetching project</p>";
			}
		}

		if(count($duplicates) > 0) {
			$mesg .= "<li>Attention, présence de doublons sur les intitulés de projets</li>";
			$mesg .= "<ul>";
			foreach ($duplicates as $label) {
				$mesg .= "<li>" . $label . "</li>";
			}
			$mesg .= "</ul>";
		}

		$mesg .= "<hr>";
	}

	$email = "";
	if (!empty(dsbackport_getDolGlobalString('DOLISCAN_MAINACCOUNT_EMAIL',''))) {
		$email = dsbackport_getDolGlobalString('DOLISCAN_MAINACCOUNT_EMAIL','');
	} else {
		$email = $mysoc->email ?? $user->email;
	}

	$param = [
		'tags' => json_encode($tags),
		'multiusers' => array_unique($doliscanusers),
		'entrepriseIdProf' => $mysoc->idprof2,
		'entrepriseMail' => $email,
	];
	//$mesg .= json_encode($param);
	//$mesg .= "<hr>";

	$result = getURLContent($endpoint . '/api/Tag', 'POST', json_encode($param), 1, doliSCANApiCommonHeader($applicationKey), ['http','https'], 2);
	if (isset($result['curl_error_no'])) {
		handleDoliscanTimeoutBlacklist($result);
	}

	if (is_array($result) && ($result['http_code'] == 200) && isset($result['content'])) {
		$data = json_decode($result['content']);
		// dol_syslog("DoliSCAN: Request response body :  " . json_encode($data));
		$mesgPopup = '<div class="ok">' . $langs->trans("DoliSscanTagsPushOK") . '</div>';
		setEventMessages($mesgPopup, []);
	} else {
		$mesgPopup = '<div class="error">' . $langs->trans("DoliSscanTagsPushErr") . '</div>';
		setEventMessages($mesgPopup, [], 'errors');
	}
	//$mesg .= json_encode($result);
	//$mesg .= json_encode($result['content']);
	$mesg .= "</ul></p>";

	$r1 = dolibarr_set_const($db, "DOLISCAN_PROJECT_NEED_PUSH", '0', 'chaine', 0, '', $conf->entity);
	return $mesg;
}

/**
 * Set flag to know there is something to update in projects to dolibarr
 *
 * @return  [type]  [return description]
 */
function doliSCANpushProjectsFlag()
{
	global $db, $conf;
	$r1 = dolibarr_set_const($db, "DOLISCAN_PROJECT_NEED_PUSH", (string) dol_now(), 'chaine', 0, '', $conf->entity);
}

/**
 * delete account on remote DoliSCAN server
 *
 * @return  [type]  [return description]
 */
function doliSCANdeleteAccount($userMail)
{
	global $user, $conf, $db, $langs, $mysoc;

	$retour = 0;
	//utilisation de l'api avec le compte principal pour aller créer ce compte utilisateur complémentaire ...
	$applicationKey = dsbackport_getDolGlobalString('DOLISCAN_MAINACCOUNT_APIKEY','');
	$endpoint = dsbackport_getDolGlobalString('DOLISCAN_MAINSERVER','');

	dol_syslog("DoliSCAN: delete account on remote server ...");
	$param = [];
	$result = getURLContent($endpoint . '/api/user/' . $userMail, 'DELETE', json_encode($param), 1, doliSCANApiCommonHeader($applicationKey), ['http','https'], 2);
	if (isset($result['curl_error_no'])) {
		handleDoliscanTimeoutBlacklist($result);
	}

	if (is_array($result) && ($result['http_code'] == 200 || $result['http_code'] == 201) && isset($result['content'])) {
		$data = json_decode($result['content']);
		// dol_syslog("DoliSCAN: Request response body :  " . json_encode($data));
		$mesg = '<div class="ok">' . $langs->trans("AccountDeleteOK") . '</div>';
		setEventMessages($mesg, [], 'mesgs');
		$retour = 1;
	} else {
		$mesg = '<div class="error">' . $langs->trans("AccountDeleteErr") . '</div>';
		setEventMessages($mesg, [], 'errors');
		$retour = -1;
	}
	dol_syslog("DoliSCAN: end of delete account on remote server... ");
	return $retour;
}


/**
 * disable account on remote DoliSCAN server
 *
 * @return  [type]  [return description]
 */
function doliSCANdisableAccount($userMail)
{
	global $user, $conf, $db, $langs, $mysoc;

	$retour = 0;
	//utilisation de l'api avec le compte principal pour aller créer ce compte utilisateur complémentaire ...
	$applicationKey = dsbackport_getDolGlobalString('DOLISCAN_MAINACCOUNT_APIKEY','');
	$endpoint = dsbackport_getDolGlobalString('DOLISCAN_MAINSERVER','');

	dol_syslog("DoliSCAN: disable account on remote server ...");
	$param = [
		'email' => $userMail
	];
	$result = getURLContent($endpoint . '/api/user/disable', 'POST', json_encode($param), 1, doliSCANApiCommonHeader($applicationKey), ['http','https'], 2);
	if (isset($result['curl_error_no'])) {
		handleDoliscanTimeoutBlacklist($result);
	}

	if (is_array($result) && ($result['http_code'] == 200 || $result['http_code'] == 201) && isset($result['content'])) {
		$data = json_decode($result['content']);
		// dol_syslog("DoliSCAN: Request response body :  " . json_encode($data));
		$mesg = '<div class="ok">' . $langs->trans("AccountDisableOK") . '</div>';
		setEventMessages($mesg, [], 'mesgs');
		$retour = 1;
	} else {
		$mesg = '<div class="error">' . $langs->trans("AccountDisableErr") . '</div>';
		setEventMessages($mesg, [], 'errors');
		$retour = -1;
	}
	dol_syslog("DoliSCAN: end of disable account on remote server... ");
	return $retour;
}


/**
 * enable account on remote DoliSCAN server
 *
 * @return  [type]  [return description]
 */
function doliSCANenableAccount($userMail)
{
	global $user, $conf, $db, $langs, $mysoc;

	$retour = 0;
	//utilisation de l'api avec le compte principal pour aller créer ce compte utilisateur complémentaire ...
	$applicationKey = dsbackport_getDolGlobalString('DOLISCAN_MAINACCOUNT_APIKEY','');
	$endpoint = dsbackport_getDolGlobalString('DOLISCAN_MAINSERVER','');

	dol_syslog("DoliSCAN: enable account on remote server ...");
	$param = [
		'email' => $userMail
	];
	$result = getURLContent($endpoint . '/api/user/enable', 'POST', json_encode($param), 1, doliSCANApiCommonHeader($applicationKey), ['http','https'], 2);
	if (isset($result['curl_error_no'])) {
		handleDoliscanTimeoutBlacklist($result);
	}

	if (is_array($result) && ($result['http_code'] == 200 || $result['http_code'] == 201) && isset($result['content'])) {
		$data = json_decode($result['content']);
		// dol_syslog("DoliSCAN: Request response body :  " . json_encode($data));
		$mesg = '<div class="ok">' . $langs->trans("AccountenableOK") . '</div>';
		setEventMessages($mesg, [], 'mesgs');
		$retour = 1;
	} else {
		$mesg = '<div class="error">' . $langs->trans("AccountenableErr") . '</div>';
		setEventMessages($mesg, [], 'errors');
		$retour = -1;
	}
	dol_syslog("DoliSCAN: end of enable account on remote server... ");
	return $retour;
}


/**
 * create account on remote DooliSCAN server if needed
 *
 * @return  [type]  [return description]
 */
function doliSCANcreateAccount(MyAccount &$object, $userToCreate)
{
	global $user, $conf, $db, $langs, $mysoc;
	dol_syslog("DoliSCAN: ask for doliSCANcreateAccount");

	$error = 0;
	//utilisation de l'api avec le compte principal pour aller créer ce compte utilisateur complémentaire ...
	$applicationKey = dsbackport_getDolGlobalString('DOLISCAN_MAINACCOUNT_APIKEY','');
	$endpoint = dsbackport_getDolGlobalString('DOLISCAN_MAINSERVER','');

	if (empty($userToCreate->email)) {
		dol_syslog("DoliSCAN: create account on remote server error : " . json_encode($userToCreate->email));
		$error--;
		$mesg = '<div class="error">' . $langs->trans("ErrorEmailAddressIsNeeded") . '</div>';
		setEventMessages($mesg, [], 'errors');
		return $error;
	}

	dol_syslog("DoliSCAN: create account on remote server ...");
	$param = [
		'email' => dol_htmlentities($userToCreate->email),
		'name' => dol_string_unaccent($userToCreate->lastname),
		'firstname' => dol_string_unaccent($userToCreate->firstname),
		'siren' => dol_string_unaccent($mysoc->idprof1),
		'siret' => dol_string_unaccent($mysoc->idprof2),
		'askForAPI' => 'true',
		'askForAPIAppName' => 'dolibarr-' . $conf->global->MAIN_INFO_SOCIETE_NOM,
	];
	$result = getURLContent($endpoint . '/api/user', 'POST', json_encode($param), 1, doliSCANApiCommonHeader($applicationKey), ['http','https'], 2);
	if (isset($result['curl_error_no'])) {
		handleDoliscanTimeoutBlacklist($result);
	}

	if (is_array($result) && ($result['http_code'] == 200 || $result['http_code'] == 201) && isset($result['content'])) {
		dol_syslog("DoliSCAN: create account on remote server success");
		$data = json_decode($result['content']);
		// dol_syslog("DoliSCAN: Request response body :  " . json_encode($data));
		$mesg = '<div class="ok">' . $langs->trans("AccountActivatedOK") . '</div>';
		$object->ds_login = $data->email;
		$object->ds_api = $data->api_token;
		$object->fk_user = $userToCreate->id;
		$object->status = MyAccount::STATUS_VALIDATED; //ou DRAFT ?
		//update or create ?
		if (empty($object->id)) {
			$id = $object->create($user);
		} else {
			$id = $object->update($user);
		}
		dol_syslog("DoliSCAN: create account on remote server success MyAccount ID is $id");
	} else {
		dol_syslog("DoliSCAN: create account on remote server error : " . json_encode($result['message']));
		$error--;
		$mesg = '<div class="error">' . $langs->trans("AccountActivatedErr") . '</div>';
	}

	if ($error < 0) {
		setEventMessages($mesg, [], 'errors');
	} else {
		setEventMessages($mesg, []);
	}

	dol_syslog("DoliSCAN: end of create account on remote server... ");
	return $error;
}

/**
 * prise en compte du firewall global cap-rel / inli
 *
 * @return  [type]  [return description]
 */
function handleDoliscanTimeoutBlacklist($curlRes)
{
	global $langs;
	$ip = '';
	dol_syslog("handleDoliscanTimeoutBlacklist : " . json_encode($curlRes));
	// voir https://curl.se/libcurl/c/libcurl-errors.html
	if (isset($curlRes['curl_error_no']) && (in_array($curlRes['curl_error_no'], [5,6,7,9,22,26,27,28,35,53,54,58,59,60,61]))) {
		$result = getURLContent('https://bl.cap-rel.fr/ip.php', 'GET', '', 1, doliSCANApiCommonHeader('', false, false), ['https'], 2);
		if (is_array($result) && ($result['http_code'] == 200) && isset($result['content'])) {
			$ip = $result['content'];
			dol_syslog('handleDoliscanTimeoutBlacklist this dolibarr public ip is ' . $ip);
			$data = doliscan_lightEncryptText(ip2long($ip));
			setEventMessages($langs->trans('DoliScanBlacklistErrorTitle'), [$langs->trans('DoliScanBlacklistError', $data, $ip)], 'errors');
		} else {
			setEventMessages($langs->trans('DoliScanBlacklistErrorTitle'), [$langs->trans('DoliScanBlacklistErrorFull')], 'errors');
		}
	} else {
		//erreur inconnue
	}
}

/**
 * encrypt text with key
 *
 * @param   [type]  $plaintext  [$plaintext description]
 * @param   [type]  $key        [$key description]
 *
 * @return  [type]              [return description]
 */
function doliscan_lightEncryptText($plaintext)
{
	$key = ip2long($_SERVER['REMOTE_ADDR']);
	$value = ($plaintext^$key);
	return base64_encode("data=" . $value);
}



/**
 *	Return list of suppliers products - hack from official dolibarr sources, but without a bug (?) or misscomp about product prices table
* -> remove all sql request about "pfp" product prices
*
*  @param	int		$socid   			Id of supplier thirdparty (0 = no filter)
*  @param   string  $selected       	Product price pre-selected (must be 'id' in product_fournisseur_price or 'idprod_IDPROD')
*  @param   string	$htmlname       	Name of HTML select
*  @param	string	$filtertype     	Filter on product type (''=nofilter, 0=product, 1=service)
*  @param   string	$filtre         	Generic filter. Data must not come from user input.
*  @param   string	$filterkey      	Filter of produdts
*  @param   int		$statut         	-1=Return all products, 0=Products not on buy, 1=Products on buy
*  @param   int		$outputmode     	0=HTML select string, 1=Array
*  @param   int     $limit          	Limit of line number
*  @param   int     $alsoproductwithnosupplierprice    1=Add also product without supplier prices
*  @param	string	$morecss			Add more CSS
*  @param	int		$showstockinlist	Show stock information (slower).
*  @param	string	$placeholder		Placeholder
*
*  @return  mixed   string or array depends on $outputmode
*/
function doliscanSelect_produits_fournisseurs_list($socid, $selected = '', $htmlname = 'productid', $filtertype = '', $filtre = '', $filterkey = '', $statut = -1, $outputmode = 0, $limit = 100, $alsoproductwithnosupplierprice = 0, $morecss = '', $showstockinlist = 0, $placeholder = '')
{
	// phpcs:enable
	global $langs, $conf, $db, $user;

	$out = '';
	$outarray = array();

	$maxlengtharticle = (empty(backport_doliscan_getDolGlobalString('PRODUCT_MAX_LENGTH_COMBO')) ? 48 : backport_doliscan_getDolGlobalString('PRODUCT_MAX_LENGTH_COMBO'));

	$langs->load('stocks');
	// Units
	if (!empty(backport_doliscan_getDolGlobalString('PRODUCT_USE_UNITS'))) {
		$langs->load('other');
	}

	$sql = "SELECT p.rowid, p.ref, p.label, p.price, p.duration, p.fk_product_type, p.stock,";
	$sql .= " p.description";
	$sql .= " FROM ".MAIN_DB_PREFIX."product as p";
	if ($socid > 0) {
		$sql .= " AND pfp.fk_soc = ".((int) $socid);
	}
	$sql .= " WHERE p.entity IN (".getEntity('product').")";
	if ($statut != -1) {
		$sql .= " AND p.tobuy = ".((int) $statut);
	}
	if (strval($filtertype) != '') {
		$sql .= " AND p.fk_product_type = ".((int) $filtertype);
	}
	if (!empty($filtre)) {
		$sql .= " ".$filtre;
	}
	$sql .= $db->plimit($limit, 0);

	// Build output string

	dol_syslog("::doliscanSelect_produits_fournisseurs_list", LOG_DEBUG);
	$result = $db->query($sql);
	if ($result) {
		require_once DOL_DOCUMENT_ROOT.'/product/dynamic_price/class/price_parser.class.php';
		require_once DOL_DOCUMENT_ROOT.'/core/lib/product.lib.php';

		$num = $db->num_rows($result);

		//$out.='<select class="flat" id="select'.$htmlname.'" name="'.$htmlname.'">';	// remove select to have id same with combo and ajax
		$out .= '<select class="flat '.($morecss ? ' '.$morecss : '').'" id="'.$htmlname.'" name="'.$htmlname.'">';
		if (!$selected) {
			$out .= '<option value="-1" selected>'.($placeholder ? $placeholder : '&nbsp;').'</option>';
		} else {
			$out .= '<option value="-1">'.($placeholder ? $placeholder : '&nbsp;').'</option>';
		}

		$i = 0;
		while ($i < $num) {
			$objp = $db->fetch_object($result);

			$outkey = $objp->idprodfournprice ?? null; // id in table of price
			if (!$outkey && $alsoproductwithnosupplierprice) {
				$outkey = 'idprod_'.$objp->rowid; // id of product
			}

			$outref = $objp->ref;
			$outval = '';
			$outbarcode = $objp->barcode ?? null;
			$outqty = 1;
			$outdiscount = 0;
			$outtype = $objp->fk_product_type;
			$outdurationvalue = $outtype == Product::TYPE_SERVICE ? substr($objp->duration, 0, dol_strlen($objp->duration) - 1) : '';
			$outdurationunit = $outtype == Product::TYPE_SERVICE ? substr($objp->duration, -1) : '';

			// Units
			$outvalUnits = '';
			if (!empty(backport_doliscan_getDolGlobalString('PRODUCT_USE_UNITS'))) {
				if (!empty($objp->unit_short)) {
					$outvalUnits .= ' - '.$objp->unit_short;
				}
				if (!empty($objp->weight) && $objp->weight_units !== null) {
					$unitToShow = showDimensionInBestUnit($objp->weight, $objp->weight_units, 'weight', $langs);
					$outvalUnits .= ' - '.$unitToShow;
				}
				if ((!empty($objp->length) || !empty($objp->width) || !empty($objp->height)) && $objp->length_units !== null) {
					$unitToShow = $objp->length.' x '.$objp->width.' x '.$objp->height.' '.measuringUnitString(0, 'size', $objp->length_units);
					$outvalUnits .= ' - '.$unitToShow;
				}
				if (!empty($objp->surface) && $objp->surface_units !== null) {
					$unitToShow = showDimensionInBestUnit($objp->surface, $objp->surface_units, 'surface', $langs);
					$outvalUnits .= ' - '.$unitToShow;
				}
				if (!empty($objp->volume) && $objp->volume_units !== null) {
					$unitToShow = showDimensionInBestUnit($objp->volume, $objp->volume_units, 'volume', $langs);
					$outvalUnits .= ' - '.$unitToShow;
				}
				if ($outdurationvalue && $outdurationunit) {
					$da = array(
						'h' => $langs->trans('Hour'),
						'd' => $langs->trans('Day'),
						'w' => $langs->trans('Week'),
						'm' => $langs->trans('Month'),
						'y' => $langs->trans('Year')
					);
					if (isset($da[$outdurationunit])) {
						$outvalUnits .= ' - '.$outdurationvalue.' '.$langs->transnoentities($da[$outdurationunit].($outdurationvalue > 1 ? 's' : ''));
					}
				}
			}

			$objRef = $objp->ref;
			if ($filterkey && $filterkey != '') {
				$objRef = preg_replace('/('.preg_quote($filterkey, '/').')/i', '<strong>$1</strong>', $objRef, 1);
			}
			$objRefFourn = $objp->ref_fourn ?? null;
			if ($filterkey && $filterkey != '') {
				$objRefFourn = preg_replace('/('.preg_quote($filterkey, '/').')/i', '<strong>$1</strong>', $objRefFourn, 1);
			}
			$label = doliscanClean_label($objp->label);
			if ($filterkey && $filterkey != '') {
				$label = preg_replace('/('.preg_quote($filterkey, '/').')/i', '<strong>$1</strong>', $label, 1);
			}

			$optlabel = $objp->ref;
			if (!empty($objp->idprodfournprice) && ($objp->ref != $objp->ref_fourn)) {
				$optlabel .= ' <span class=\'opacitymedium\'>('.$objp->ref_fourn.')</span>';
			}
			if (!empty($conf->barcode->enabled) && !empty($objp->barcode)) {
				$optlabel .= ' ('.$outbarcode.')';
			}
			$optlabel .= ' - '.dol_trunc($label, $maxlengtharticle);

			$outvallabel = $objRef;
			if (!empty($objp->idprodfournprice) && ($objp->ref != $objp->ref_fourn)) {
				$outvallabel .= ' ('.$objRefFourn.')';
			}
			if (!empty($conf->barcode->enabled) && !empty($objp->barcode)) {
				$outvallabel .= ' ('.$outbarcode.')';
			}
			$outvallabel .= ' - '.dol_trunc($label, $maxlengtharticle);

			// Units
			$optlabel .= $outvalUnits;
			$outvallabel .= $outvalUnits;

			if (!empty($objp->idprodfournprice)) {
				$outqty = $objp->quantity;
				$outdiscount = $objp->remise_percent;
				if (!empty($conf->dynamicprices->enabled) && !empty($objp->fk_supplier_price_expression)) {
					$prod_supplier = new ProductFournisseur($db);
					$prod_supplier->product_fourn_price_id = $objp->idprodfournprice;
					$prod_supplier->id = $objp->fk_product;
					$prod_supplier->fourn_qty = $objp->quantity;
					$prod_supplier->fourn_tva_tx = $objp->tva_tx;
					$prod_supplier->fk_supplier_price_expression = $objp->fk_supplier_price_expression;
					$priceparser = new PriceParser($db);
					$price_result = $priceparser->parseProductSupplier($prod_supplier);
					if ($price_result >= 0) {
						$objp->fprice = $price_result;
						if ($objp->quantity >= 1) {
							$objp->unitprice = $objp->fprice / $objp->quantity; // Replace dynamically unitprice
						}
					}
				}
				if ($objp->quantity == 1) {
					$optlabel .= ' - '.price($objp->fprice * (!empty(backport_doliscan_getDolGlobalString('DISPLAY_DISCOUNTED_SUPPLIER_PRICE')) ? (1 - $objp->remise_percent / 100) : 1), 1, $langs, 0, 0, -1, $conf->currency)."/";
					$outvallabel .= ' - '.price($objp->fprice * (!empty(backport_doliscan_getDolGlobalString('DISPLAY_DISCOUNTED_SUPPLIER_PRICE')) ? (1 - $objp->remise_percent / 100) : 1), 0, $langs, 0, 0, -1, $conf->currency)."/";
					$optlabel .= $langs->trans("Unit"); // Do not use strtolower because it breaks utf8 encoding
					$outvallabel .= $langs->transnoentities("Unit");
				} else {
					$optlabel .= ' - '.price($objp->fprice * (!empty(backport_doliscan_getDolGlobalString('DISPLAY_DISCOUNTED_SUPPLIER_PRICE')) ? (1 - $objp->remise_percent / 100) : 1), 1, $langs, 0, 0, -1, $conf->currency)."/".$objp->quantity;
					$outvallabel .= ' - '.price($objp->fprice * (!empty(backport_doliscan_getDolGlobalString('DISPLAY_DISCOUNTED_SUPPLIER_PRICE')) ? (1 - $objp->remise_percent / 100) : 1), 0, $langs, 0, 0, -1, $conf->currency)."/".$objp->quantity;
					$optlabel .= ' '.$langs->trans("Units"); // Do not use strtolower because it breaks utf8 encoding
					$outvallabel .= ' '.$langs->transnoentities("Units");
				}

				if ($objp->quantity > 1) {
					$optlabel .= " (".price($objp->unitprice * (!empty(backport_doliscan_getDolGlobalString('DISPLAY_DISCOUNTED_SUPPLIER_PRICE')) ? (1 - $objp->remise_percent / 100) : 1), 1, $langs, 0, 0, -1, $conf->currency)."/".$langs->trans("Unit").")"; // Do not use strtolower because it breaks utf8 encoding
					$outvallabel .= " (".price($objp->unitprice * (!empty(backport_doliscan_getDolGlobalString('DISPLAY_DISCOUNTED_SUPPLIER_PRICE')) ? (1 - $objp->remise_percent / 100) : 1), 0, $langs, 0, 0, -1, $conf->currency)."/".$langs->transnoentities("Unit").")"; // Do not use strtolower because it breaks utf8 encoding
				}
				if ($objp->remise_percent >= 1) {
					$optlabel .= " - ".$langs->trans("Discount")." : ".vatrate($objp->remise_percent).' %';
					$outvallabel .= " - ".$langs->transnoentities("Discount")." : ".vatrate($objp->remise_percent).' %';
				}
				if ($objp->duration) {
					$optlabel .= " - ".$objp->duration;
					$outvallabel .= " - ".$objp->duration;
				}
				if (!$socid) {
					$optlabel .= " - ".dol_trunc($objp->name, 8);
					$outvallabel .= " - ".dol_trunc($objp->name, 8);
				}
				if ($objp->supplier_reputation) {
					//TODO dictionary
					$reputations = array(''=>$langs->trans('Standard'), 'FAVORITE'=>$langs->trans('Favorite'), 'NOTTHGOOD'=>$langs->trans('NotTheGoodQualitySupplier'), 'DONOTORDER'=>$langs->trans('DoNotOrderThisProductToThisSupplier'));

					$optlabel .= " - ".$reputations[$objp->supplier_reputation];
					$outvallabel .= " - ".$reputations[$objp->supplier_reputation];
				}
			} else {
				if (empty($alsoproductwithnosupplierprice)) {     // No supplier price defined for couple product/supplier
					$optlabel .= " - <span class='opacitymedium'>".$langs->trans("NoPriceDefinedForThisSupplier").'</span>';
					$outvallabel .= ' - '.$langs->transnoentities("NoPriceDefinedForThisSupplier");
				} else { // No supplier price defined for product, even on other suppliers
					$optlabel .= " - <span class='opacitymedium'>".$langs->trans("NoPriceDefinedForThisSupplier").'</span>';
					$outvallabel .= ' - '.$langs->transnoentities("NoPriceDefinedForThisSupplier");
				}
			}

			if (!empty($conf->stock->enabled) && $showstockinlist && isset($objp->stock) && ($objp->fk_product_type == Product::TYPE_PRODUCT || !empty(backport_doliscan_getDolGlobalString('STOCK_SUPPORTS_SERVICES')))) {
				$novirtualstock = ($showstockinlist == 2);

				if (!empty($user->rights->stock->lire)) {
					$outvallabel .= ' - '.$langs->trans("Stock").': '.price((float) price2num($objp->stock, 'MS'));

					if ($objp->stock > 0) {
						$optlabel .= ' - <span class="product_line_stock_ok">';
					} elseif ($objp->stock <= 0) {
						$optlabel .= ' - <span class="product_line_stock_too_low">';
					}
					$optlabel .= $langs->transnoentities("Stock").':'.price((float) price2num($objp->stock, 'MS'));
					$optlabel .= '</span>';
					if (empty($novirtualstock) && !empty(backport_doliscan_getDolGlobalString('STOCK_SHOW_VIRTUAL_STOCK_IN_PRODUCTS_COMBO'))) {  // Warning, this option may slow down combo list generation
						$langs->load("stocks");

						$tmpproduct = new Product($db);
						$tmpproduct->fetch($objp->rowid, '', '', '', 1, 1, 1); // Load product without lang and prices arrays (we just need to make ->virtual_stock() after)
						$tmpproduct->load_virtual_stock();
						$virtualstock = $tmpproduct->stock_theorique;

						$outvallabel .= ' - '.$langs->trans("VirtualStock").':'.$virtualstock;

						$optlabel .= ' - '.$langs->transnoentities("VirtualStock").':';
						if ($virtualstock > 0) {
							$optlabel .= '<span class="product_line_stock_ok">';
						} elseif ($virtualstock <= 0) {
							$optlabel .= '<span class="product_line_stock_too_low">';
						}
						$optlabel .= $virtualstock;
						$optlabel .= '</span>';

						unset($tmpproduct);
					}
				}
			}

			$opt = '<option value="'.$outkey.'"';
			//erics
			if ($selected && $selected == $outkey) {
				$opt .= ' selected';
			}
			if (empty($objp->idprodfournprice) && empty($alsoproductwithnosupplierprice)) {
				$opt .= ' disabled';
			}
			if (!empty($objp->idprodfournprice) && $objp->idprodfournprice > 0) {
				$opt .= ' data-qty="'.$objp->quantity.'" data-up="'.$objp->unitprice.'" data-discount="'.$outdiscount.'"';
			}
			$opt .= ' data-description="'.dol_escape_htmltag($objp->description, 0, 1).'"';
			$opt .= ' data-html="'.dol_escape_htmltag($optlabel).'"';
			$opt .= '>';

			$opt .= $optlabel;
			$outval .= $outvallabel;

			$opt .= "</option>\n";


			// Add new entry
			// "key" value of json key array is used by jQuery automatically as selected value. Example: 'type' = product or service, 'price_ht' = unit price without tax
			// "label" value of json key array is used by jQuery automatically as text for combo box
			$out .= $opt;

			$disabled = (empty($objp->idprodfournprice) ? true : false);
			if (!empty($alsoproductwithnosupplierprice)) {
				$disabled = false;
			}

			$outarray[$objp->rowid] = $outref;

			// array_push(
			// 	$outarray,
			// 	$outref
				//erics pour eviter "array" en mode visualisation du formulaire
				// array(
				// 	'key'=>$outkey,
				// 	'id'=>$objp->rowid,
				// 	'value'=>$outref,
				// 	'label'=>$outval,
				// 	'qty'=>$outqty,
				// 	'price_ht'=>price2num($objp->unitprice ?? 0, 'MT'),
				// 	'discount'=>$outdiscount,
				// 	'type'=>$outtype,
				// 	'duration_value'=>$outdurationvalue,
				// 	'duration_unit'=>$outdurationunit,
				// 	'disabled'=>$disabled,
				// 	'description'=>$objp->description
				// )
			// );
			// Exemple of var_dump $outarray
			// array(1) {[0]=>array(6) {[key"]=>string(1) "2" ["value"]=>string(3) "ppp"
			//           ["label"]=>string(76) "ppp (<strong>f</strong>ff2) - ppp - 20,00 Euros/1unité (20,00 Euros/unité)"
			//      	 ["qty"]=>string(1) "1" ["discount"]=>string(1) "0" ["disabled"]=>bool(false)
			//}
			//var_dump($outval); var_dump(utf8_check($outval)); var_dump(json_encode($outval));
			//$outval=array('label'=>'ppp (<strong>f</strong>ff2) - ppp - 20,00 Euros/ Unité (20,00 Euros/unité)');
			//var_dump($outval); var_dump(utf8_check($outval)); var_dump(json_encode($outval));

			$i++;
		}
		$out .= '</select>';

		$db->free($result);

		include_once DOL_DOCUMENT_ROOT.'/core/lib/ajax.lib.php';
		$out .= ajax_combobox($htmlname);

		if (empty($outputmode)) {
			return $out;
		}
		return $outarray;
	} else {
		dol_print_error($db);
	}
	return $outarray;
}

/**
 * waiting for dolibarr mini with 2 args will become the oldest dolibarr in prod
 *
 * @param   [type]  $key      [$key description]
 * @param   [type]  $default  [$default description]
 *
 * @return  [type]            [return description]
 */
function backport_doliscan_getDolGlobalString($key, $default = '')
{
	if (function_exists('getDolGlobalString')) {
		if (((int) DOL_VERSION) < 15) {
			$res = getDolGlobalString($key);
			if (empty($res)) {
				$res = $default;
			}
			return $res;
		} else {
			/** @phpstan-ignore-next-line */
			return getDolGlobalString($key, $default);
		}
	}
	global $conf;
	// return $conf->global->$key ?? $default;
	return (string) (empty($conf->global->$key) ? $default : $conf->global->$key);
}


/**
 * clean up text (remove end spaces and non text char)
 *
 * @param   [type]  $txt  [$txt description]
 *
 * @return  [type]        [return description]
 */
function doliscanClean_label($txt)
{
	return trim($txt, '\s\t\n\r\0\0xB: ');
}



/**
 * Is Dolibarr module enabled
 *
 * @param string $module module name to check
 * @return int
 */
if (!function_exists('isModEnabled')) {
	function isModEnabled($module)
	{
		global $conf;
		return !empty($conf->$module->enabled);
	}
}

function dsbackport_getDolGlobalString($key, $default = '')
{
	if (function_exists('getDolGlobalString')) {
        if (((int) DOL_VERSION) < 15) {
            $res = getDolGlobalString($key);
			if(empty($res)) {
				$res = $default;
			}
			return $res;
        } else {
			/** @phpstan-ignore-next-line */
			return getDolGlobalString($key, $default);
		}
	}
	global $conf;
	// return $conf->global->$key ?? $default;
	return (string) (empty($conf->global->$key) ? $default : $conf->global->$key);
}
