<?php
/* Copyright (C) 2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

require_once DOL_DOCUMENT_ROOT . '/fourn/class/fournisseur.facture.class.php';
require_once DOL_DOCUMENT_ROOT . '/fourn/class/paiementfourn.class.php';
require_once DOL_DOCUMENT_ROOT . '/expensereport/class/expensereport.class.php';
require_once DOL_DOCUMENT_ROOT . '/ecm/class/ecmfiles.class.php';
require_once DOL_DOCUMENT_ROOT . '/product/class/product.class.php';
if (!empty($conf->projet->enabled)) {
	require_once DOL_DOCUMENT_ROOT . '/projet/class/project.class.php';
}

dol_include_once('/doliscan/core/modules/modDoliscan.class.php');
dol_include_once('/doliscan/lib/doliscan.lib.php');

/**
 * \file    lib/doliscan_myndf.lib.php
 * \ingroup doliscan
 * \brief   Library files with common functions for MyNDF
 */

/**
 * Prepare array of tabs for MyNDF
 *
 * @param    MyNDF    $object        MyNDF
 * @return     array                    Array of tabs
 */
function myndfPrepareHead($object)
{
	global $db, $langs, $conf;

	$langs->load("doliscan@doliscan");

	$h = 0;
	$head = array();

	$head[$h][0] = dol_buildpath("/doliscan/myndf_card.php", 1) . '?id=' . $object->id;
	$head[$h][1] = $langs->trans("Card");
	$head[$h][2] = 'card';
	$h++;

	// if (isset($object->fields['note_public']) || isset($object->fields['note_private']))
	// {
	//     $nbNote = 0;
	//     if (!empty($object->note_private)) $nbNote++;
	//     if (!empty($object->note_public)) $nbNote++;
	//     $head[$h][0] = dol_buildpath('/doliscan/myndf_note.php', 1).'?id='.$object->id;
	//     $head[$h][1] = $langs->trans('Notes');
	//     if ($nbNote > 0) $head[$h][1] .= (empty(dsbackport_getDolGlobalString('MAIN_OPTIMIZEFORTEXTBROWSER','')) ? '<span class="badge marginleftonlyshort">'.$nbNote.'</span>' : '');
	//     $head[$h][2] = 'note';
	//     $h++;
	// }

	require_once DOL_DOCUMENT_ROOT . '/core/lib/files.lib.php';
	require_once DOL_DOCUMENT_ROOT . '/core/class/link.class.php';
	$upload_dir = $conf->doliscan->dir_output . "/myndf/" . dol_sanitizeFileName($object->ref);
	$nbFiles = count(dol_dir_list($upload_dir, 'files', 0, '', ['(\.meta|_preview.*\.png)$']));
	$nbLinks = Link::count($db, $object->element, $object->id);
	$head[$h][0] = dol_buildpath("/doliscan/myndf_document.php", 1) . '?id=' . $object->id;
	$head[$h][1] = $langs->trans('Documents');
	if (($nbFiles + $nbLinks) > 0) {
		$head[$h][1] .= '<span class="badge marginleftonlyshort">' . ($nbFiles + $nbLinks) . '</span>';
	}

	$head[$h][2] = 'document';
	$h++;

	// $head[$h][0] = dol_buildpath("/doliscan/myndf_agenda.php", 1).'?id='.$object->id;
	// $head[$h][1] = $langs->trans("Events");
	// $head[$h][2] = 'agenda';
	// $h++;

	// Show more tabs from modules
	// Entries must be declared in modules descriptor with line
	//$this->tabs = array(
	//    'entity:+tabname:Title:@doliscan:/doliscan/mypage.php?id=__ID__'
	//); // to add new tab
	//$this->tabs = array(
	//    'entity:-tabname:Title:@doliscan:/doliscan/mypage.php?id=__ID__'
	//); // to remove a tab
	// complete_head_from_modules($conf, $langs, $object, $head, $h, 'myndf@doliscan');
	// complete_head_from_modules($conf, $langs, $object, $head, $h, 'myndf@doliscan', 'remove');

	return $head;
}

/**
 * fournisseurIdfromSlug transforme le slug doliscan en référence fournisseur dolibarr
 *
 * @param  mixed $slug
 * @return int
 */
function fournisseurIdfromSlug($slug)
{
	global $conf;
	$dolFraisPro = json_decode(dsbackport_getDolGlobalString('DOLISCAN_GLOBAL_FRAISPRO','{}'));
	$r = $dolFraisPro->$slug;
	dol_syslog("DoliSCAN: Recherche du fournisseur dolibarr associé au frais payé pro doliscan '$slug' : $r");
	return $r;
}

/**
 * fraisIdfromSlug transforme le slug doliscan en référence de frais dolibarr
 *
 * @param  mixed $slug
 * @return int
 */
function fraisIdfromSlug($slug)
{
	global $conf;
	$dolFraisPerso = json_decode(dsbackport_getDolGlobalString('DOLISCAN_GLOBAL_FRAISPERSO','{}'));
	$r = $dolFraisPerso->$slug;
	dol_syslog("DoliSCAN: Recherche du frais dolibarr associé au frais payé perso doliscan '$slug' : $r");
	return $r;
}

/**
 * paiementIdFromSlug transforme le slug doliscan en référence de paiement dolibarr
 *
 * @param  mixed $slug
 * @return array
 */
function paiementIdFromSlug($slug, $user)
{
	global $conf;
	$dolBanque = json_decode(dsbackport_getDolGlobalString('DOLISCAN_GLOBAL_BANQUE','{}'));
	//{"cb-pro":{"idbanque":"1","idpaiement":"6"},"cheque-pro":{"idbanque":"1","idpaiement":"7"},"autre-pro":{"idbanque":"1","idpaiement":"2"},"especes-pro":{"idbanque":"1","idpaiement":"4"}}
	$banque = $dolBanque->$slug->idbanque;
	$paiement = $dolBanque->$slug->idpaiement;

	//attention si cb-pro d'un utilisateur il y a peut-être une config particuliere
	if ($slug == "cb-pro") {
		$dolkey = "DOLISCAN_BANQUE_COMPTE_CB_PRO_USER_" . $user->id;
		$banque = $conf->global->$dolkey;
		$dolkeyPaiement = "DOLISCAN_BANQUE_COMPTE_CB_PRO_ID_USER_" . $user->id;
		$paiement = $conf->global->$dolkeyPaiement;
	}

	dol_syslog("DoliSCAN: Recherche du paiement dolibarr associé au paiement doliscan '$slug' : banque=$banque, paiement=$paiement");
	return array((int) $banque, (int) $paiement);
}


/**
 * paiementIdFromSupplier recupere le moyen de paiement par défaut associé au fournisseur qui serait
 * donc prioritaire
 *
 * @param  Societe $fourn fournisseur
 * @return array
 */
function paiementIdFromSupplier($fourn)
{
	dol_syslog("DoliSCAN: Recherche du paiement dolibarr par defaut pour ce fournisseur");
	return array($fourn->fk_account, $fourn->mode_reglement_supplier_id);
}


/**
 * flatTvaArray : ne garde que les entrées non nulles
 *                note: passe les clés en (string) pour eviter le bug de 5.5 -> 5
 *
 * @param  mixed $l
 * @return array
 */
function flatTvaArray($l)
{
	$t = array();
	if ($l->tvaVal1 > 0) {
		$k = (string) $l->tvaTx1;
		$t[$k] = round($l->tvaVal1,2);
		// dol_syslog("DoliSCAN: flatTvaArray 1 :  key=$k(" . $l->tvaTx1 . "), val=" . $l->tvaVal1);
	}
	if ($l->tvaVal2 > 0) {
		$k = (string) $l->tvaTx2;
		$t[$k] = round($l->tvaVal2,2);
		// dol_syslog("DoliSCAN: flatTvaArray 2 :  key=$k(" . $l->tvaTx2 . "), val=" . $l->tvaVal2);
	}
	if ($l->tvaVal3 > 0) {
		$k = (string) $l->tvaTx3;
		$t[$k] = round($l->tvaVal3,2);
		// dol_syslog("DoliSCAN: flatTvaArray 3 :  key=$k(" . $l->tvaTx3 . "), val=" . $l->tvaVal3);
	}
	if ($l->tvaVal4 > 0) {
		$k = (string) $l->tvaTx4;
		$t[$k] = round($l->tvaVal4,2);
		// dol_syslog("DoliSCAN: flatTvaArray 4 :  key=$k(" . $l->tvaTx4 . "), val=" . $l->tvaVal4);
	}

	//Pour avoir le calcul inverse de la tva qui "favorise" les montants les plus importants
	//(ne change pas le montant HT et TTC mais la ventilation dans dolibarr)
	arsort($t);
	return $t;
}


/**
 * recherche d'une facture fournisseur en doublon
 * TODO pour l'instant on ne fait rien partant du principe que l'import précédent a bien importé la ligne mais
 * il faudra faire un update probablement
 *
 * @var [type]
 * @return FactureFournisseur|null
 */
function doublon_fac_fournisseur($l, $user)
{
	global $db, $conf, $entity;
	dol_syslog("DoliSCAN: recherche d'un doublon de facture fournisseur pour : " . $l->label);
	$facid = null;
	$num = null;

	//Nouvelle méthode : on s'appuie sur le champ import_key et on lui affecte l'id du frais doliscan
	$uniqueID = $l->id;
	$date_update = dolibarr_get_const($db, "DOLISCAN_UPDATE_1_7_10", $entity);
	if ($date_update == 0) {
		$date_update = date("Y-m-d");
		$result = dolibarr_set_const($db, "DOLISCAN_UPDATE_1_7_10", date('Y-m-d'), 'chaine', 0, '', $entity);
	}

	if ($uniqueID > 0) {
		$sqlUnique  = "SELECT rowid FROM " . MAIN_DB_PREFIX . "facture_fourn WHERE import_key='" . $uniqueID . "' AND `datec` >= '" . $date_update . "' LIMIT 1";
		$resqlUnique = $db->query($sqlUnique);
		if ($resqlUnique) {
			$num = $db->num_rows($resqlUnique);
			dol_syslog("DoliSCAN:   Recherche d'un doublon de fact.fournisseur pour import_key $uniqueID ...");
			if ($num > 0) {
				$obj = $db->fetch_object($resqlUnique);
				$facid = $obj->rowid;
				dol_syslog("DoliSCAN:   doublon de fact.fournisseur trouvé: $facid ...");
			} else {
				dol_syslog("DoliSCAN:   doublon de fact.fournisseur pas trouvé, on passe à l'ancienne recherche");
				$num = null;
			}
		}
	}

	if (null === $num) {
		//Ancien moyen de trouver un doublon la ref était ladate:ttc
		//Problème: par exemple pour le peage on peut avoir 2 factures du même jour avec la meme ref ...
		//autre problème on a des utilisateurs qui modifient la référence fournisseur, d'ou passage sur le champ import_key
		$ref = str_replace('-', '', $l->ladate) . ":" . $l->ttc;
		dol_syslog("DoliSCAN:   Recherche d'un doublon fact.fournisseur sur ancienne ref $ref ...");
		$sql = "SELECT rowid FROM " . MAIN_DB_PREFIX . "facture_fourn WHERE ref_supplier='" . $ref . "' AND `datec` < '" . $date_update . "' LIMIT 1";
		$resql = $db->query($sql);
		if ($resql) {
			$num = $db->num_rows($resql);
			if ($num > 0) {
				$obj = $db->fetch_object($resql);
				$facid = $obj->rowid;
				dol_syslog("DoliSCAN:   doublon fact.fournisseur ancienne ref trouvé: $facid ...");
			} else {
				dol_syslog("DoliSCAN:   doublon fact.fournisseur ancienne ref non trouvé");
				$num = null;
			}
		}
	}

	if (null === $num) {
		//Autre moyen de rechercher un doublon si l'utilisateur a créé la facture dans dolibarr
		$ref = str_replace('-', '', $l->ladate) . "/" . $l->id;
		dol_syslog("DoliSCAN:   Recherche d'un doublon fact.fournisseur sur ref automatique doliscan $ref ...");
		$sql = "SELECT rowid FROM " . MAIN_DB_PREFIX . "facture_fourn WHERE ref_supplier='" . $ref . "'";
		$resql = $db->query($sql);
		if ($resql) {
			$num = $db->num_rows($resql);
			if ($num > 0) {
				$obj = $db->fetch_object($resql);
				$facid = $obj->rowid;
				dol_syslog("DoliSCAN:   doublon fact.fournisseur sur ref automatique trouvé: $facid ...");
			} else {
				dol_syslog("DoliSCAN:   doublon fact.fournisseur sur ref automatique non trouvé");
				$num = null;
			}
		}
	}
	$facfou = null;
	if(!empty($facid)) {
		$facfou = new FactureFournisseur($db);
		$facfou->fetch($facid);
	}

	dol_syslog("DoliSCAN:   doublon fact.fournisseur return $facid");
	return $facfou;
}

/**
 * factorisation de code pour create_fact_fournisseur_carbu et create_fact_fournisseur
 *
 * @param   [type]  $l     [$l description]
 * @param   [type]  $user  [$user description]
 * @param   [type]  $ndf   [$ndf description]
 *
 * @return  FactureFournisseur         [return description]
 */
function create_fact_fournisseur_common($l, $user, $ndf)
{
	global $db, $conf, $langs;
	// dol_syslog("DoliSCAN: ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
	dol_syslog("DoliSCAN: Création d'une facture fournisseur pour : " . json_encode($l));
	// dol_syslog("DoliSCAN: Création d'une facture fournisseur pour : " . json_encode($user));exit;
	// dol_syslog("DoliSCAN: ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

	$fournisseur = new Societe($db);
	$fournisseurID = fournisseurIdfromSlug($l->type_frais->slug);
	$fetchRes = $fournisseur->fetch($fournisseurID);
	if ($fetchRes <= 0) {
		dol_syslog("DoliSCAN: Erreur de chargement du fournisseur pour " . $l->type_frais->slug, LOG_ERR);
	}

	$facfou = new FactureFournisseur($db);
	$ref = str_replace('-', '', $l->ladate) . "/" . $l->id;

	list($banqueidDS, $paiementidDS) = paiementIdFromSlug($l->moyen_paiement->slug, $user);

	//priorité au moyen de paiement par défaut pour ce fournisseur ?
	list($banqueid, $paiementid) = paiementIdFromSupplier($fournisseur);

	if($banqueid <= 0) {
		$banqueid = $banqueidDS;
	}

	if($paiementid <= 0) {
		$paiementid = $paiementidDS;
	}

	$facfou->ref = $ref; //sera remplacee par dolibarr
	$facfou->ref_supplier = $ref; //La ref pour eviter les doublons
	$facfou->socid = $fournisseurID;
	$facfou->thirdparty = $fournisseur;
	$facfou->label = $l->label;
	$facfou->date = strtotime($l->ladate);
	$facfou->date_echeance = strtotime($l->ladate);
	$facfou->note_public = '';
	$facfou->fk_account = $banqueid;
	$facfou->note_private = $langs->trans("doliscanFromUser", $ndf->label, $user->firstname . " " . $user->lastname);
	$facfou->setPaymentMethods($paiementid);
	$facid = $facfou->create($user);

	if ($facid > 0) {
		updateFacFouImportKey($facid, $l->id);
	} else {
		$facfou = null;
	}

	return $facfou;
}

/**
 * creation d'une facture fournisseur spéciale carburant
 * voir https://doc.cap-rel.fr/doliscan-dolibarr/gestion_des_regles_fiscales
 *
 * @param   [type]  $l     [$l description]
 * @param   [type]  $user  [$user description]
 * @param   [type]  $ndf   [$ndf description]
 *
 * @return  [type]         [return description]
 */
function create_fact_fournisseur_carbu($l, $user, $ndf)
{
	global $db, $conf, $langs, $mysoc;

	$nberror = 0;

	//double check
	if (dsbackport_getDolGlobalString('DOLISCAN_ENABLE_FRENCH_CARBU_RULES','') == '' || ($l->type_frais->slug != "carburant")) {
		return null;
	}

	$facfou = create_fact_fournisseur_common($l, $user, $ndf);
	if (empty($facfou)) {
		return -1;
	}

	$fournisseur = new Societe($db);
	$fournisseur->fetch($facfou->socid);

	//une seule ligne si tva 100% recup
	if ($l->vehicule->type == 'vu') {
		dol_syslog("DoliSCAN:  Fact. fournisseur carburant 100% récup TVA");
		$fk_product = $conf->global->DOLISCAN_ENABLE_FRENCH_CARBU_100_PERCENT;
		$detailtsProduit = new Product($db);
		$typeProductOrService = 0; //par défaut produit ...
		if ($detailtsProduit->fetch($fk_product)) {
			//mais plus propre d'aller chercher dans dolibarr
			$typeProductOrService = $detailtsProduit->type;
		}

		$label = '<strong>' . ndf_label($l) . '</strong>';
		$label .= "<p>Véhicule : " . $l->vehicule->number . ", " . $l->vehicule->name . ", " . $l->vehicule->power . ", carburant: " . $l->vehicule->energy . "</p>";
		$label .= "<p>Règle fiscale appliquée : 100% de récupération de la TVA (catégorie de véhicule: utilitaire).</p>";
		$amount = ds_nf($l->ttc);
		$qty = '1';
		$price_base = 'TTC';
		$tva_tx = get_default_tva($mysoc, $mysoc);
		$remise_percent = 0;
		$ret = $facfou->addline($label, $amount, $tva_tx, 0, 0, $qty, $fk_product, $remise_percent, '', '', 0, '', $price_base, $typeProductOrService, -1, false, 0, null, 0, 0);
		if ($ret < 0) {
			dol_syslog("DoliSCAN:  Fact. fournisseur error sur l'ajout de la ligne 100% recup tva", LOG_ERR);
			$nberror--;
		}
	} elseif ($l->vehicule->type == 'vp') {
		dol_syslog("DoliSCAN:  Fact. fournisseur carburant 80% récup TVA");
		//deux lignes si tva 80% recup
		$fk_product = $conf->global->DOLISCAN_ENABLE_FRENCH_CARBU_80_PERCENT;
		$detailtsProduit = new Product($db);
		$typeProductOrService = 0; //par défaut produit ...
		if ($detailtsProduit->fetch($fk_product)) {
			//mais plus propre d'aller chercher dans dolibarr
			$typeProductOrService = $detailtsProduit->type;
		}
		$tva_tx = get_default_tva($mysoc, $mysoc);
		dol_syslog("DoliSCAN:  taux de tva a appliquer " . $tva_tx);

		//calcul du montant de la ligne qui correspond a 80% de TVA
		$ht = ds_nf($l->ttc) / ($tva_tx/100+1);
		$tva = ds_nf($l->ttc) - $ht;
		$eightyPercent = ($tva * 80 / 100);
		$htForThatVAT = $eightyPercent / ($tva_tx/100);
		dol_syslog("DoliSCAN:  Fact. fournisseur carburant 80% récup TVA, TVA = $tva, 80% de TVA = $eightyPercent, il faut donc une ligne de $htForThatVAT");

		$label = '<strong>' . ndf_label($l) . '</strong>';
		$label .= "<p>Véhicule : " . $l->vehicule->number . ", " . $l->vehicule->name . ", " . $l->vehicule->power . ", carburant: " . $l->vehicule->energy . "</p>";
		$label .= "<p>Règle fiscale appliquée : 80% de récupération de la TVA (catégorie de véhicule: tourisme).</p>";
		$amount = $htForThatVAT;
		$amountTTCfistLine = $htForThatVAT * (1 + $tva_tx/100);
		$qty = '1';
		$price_base = 'HT';
		$remise_percent = 0;
		$ret = $facfou->addline($label, $amount, $tva_tx, 0, 0, $qty, $fk_product, $remise_percent, '', '', 0, '', $price_base, $typeProductOrService, -1, false, 0, null, 0, 0);
		if ($ret < 0) {
			dol_syslog("DoliSCAN:  Fact. fournisseur error sur l'ajout de la ligne 80% recup tva", LOG_ERR);
			$nberror--;
		}

		//La 2° ligne : sans TVA pour avoir le total correct
		$fk_product = $conf->global->DOLISCAN_ENABLE_FRENCH_CARBU_0_PERCENT;
		$detailtsProduit = new Product($db);
		$typeProductOrService = 0; //par défaut produit ...
		if ($detailtsProduit->fetch($fk_product)) {
			//mais plus propre d'aller chercher dans dolibarr
			$typeProductOrService = $detailtsProduit->type;
		}

		$label = '<strong>' . ndf_label($l) . '</strong>';
		$label .= "<p>Note: TVA récupérée partiellement.</p>";
		$amount = ds_nf($l->ttc) - $amountTTCfistLine;
		$qty = '1';
		$price_base = 'TTC';
		$remise_percent = 0;
		$tva_tx = 0;
		dol_syslog("DoliSCAN:  Fact. fournisseur carburant 80% récup TVA, ajout de la ligne à zéro de faut donc une ligne de $amount");
		$ret = $facfou->addline($label, $amount, $tva_tx, 0, 0, $qty, $fk_product, $remise_percent, '', '', 0, '', $price_base, $typeProductOrService, -1, false, 0, null, 0, 0);
		if ($ret < 0) {
			dol_syslog("DoliSCAN:  Fact. fournisseur error sur l'ajout de la ligne 0% recup tva", LOG_ERR);
			$nberror--;
		}
	}
	if ($nberror == 0) {
		return $facfou;
	}
	return $nberror;
}


 /**
  * create_fact_fournisseur : création de la facture fournisseur
  *
  * @param   [type]  $l     ligne de frais
  * @param   [type]  $user  utilisateur
  * @param   [type]  $ndf   note de frais
  *
  * @return  [type]         [return description]
  */
function create_fact_fournisseur($l, $user, $ndf)
{
	global $db, $conf, $langs;
	// dol_syslog("DoliSCAN: ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
	dol_syslog("DoliSCAN: Création d'une facture fournisseur pour : " . json_encode($l));
	// dol_syslog("DoliSCAN: Création d'une facture fournisseur pour : " . json_encode($user));exit;
	// dol_syslog("DoliSCAN: ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

	$r = new stdClass();
	$facid = null;
	$nberror = 0;

	//Ancien moyen de création d'une référence fournisseur: date:ttc
	//modification sept 2021: date/doliscan_id
	$ref = str_replace('-', '', $l->ladate) . "/" . $l->id;
	$facfou = doublon_fac_fournisseur($l, $user);
	if(is_object($facfou)) {
		$facid = $facfou->id;
	}

	//cas très particulier du carburant
	if ($facid === null && $conf->global->DOLISCAN_ENABLE_FRENCH_CARBU_RULES && $l->type_frais->slug == "carburant") {
		$facfou = create_fact_fournisseur_carbu($l, $user, $ndf);
		if (is_object($facfou)) {
			$facid = $facfou->id;
			dol_syslog("DoliSCAN: Création d'une facture fournisseur carburant id = " . $facid);
		} else {
			$nberror++;
		}
	}

	if ($facid === null) {
		$facfou = create_fact_fournisseur_common($l, $user, $ndf);
		if (empty($facfou)) {
			return -1;
		}

		$facid = $facfou->id;

		$fk_product = null;
		$typeProductOrService = 1; //service = 1

		if (dsbackport_getDolGlobalString('DOLISCAN_USE_DEFAULT_PRODUCT_FROM_SCANINVOICES','') != '') {
			if (dol_include_once('/scaninvoices/class/settings.class.php')) {
				//Le produit par défaut s'il est configuré
				if(class_exists('\modules\scaninvoices\Settings')) {
					/** @phpstan-ignore-next-line */
					$defaultproduct = new modules\scaninvoices\Settings($db);
				} else {
					/** @phpstan-ignore-next-line */
					$defaultproduct = new Settings($db);
				}
				$resultDefProAll = $defaultproduct->fetchAll('', '', 0, 0, array('customsql'=>"t.fk_soc=".$facfou->socid));
				$defaultproduct = reset($resultDefProAll);
				if ($resultDefProAll && !empty($defaultproduct->fk_default_product)) {
					$fk_product = $defaultproduct->fk_default_product;
					if ($fk_product > 0) {
						$detailtsProduit = new Product($db);
						if ($detailtsProduit->fetch($fk_product)) {
							$typeProductOrService = $detailtsProduit->type;
						}
					}
					dol_syslog('doliSCAN produit/service par défaut (from scaninvoices supplier) id='.$fk_product);
				} elseif (dsbackport_getDolGlobalString('SCANINVOICES_DEFAULT_PRODUCT','') != '') {
					$fk_product = dsbackport_getDolGlobalString('SCANINVOICES_DEFAULT_PRODUCT');
					dol_syslog('doliSCAN produit/service par défaut (from scaninvoices supplier) id='.$fk_product);
				} else {
					dol_syslog('doliSCAN pas de produit/service par défaut pour ce fournisseur (voir conf de scaninvoices)');
				}
			}
		}

		$tvaTab = flatTvaArray($l);
		//Pas de TVA
		if (count($tvaTab) == 0) {
			$label = '<strong>' . ndf_label($l) . '</strong>';
			$amount = ds_nf($l->ttc);
			$qty = '1';
			$price_base = 'HT';
			$tva_tx = '0';
			$remise_percent = 0;
			$ret = $facfou->addline($label, $amount, $tva_tx, 0, 0, $qty, $fk_product, $remise_percent, '', '', 0, '', $price_base, $typeProductOrService, -1, false, 0, null, 0, 0);
			if ($ret < 0) {
				$nberror++;
			}
		} else {
			//Un taux de tva
			if (count($tvaTab) == 1) {
				foreach ($tvaTab as $tx => $tva) {
					dol_syslog("DoliSCAN:  Fact. fournisseur avec un seul taux de TVA : $tx / $tva");
					$label = '<strong>' . ndf_label($l) . '</strong>';
					$amount = ds_nf($l->ht);
					$qty = '1';
					$price_base = 'HT';
					$tva_tx = vatrate($tx);
					$remise_percent = 0;
					$ret = $facfou->addline($label, $amount, $tva_tx, 0, 0, $qty, $fk_product, $remise_percent, '', '', 0, '', $price_base, $typeProductOrService, -1, false, 0, null, 0, 0);
					if ($ret < 0) {
						$nberror++;
					}
				}
			} else {
				//plusieurs taux
				//le pb c'est qu'à force d'utiliser des arrondis les totaux sont faux :-(
				// on fait donc tourner l'algo pour ensuite aller récupérer les centimes !
				$nbtaux = 0;
				$grandTotalTTC = 0;
				//Si multi taux de tva
				foreach ($tvaTab as $tx => $tva) {
					//On a tx=10% et tva=6.67€ il faut remettre le TTC / HT ad hoc
					$ht = round($tva / $tx * 100, 2);
					$newtva = round($ht * $tx / 100, 2);
					$ttc = round($ht + $newtva, 2);
					$grandTotalTTC += $ttc;
					$nbtaux++;
					dol_syslog("DoliSCAN:  Facture avec Multi-TVA : $tx / $tva => HT=$ht NEWTVA=$newtva || TTC=$ttc");
					//Dernière chance de corriger la tva
					if ($nbtaux == count($tvaTab)) {
						$ttcTicket = $l->ttc;
						if ($ttcTicket != $grandTotalTTC) {
							$correctif = $grandTotalTTC - $ttcTicket;
							dol_syslog("DoliSCAN:  -> Dernière ligne avec Multi-TVA correctif = $correctif");
							$ttc -= $correctif;
							$ht = round($ttc / (1 + $tx / 100), 2);
							$newtva = round($ht * $tx / 100, 2);
							dol_syslog("DoliSCAN:  -> Dernière ligne avec Multi-TVA : HT corrigé=$ht TTC corrigé=$ttc");
						}
					}

					//#23: verification que le total ttc est bien identique au montant ttc du justificatif
					if($l->ttc != $grandTotalTTC) {
						dol_syslog("DoliSCAN:  -> ttc du justificatif ($l->ttc) différent du ttc dolibarr ($grandTotalTTC) !");
					}

					$label = '<strong>' . ndf_label($l) . "</strong>, Total Facture $l->ttx - Partie de la facture avec taux de TVA à $tx%";
					$amount = ds_nf($ht);
					$qty = '1';
					$price_base = 'HT';
					$tva_tx = vatrate($tx);
					$remise_percent = 0;
					$ret = $facfou->addline($label, $amount, $tva_tx, 0, 0, $qty, $fk_product, $remise_percent, '', '', 0, '', $price_base, $typeProductOrService, -1, false, 0, null, 0, 0);
					if ($ret < 0) {
						$nberror++;
					}
				}

				if($l->ttc != $grandTotalTTC) {
					dol_syslog("DoliSCAN:  -> ttc du justificatif ($l->ttc) différent du ttc dolibarr ($grandTotalTTC) !", LOG_WARNING);
					//TODO : ajout d'une ligne pour corriger le centime d'erreur ?
				}

			}
		}
	}

	if ($facid != null) {
		//A voir si on fait aussi le reglement ou pas...
		//Si la facture n'est pas déjà marquée payée
		if (dsbackport_getDolGlobalString('DOLISCAN_PRO_AUTO_PAIEMENT_ENABLED','') != '') {
			if($facfou->paye == 1) {
				dol_syslog("DoliSCAN: Facture fournisseur déjà payée, ajout du paiement automatique actif mais débrayé pour ce frais");
			}
			else {
				$remainToPay = $facfou->getRemainToPay();
				dol_syslog("DoliSCAN: Facture fournisseur reste a payer = " . $remainToPay);
				if($remainToPay > 0.5) {
					dol_syslog("DoliSCAN: Facture fournisseur reste a payer > 0.5 ajout du paiement");
					list($banqueid, $paiementid) = paiementIdFromSlug($l->moyen_paiement->slug, $user);
					$facfou->validate($user);
					$reglement = new PaiementFourn($db);
					$reglement->amounts = [$facid => $l->ttc];
					$reglement->date = strtotime($l->ladate);
					$reglement->datepaye = strtotime($l->ladate);
					$reglement->fk_account = $banqueid;
					$reglement->paiementid = $paiementid;
					$reglementid = $reglement->create($user, 1, $user->socid);
					if ($reglementid && !empty($conf->banque->enabled)) {
						$result = $reglement->addPaymentToBank($user, 'payment_supplier', '(SupplierInvoicePayment)', $banqueid, '', '');
					}
				} else {
					dol_syslog("DoliSCAN: Facture fournisseur déjà payée, ajout du paiement automatique actif mais évite les doublons pour ce cas particulier");
				}
				// $facfou->setPaid($user);
			}
		}

		if ($nberror) {
			$r->error = "erreur";
			$r->message = "<li>Erreur de création de la facture fournisseur $l->label du $l->ladate, code d'erreur :" . $facfou->error . "</li>";
			dol_syslog("DoliSCAN: Erreur de création de la facture fournisseur $l->label du $l->ladate, code d'erreur :" . $facfou->error, LOG_WARNING);
		} else {
			$url = $facfou->getNomUrl(1, '', '', '', '', 0, 0, 0);
			$r->error = "";
			$r->message = "<li>Création de la facture fournisseur $l->label du $l->ladate réussie -> $url</li>\n";
		}
	}

	if($nberror != 0) {
		$db->rollback();
		$r->error = "erreur";
		$r->message = "<li>Erreur de création de la facture fournisseur $l->label du $l->ladate, cette facture semble déjà avoir été importée. Code d'erreur :" . $facfou->error . " | $nberror</li>";
		dol_syslog("DoliSCAN: Erreur de création de la facture fournisseur $l->label du $l->ladate, cette facture semble déjà avoir été importée. Code d'erreur :" . $facfou->error . " | $nberror", LOG_WARNING);
	} else {
		$db->commit();
	}


	//Si pas d'erreur pour la création de la fact. fournisseur on essaye de joindre le justificatif
	dol_syslog("DoliSCAN: création de la facture fournisseur $l->label du $l->ladate ok, tentative pour joindre le justificatif...");
	if ($nberror == 0) {
		if ($facid && is_object($facfou)) {
			$ref = dol_sanitizeFileName($facfou->ref);
			$upload_dir = $conf->fournisseur->facture->dir_output . '/' . get_exdir($facfou->id, 2, 0, 0, $facfou, 'invoice_supplier') . $ref;

			if (!is_dir($upload_dir)) {
				dol_mkdir($upload_dir);
			}

			if (is_dir($upload_dir) && ($l->fileName != "")) {
				$uri = "/ldfImages/" . $user->email . "/" . $l->fileName;
				$ext = pathinfo($l->fileName, PATHINFO_EXTENSION);
				//$dest = $upload_dir . "/" . $facfou->ref_supplier . ".$ext";
				//20230724 - ajout du prefixe ref
				$dest = $upload_dir . "/" . $ref . '-' . str_replace('...', '', $l->fileName);
				// file_put_contents($file_name, file_get_contents($url));
				$r->message .= "<ul><li>Téléchargement du document justificatif <b>" . basename($dest) . "</b>  : ";

				$mustDownloadFile = true;
				if (file_exists($dest)) {
					if(sha1_file($dest) == $l->fileCheck) {
						dol_syslog("DoliSCAN: download_fichier $dest : bingo sha1 identique ça ne sert à rien de le re-télécharger !");
						$mustDownloadFile = false;
					} else {
						dol_syslog("DoliSCAN: download_fichier $dest existe mais sha1 différent, re téléchargement du fichier");
					}
				}
				if($mustDownloadFile) {
					$ret = download_fichier($uri, $dest, $user, $facfou, "invoice_supplier");
					$r->message .= $ret->message;
					$r->error .= $ret->error;
				}
				$r->message .= "</li></ul>";
			}
		}

		//puis association au projet
		if (!empty($conf->projet->enabled) && isset($l->tags_frais)) {
			//doliscan peut associer plusieurs tags à un frais ... dolibarr un seul
			$tagdoliscan = reset($l->tags_frais);
			$project = new Project($db);
			$res = $project->fetch('', $tagdoliscan->code);
			if ($res) {
				dol_syslog("DoliSCAN: association de projet par le tag " . json_encode($tagdoliscan));
				$facfou->setProject($project->id);
			}
		}

		//TODO creation du lien avec le doliscan correspondant
	}

	return $r;
}

/** remove old "last line of LDF"
 *
 */
function remove_last_line_ndf($id)
{
	global $db;
	if ($id > 0) {
		$sql = "DELETE FROM " . MAIN_DB_PREFIX . "expensereport_det WHERE fk_expensereport='$id' AND comments='Fin de la note de frais'";
		$resql = $db->query($sql);
		if ($resql) {
			dol_syslog("DoliSCAN: remove last line ok");
		}
	}
}

/**
 * add last line to ndf
 *
 * @return  [type]  [return description]
 */
function add_last_line_ndf($id, $socid, $ladate)
{
	if ($id <= 0 || $id === null) {
		dol_syslog("DoliSCAN: Ajout de la derniere ligne de NDF ERROR id is null or bad value : $id");
		return -1;
	}

	global $db, $conf;
	$r = new stdClass();
	$fraisID = fraisIdfromSlug('divers');

	dol_syslog("DoliSCAN: Ajout de la derniere ligne de NDF ... $id : $socid : $ladate");
	$newndfline = new ExpenseReportLine($db);
	$newndfline->fk_expensereport = $id;
	$newndfline->fk_c_type_fees = $fraisID;
	$newndfline->fk_project = null;
	$newndfline->fk_soc = $socid;
	$newndfline->vatrate = '0';
	$newndfline->vat_src_code = null;
	$newndfline->comments = "Fin de la note de frais";
	$newndfline->qty = 1;
	$newndfline->value_unit = '0';
	$newndfline->total_ht = '0';
	$newndfline->total_ttc = '0';
	$newndfline->total_tva = '0';
	$newndfline->date = strtotime($ladate);
	$newndfline->rule_warning_message = null;
	$newndfline->fk_c_exp_tax_cat = null;
	$newndfline->fk_ecm_files = null;
	$result = $newndfline->insert();
	if ($result < 0) {
		dol_syslog("DoliSCAN:   Erreur d'ajout de la derniere ligne de NDF ..." . $newndfline->error);
	}

	return $r;
}

/**
 * add_line_ndf ajoute une ligne sur la note de frais $id
 *
 * @param   [type]  $id      id de la note de frais
 * @param   [type]  $l       ligne de frais doliscan
 * @param   [type]  $ndfref  ref de la note de frais
 * @param   [type]  $user    user
 *
 * @return  [type]           [return description]
 */
function add_line_ndf($id, $l, $ndfref, $user)
{
	global $db, $conf, $mysoc, $entity;
	$r = new stdClass();

	$downloadPiece = null;
	$idProject = null;
	$idFichierJustif = null;

	//print "Ajout d'une ligne sur la NDF : " . $l->label . " et justificatif " . $l->fileName . "<br />";
	dol_syslog("DoliSCAN: Ajout d'une ligne sur la NDF ...");
	// dol_syslog(json_encode($l));
	//Verification du doublon ... le hic c'est qu'on peut très bien avoir fait 2 fois la même dépense (date/montant/intitulé)
	//le même jour (exemple péage, resto etc.) -> on utilise import_key pour ne pas importer 2 fois la même ligne
	$uniqueID = $l->id;
	$fraisID = fraisIdfromSlug($l->type_frais->slug);

	$date_update = dolibarr_get_const($db, "DOLISCAN_UPDATE_1_7_10", $entity);
	if ($date_update == 0) {
		$date_update = date("Y-m-d");
		$result = dolibarr_set_const($db, "DOLISCAN_UPDATE_1_7_10", date('Y-m-d'), 'chaine', 0, '', $entity);
	}

	dol_syslog("DoliSCAN: [#$fraisID/$uniqueID]  Recherche d'un doublon de LDF pour import_key=$uniqueID ...");

	$sqlUnique  = "SELECT rowid FROM " . MAIN_DB_PREFIX . "expensereport_det WHERE import_key='" . $uniqueID . "' LIMIT 1";
	$resqlUnique = $db->query($sqlUnique);
	if ($resqlUnique) {
		$num = $db->num_rows($resqlUnique);
		if ($num > 0) {
			dol_syslog("DoliSCAN: [#$fraisID/$uniqueID]  doublon de LDF trouvé, early return");
			//TODO pour l'instant on ne fait rien partant du principe que l'import précédent a bien importé la ligne mais
			//il faudrait penser à faire un update
			return;
		} else {
			dol_syslog("DoliSCAN: [#$fraisID/$uniqueID]  pas de doublon de LDF trouvé pour cet id");
			//on essaye de voir s'il y a un doublon si on était sur l'ancien systeme
			//dans le cas ou la date du frais serait < date de passage du plugin en version 1_7_10
			$dateFrais = new DateTime($l->ladate);
			$dateNewUnique = new DateTime($date_update);
			if ($dateFrais < $dateNewUnique) {
				dol_syslog("DoliSCAN: [#$fraisID/$uniqueID]  ce frais date d'avant l'installation du module doliscan > 1.7.10 on cherche donc si ce frais est unique sur l'ancien système");
				//note: c'était pas top du fait qu'on peut avoir le même montant plusieurs fois le même jour: exemple un ticket de péage aller / retour
				$sqlUnique  = "SELECT rowid FROM " . MAIN_DB_PREFIX . "expensereport_det WHERE date='" . $l->ladate . "' AND total_ttc='" . $l->ttc . "' AND fk_c_type_fees = '" . $fraisID . "' AND comments = '" . ndf_label($l) . "' LIMIT 1";
				$resqlUnique = $db->query($sqlUnique);
				if ($resqlUnique) {
					$num = $db->num_rows($resqlUnique);
					if ($num > 0) {
						dol_syslog("DoliSCAN: [#$fraisID/$uniqueID]  doublon de LDF trouvé (ancien modèle), early return");
						return;
					}
				}
				//AND `date` >= '" . $date_update .  "'
			}
		}
	}

	//association de la ligne de frais à un projet dolibarr
	if (!empty($conf->projet->enabled) && isset($l->tags_frais)) {
		//doliscan peut associer plusieurs tags à un frais ... dolibarr un seul
		$tagdoliscan = reset($l->tags_frais);
		$project = new Project($db);
		$res = $project->fetch('', $tagdoliscan->code);
		if ($res) {
			dol_syslog("DoliSCAN: association de projet par le tag " . json_encode($tagdoliscan));
			$idProject = $project->id;
		}
	}

	//télécharge le justificatif
	$ref = dol_sanitizeFileName($ndfref);
	$upload_dir = $conf->expensereport->dir_output . '/' . $ref;
	if (!is_dir($upload_dir)) {
		dol_mkdir($upload_dir);
	}

	if (is_dir($upload_dir) && ($l->fileName != "")) {
		$uri = "/ldfImages/" . $user->email . "/" . $l->fileName;
		$ext = pathinfo($l->fileName, PATHINFO_EXTENSION);
		$dest = $upload_dir . "/" . str_replace('...', '', $l->fileName);
		if (file_exists($dest)) {
			dol_syslog("DoliSCAN: add_line_ndf do not (re)download file $dest");
		} else {
			$ndf = new ExpenseReport($db);
			$ndf->fetch($id);
			$downloadPiece = download_fichier($uri, $dest, $user, $ndf, "expensereport");
			if ($downloadPiece->error !== null) {
				$r->message .= "<li>" . $l->fileName . " pas téléchargé (" . $downloadPiece->message . ")</li>";
				$idFichierJustif = null;
			} else {
				$idFichierJustif = $downloadPiece->ecmID;
				$r->message .= "<li>" . $downloadPiece->message . " pour " . $l->fileName . " -> id fichier $idFichierJustif</li>";
			}
		}
	}

	$tvaTab = flatTvaArray($l);
	dol_syslog("DoliSCAN: [#$fraisID/$uniqueID] data : " . json_encode($l));
	dol_syslog("DoliSCAN: [#$fraisID/$uniqueID] Ligne ttc= " . ds_nf($l->ttc) . " avec TVA : " . json_encode($tvaTab));

	// print(json_encode($l) . "\n");
	$label = ndf_label($l);

	//Cas particulier du carburant si option activée de gestion spéciale france : vu simple
	if ($conf->global->DOLISCAN_ENABLE_FRENCH_CARBU_RULES && $l->type_frais->slug == "carburant") {
		$tva_tx = get_default_tva($mysoc, $mysoc);
		dol_syslog("DoliSCAN: [#$fraisID/$uniqueID] Carburant, on applique la TVA par defaut pour ce pays : $tva_tx");

		if ($l->vehicule->type == 'vu') {
			dol_syslog("DoliSCAN:  Carburant 100% récup TVA");
			$label = '<strong>' . ndf_label($l) . '</strong>';
			$label .= "<p>Véhicule : " . $l->vehicule->number . ", " . $l->vehicule->name . ", " . $l->vehicule->power . ", carburant: " . $l->vehicule->energy . "</p>";
			$label .= "<p>Règle fiscale appliquée : 100% de récupération de la TVA (catégorie de véhicule: utilitaire).</p>";
			$tvaTab[$tva_tx] = $l->ttc / ($tva_tx / 100 + 1);
		}

		if ($l->vehicule->type == 'vp') {
			//code special qui vise uniquement a passer sur l'option 1 ci apres
			//dans laquelle la specificité des 2 lignes est implémentée
			$tvaTab[99] = 99;
		}
	}


	//Pas de TVA
	if (count($tvaTab) == 0) {
		dol_syslog("DoliSCAN: [#$fraisID/$uniqueID] Ligne sans TVA");
		$newndfline = new ExpenseReportLine($db);
		$newndfline->fk_expensereport = $id;
		$newndfline->fk_c_type_fees = $fraisID;
		$newndfline->fk_project = $idProject;
		$newndfline->fk_soc = $user->socid;
		$newndfline->vatrate = '0';
		$newndfline->vat_src_code = null;
		$newndfline->comments = $label;
		$newndfline->qty = 1;
		$newndfline->value_unit = ds_nf($l->ttc);
		$newndfline->total_ht = ds_nf($l->ttc);
		$newndfline->total_ttc = ds_nf($l->ttc);
		$newndfline->total_tva = '0';
		$newndfline->date = strtotime($l->ladate);
		$newndfline->rule_warning_message = null;
		$newndfline->fk_c_exp_tax_cat = null;
		$newndfline->fk_ecm_files = $idFichierJustif;

		//Cas particulier du carburant - dolibarr calcule mal le total ttc à rembourser au salarié
		//alors on force le de taux de tva au taux par défaut dans cette zone
		if (dsbackport_getDolGlobalString('DOLISCAN_ENABLE_FRENCH_CARBU_RULES','') != '' && $l->type_frais->slug == "carburant") {
			$tva_tx = get_default_tva($mysoc, $mysoc);
			dol_syslog("DoliSCAN: [#$fraisID/$uniqueID] Carburant, on applique la TVA par defaut pour ce pays : $tva_tx");
			$newndfline->vatrate = vatrate($tva_tx);
			$newndfline->total_ht = ds_nf(round(floatval($l->ttc) / (1 + $tva_tx / 100), 2));
			$newndfline->total_tva = $newndfline->total_ttc - $newndfline->total_ht;
		}

		$result = $newndfline->insert();
		if ($result < 0) {
			$r->error = "erreur";
			$r->message .= "<li>Erreur d'ajout d'une ligne sur la note de frais : $l->label du $l->ladate, code d'erreur :" . $newndfline->error . "</li>";
		} else {
			updateLdfImportKey($result, $uniqueID);
		}
	} elseif (count($tvaTab) == 1) {
		//Un seul taux de tva
		foreach ($tvaTab as $tx => $ht) {
			// print("DoliSCAN: [#$fraisID/$uniqueID] Ligne avec un taux de TVA : $tx / $ht");
			dol_syslog("DoliSCAN: [#$fraisID/$uniqueID] Ligne avec un taux de TVA : $tx / $ht");

			$newndfline = new ExpenseReportLine($db);
			$newndfline->fk_expensereport = $id;
			$newndfline->fk_c_type_fees = $fraisID;
			$newndfline->fk_project = $idProject;
			$newndfline->fk_soc = $user->socid;
			$newndfline->vat_src_code = null;
			$newndfline->comments = $label;
			$newndfline->qty = 1;
			$newndfline->value_unit = ds_nf($l->ttc);
			$newndfline->vatrate = vatrate($tx);

			//Le montant ht n'est peut-être pas donné
			if($tx == 99 && $ht == 99) {
				$newndfline->total_ht = 0;
				$newndfline->total_tva = 0;
			} else {
				$newndfline->total_ht = ds_nf($ht);
				$newndfline->total_tva = ds_nf($l->ttc - $ht);
			}
			$newndfline->total_ttc = ds_nf($l->ttc);
			$newndfline->date = strtotime($l->ladate);
			$newndfline->rule_warning_message = null;
			$newndfline->fk_c_exp_tax_cat = null;
			$newndfline->fk_ecm_files = $idFichierJustif;

			if ($conf->global->DOLISCAN_ENABLE_FRENCH_CARBU_RULES && $l->type_frais->slug == "carburant" && $l->vehicule->type == 'vp') {
				$tva_tx = get_default_tva($mysoc, $mysoc);
				dol_syslog("DoliSCAN:  taux de tva a appliquer " . $tva_tx);

				//calcul du montant de la ligne qui correspond a 80% de TVA
				$ht = ds_nf($l->ttc) / ($tva_tx/100+1);
				$tva = ds_nf($l->ttc) - $ht;
				$eightyPercent = ($tva * 80 / 100);
				$htForThatVAT = $eightyPercent / ($tva_tx/100);
				dol_syslog("DoliSCAN:  frais carburant 80% récup TVA, TVA = $tva, 80% de TVA = $eightyPercent, il faut donc une ligne de $htForThatVAT");

				$label = '<strong>' . ndf_label($l) . '</strong>';
				$label .= "<p>Véhicule : " . $l->vehicule->number . ", " . $l->vehicule->name . ", " . $l->vehicule->power . ", carburant: " . $l->vehicule->energy . "</p>";
				$label .= "<p>Règle fiscale appliquée : 80% de récupération de la TVA (catégorie de véhicule: tourisme).</p>";
				$amount = $htForThatVAT;
				$amountTTCfistLine = $htForThatVAT * (1 + $tva_tx/100);

				//modification 1ere ligne
				$newndfline->comments = $label;
				$newndfline->value_unit = $amountTTCfistLine;
				$newndfline->vatrate = vatrate($tva_tx);
				$newndfline->total_ht = $amount;
				$newndfline->total_ttc = $amountTTCfistLine;
				$newndfline->total_tva = $amountTTCfistLine - $htForThatVAT;
				// print "<p>Ajout d'une ligne (0a) Grand TTC=$l->ttc // TTC=$newndfline->total_ttc, HT=$newndfline->total_ht, TVA=" .$newndfline->total_tva . "</p>";
				$result = $newndfline->insert(1, true);

				//2° ligne
				$label = '<strong>' . ndf_label($l) . '</strong>';
				$label .= "<p>Véhicule : " . $l->vehicule->number . ", " . $l->vehicule->name . ", " . $l->vehicule->power . ", carburant: " . $l->vehicule->energy . "</p>";
				$label .= "<p>Note: partie de TVA non récupérée.</p>";

				$newndfline = new ExpenseReportLine($db);
				$newndfline->fk_expensereport = $id;
				$newndfline->fk_c_type_fees = $fraisID;
				$newndfline->fk_project = $idProject;
				$newndfline->fk_soc = $user->socid;
				$newndfline->vat_src_code = null;
				$newndfline->comments = $label;
				$newndfline->qty = 1;
				$newndfline->value_unit = ds_nf($l->ttc) - $amountTTCfistLine;
				$newndfline->vatrate = vatrate('0');

				//Le montant ht n'est peut-être pas donné
				$newndfline->total_ht = ds_nf($l->ttc) - $amountTTCfistLine;
				$newndfline->total_ttc = ds_nf($l->ttc) - $amountTTCfistLine;
				$newndfline->total_tva = ds_nf(0);
				$newndfline->date = strtotime($l->ladate);
				$newndfline->rule_warning_message = null;
				$newndfline->fk_c_exp_tax_cat = null;
				$newndfline->fk_ecm_files = $idFichierJustif;
				// print "<p>Ajout d'une ligne (0b) Grand TTC=$l->ttc // TTC=$newndfline->total_ttc, HT=$newndfline->total_ht, TVA=" .$newndfline->total_tva . "</p>";
				$result = $newndfline->insert(1, true);
			} else {
				// print "<p>Ajout d'une ligne (1) TTC=$newndfline->total_ttc, HT=$newndfline->total_ht, TVA=" .$newndfline->total_tva . "</p>";
				$result = $newndfline->insert(1, true);
			}

			if ($result < 0) {
				$r->error = "erreur";
				$r->message .= "<li>Erreur d'ajout d'une ligne sur la note de frais : $l->label du $l->ladate, code d'erreur :" . $newndfline->error . "</li>";
			} else {
				updateLdfImportKey($result, $uniqueID);
			}
		}
	} else {
		//plusieurs taux
		//le pb c'est qu'à force d'utiliser des arrondis les totaux sont faux :-(
		// on fait donc tourner l'algo pour ensuite aller récupérer les centimes
		dol_syslog("DoliSCAN: [#$fraisID/$uniqueID] multi taux de tva bascule sur computeAlgo");
		$devineHT = $l->ttc;
		$nbtaux = 0;
		foreach ($tvaTab as $tx => $tva) {
			$nbtaux++;
			$devineHT -= $tva;
		}
		$montantsHT = algoComputeTVAContrainst($tvaTab, $devineHT);
		dol_syslog("DoliSCAN: [#$fraisID/$uniqueID]  la solution est donc d'utiliser les montants ht suivants : " . json_encode($montantsHT));

		// attention il est aussi possible d'avoir une note avec une partie sans TVA (ex. taxe de séjour)
		$grandTotalTTC = 0;
		$grandTotalHT = 0;
		//Pour deviner le montant qui n'a pas de TVA on part du TTC et on retranchera la TVA au fur et a mesure
		$devineHT = $l->ttc;
		$moreComment = ""; //utilisé pour ajouter un complément de commentaire en cas de Pb TVA
		$multiTauxNormal = true;
		$nbtaux = 0;
		foreach ($tvaTab as $tx => $tva) {
			$nbtaux++;
			$ht = $montantsHT[$tx];
			//is_numeric pour eviter de considerer 0 comme empty
			if(empty($tx) || (empty($ht) && !is_numeric($ht))) {
				dol_syslog("DoliSCAN: [#$fraisID/$uniqueID tx=$tx/ht=$ht] Cas particulier, continue");
				continue;
			}
			//cas particulier "fraude a la tva / tva louche" (voir retour de algoComputeTVAContrainst)
			//$ht = -1 si pb de saisie utilisateur par exemple
			//$ht = -2 si totaux tva trop louches
			if($ht < 0) {
				$multiTauxNormal = false;
				//passage directement "à la dernière ligne"
				if($nbtaux < count($tvaTab)) {
					continue;
				}
				dol_syslog("DoliSCAN: [#$fraisID/$uniqueID tx=$tx/nbtaux=$nbtaux/count=". count($tvaTab). "ht=$ht] Cas particulier TVA trop louche, pour l'instant zéro récup de TVA");
				$moreComment = "\n";
				if($ht == -1) {
					$moreComment .= "PROBLEME DE RECALCUL DES BASES DE TVA.\n";
					$moreComment .= "Vérification manuelle de l'original nécessaire.\n";
					$moreComment .= "Désactivation de la récupération de TVA sur cette pièce.\n";
					dol_syslog("DoliSCAN: [#$fraisID/$uniqueID] Commentaire spécial ajouté (1)");
				}
				if($ht == -2) {
					$moreComment .= "PROBLEME DE FIABILITE DU TICKET.\n";
					$moreComment .= "TVA improbable, Vérification manuelle de l'original nécessaire.\n";
					$moreComment .= "Désactivation de la récupération de TVA sur cette pièce.\n";
					dol_syslog("DoliSCAN: [#$fraisID/$uniqueID] Commentaire spécial ajouté (2)");
				}

				$ht = $l->ttc;
				$ttc = $l->ttc;
				$grandTotalTTC = $ttc;
				$grandTotalHT = $ttc;
				$devineHT = $ttc;
				$tva = $newtva = $tx = 0;
			}

			//On a tx=10% et tva=6.67€ il faut remettre le TTC / HT ad hoc
			$newtva = round($ht * $tx / 100, 2);
			$ttc = round($ht + $newtva, 2);
			$grandTotalTTC += $ttc;
			$grandTotalHT += $ht;
			$devineHT -= $tva;
			dol_syslog("DoliSCAN: [#$fraisID/$uniqueID] Ligne avec Multi-TVA (" . count($tvaTab) . " taux): $tx / $tva => HT=$ht NEWTVA=$newtva || TTC=$ttc");

			//Dernier tour donc dernière chance de corriger la tva
			//Soit l'écart est minime -> erreur d'arrondis et on gere ici
			//Soit écart > 1€ et c'est probablement qu'on a une partie avec zéro de TVA
			if ($multiTauxNormal && $nbtaux == count($tvaTab)) {
				$ttcTicket = $l->ttc;
				if ($ttcTicket != $grandTotalTTC && (($ttcTicket - $grandTotalTTC) < 1)) {
					$correctif = $grandTotalTTC - $ttcTicket;
					$ttc -= $correctif;
					$ht = round($ttc / (1 + $tx / 100), 2);
					$newtva = round($ht * $tx / 100, 2);
					if ($ht < 0 || $ttc < 0) {
						dol_syslog("DoliSCAN: [#$fraisID/$uniqueID] -> Erreur de calcul, résultat négatif ht=$ht / ttc=$ttc");
					} else {
						dol_syslog("DoliSCAN: [#$fraisID/$uniqueID] -> Dernière ligne avec Multi-TVA : HT corrigé=$ht TTC corrigé=$ttc, grand ttc=$grandTotalTTC au lieu de TTC ticket=$ttcTicket");
					}
				} else {
					//Il faudra ajouter une derniere ligne pour le montant sans TVA
					$montantSansTVA = round($devineHT - $grandTotalHT,2);

					$newndfline = new ExpenseReportLine($db);
					//Possibilité de tomber sur un couac : montant négatif !
					if ($montantSansTVA  < 0) {
						dol_syslog("DoliSCAN: [#$fraisID/$uniqueID] -> Erreur de calcul pour la ligne SANS TVA : grand total HT $grandTotalHT  / grand total TTC $grandTotalTTC | devineHT=$devineHT, montantSansTVA=$montantSansTVA");
						$newndfline->comments = ndf_label($l) . " - Total facturette $l->ttc - Montant négatif, ERREUR PROBABLE DES DONNÉES D'IMPORT";
					} else {
						dol_syslog("DoliSCAN: [#$fraisID/$uniqueID] -> Il faut ajouter une ligne SANS TVA : grand total HT $grandTotalHT  / grand total TTC $grandTotalTTC | devineHT=$devineHT");
						$newndfline->comments = ndf_label($l) . " - Total facturette $l->ttc - Partie avec taux de TVA à 0%";
					}

					$newndfline->comments .= $moreComment;
					$newndfline->fk_expensereport = $id;
					$newndfline->fk_c_type_fees = $fraisID;
					$newndfline->fk_project = null;
					$newndfline->fk_soc = $user->socid;
					$newndfline->vat_src_code = null;
					$newndfline->qty = 1;
					$newndfline->value_unit = ds_nf($montantSansTVA);
					$newndfline->vatrate = vatrate('0');
					$newndfline->total_ht = ds_nf($montantSansTVA);
					$newndfline->total_ttc = ds_nf($montantSansTVA);
					$newndfline->total_tva = ds_nf(0);
					$newndfline->date = strtotime($l->ladate);
					$newndfline->rule_warning_message = null;
					$newndfline->fk_c_exp_tax_cat = null;
					$newndfline->fk_ecm_files = null;
					$newndfline->fk_ecm_files = $idFichierJustif;

					// print "<p>Ajout d'une ligne (3) TTC=$newndfline->total_ttc, HT=$newndfline->total_ht, TVA=" .$newndfline->total_tva . "</p>";
					$result = $newndfline->insert();

					if ($result < 0) {
						$r->error = "erreur";
						$r->message .= "<li>Erreur d'ajout d'une ligne sur la note de frais : $l->label du $l->ladate, code d'erreur :" . $newndfline->error . "</li>";
					} else {
						updateLdfImportKey($result, $uniqueID);
					}
				}
			}
			$newndfline = new ExpenseReportLine($db);
			$newndfline->fk_expensereport = $id;
			$newndfline->fk_c_type_fees = $fraisID;
			$newndfline->fk_project = null;
			$newndfline->fk_soc = $user->socid;
			$newndfline->vat_src_code = null;
			if ($ht < 0 || $ttc < 0) {
				$newndfline->comments = ndf_label($l) . " - Total facturette $l->ttc - Montant négatif, ERREUR PROBABLE DES DONNÉES D'IMPORT";
			} else {
				$newndfline->comments = ndf_label($l) . " - Total facturette $l->ttc - Partie avec taux de TVA à $tx%";
			}
			$newndfline->comments .= $moreComment;

			$newndfline->qty = 1;
			$newndfline->value_unit = ds_nf($ttc);
			$newndfline->vatrate = vatrate($tx);
			$newndfline->total_ht = ds_nf($ht);
			$newndfline->total_ttc = ds_nf($ttc);
			$newndfline->total_tva = ds_nf($newtva);
			$newndfline->date = strtotime($l->ladate);
			$newndfline->rule_warning_message = null;
			$newndfline->fk_c_exp_tax_cat = null;
			$newndfline->fk_ecm_files = null;
			$newndfline->fk_ecm_files = $idFichierJustif;

			// print "<p>Ajout d'une ligne (4) TTC=$newndfline->total_ttc, HT=$newndfline->total_ht, TVA=" .$newndfline->total_tva . "</p>";
			$result = $newndfline->insert();

			if ($result < 0) {
				$r->error = "erreur";
				$r->message .= "<li>Erreur d'ajout d'une ligne sur la note de frais : $l->label du $l->ladate, code d'erreur :" . $newndfline->error . "</li>";
			} else {
				updateLdfImportKey($result, $uniqueID);
			}
		}
	}

	return $r;
}


//Creation d'un label special pour les lignes de frais
//exemple IK on ajoute départ/arrivée/distance/véhicule
//resto on ajoute les invités etc.
function ndf_label($l)
{
	$r = $l->label;
	//Cas particulier du carburant : on ajoute les informations du véhicule
	//Opel astra;diesel;6cv;vp;0;AA-4444-BB;aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee
	if ($l->type_frais->slug == "carburant") {
		$lbl = $l->vehicule->energy ?? $l->label;
		$r = "Carburant : " . ucfirst(strtolower($lbl));
		if (strpos($lbl, ';')) {
			$t = explode(';', $lbl);
			//Si on a des détails on les ajoute
			if (count($t) > 0) {
				$r .= " ( ";
				$nb = 0;
				//On ajoute les informations disponibles sauf le kmbefore et l'uuid
				foreach ($t as $v) {
					if ($nb != 4 && $nb != 6) {
						if ($nb > 0) {
							$r .= ", ";
						}
						$r .= "$v";
					}
					$nb++;
				}
				$r .= ")";
			}
		}
	} elseif ($l->invites != "") {
		$r .= " - " . $l->invites;
	} elseif ($l->depart != "") {
		$r .= " - " . $l->depart . " -> " . $l->arrivee . " (" . $l->distance . " km)";
	}
	return $r;
}


/**
 * recherche d'une note de frais en doublon
 *
 * @var [type]
 */
function doublon_ndf($debut, $fin, $user)
{
	global $db, $conf, $entity;
	$id = null;
	dol_syslog("DoliSCAN::doublon_ndf " . $debut . " et " . $fin);

	if ($debut == "" || $fin == "") {
		dol_syslog("DoliSCAN::doublon_ndf impossible dates vides");
		return -2;
	}

	//seul moyen de savoir si une note de frais dolibarr peut correspondre au doliscan temporaire : date de début / date de fin
	//car l'utilisateur a peut-être créé une NDF dans dolibarr avant d'utiliser doliscan et/ou ...
	$sql  = "SELECT rowid FROM " . MAIN_DB_PREFIX . "expensereport WHERE date_debut='" . $debut . "' AND date_fin='" . $fin . "'";
	$sql .= " AND fk_user_author='" . $user->id . "'";

	// if ($conf->multicompany->enabled && (dsbackport_getDolGlobalString('DOLISCAN_MULTICOMPANY','') == 1 && $user->entity != 0)) {

	$sql .= " LIMIT 1";

	$error = 0;
	$resql = $db->query($sql);
	if ($resql) {
		$num = $db->num_rows($resql);
		dol_syslog("DoliSCAN:   Recherche d'un doublon de NDF pour cet utilisateur debut/fin $debut/$fin ...");
		if ($num > 0) {
			dol_syslog("DoliSCAN:   doublon trouvé ...");
			//On retourne -1 pour eviter de re-ajouter des lignes sur la NDF qui existe déjà
			// mauvaise idee car avec un cron lancé tous les jours il faut faire un update des nouveautés
			// $id = -1;
			$obj = $db->fetch_object($resql);
			$id = $obj->rowid;
			dol_syslog("DoliSCAN:   doublon trouvé: $id ...");
		} else {
			dol_syslog("DoliSCAN:   pas de doublon !");
		}
	}
	return $id;
}

/**
 * create_ndf création d'une nouvelle note de frais dolibarr (Expense Report)
 *
 * @param  mixed        $ndf DoliSCAN NDF
 * @param  mixed        $user
 *
 * @return null|int     null par defaut, -1 si creation d'une ndf erreur, et id de la ndf existante si doublon
 */
function create_ndf($ndf, $user)
{
	global $db, $conf;
	$error = 0;
	dol_syslog("DoliSCAN::create_ndf " . $ndf->debut . " et " . $ndf->fin);

	if ($ndf->debut == "" || $ndf->fin == "") {
		dol_syslog("DoliSCAN::create_ndf impossible dates vides");
		dol_syslog("DoliSCAN:: ndf=" . json_encode($ndf));
		return -2;
	}

	$object = new ExpenseReport($db);

	$id = doublon_ndf($ndf->debut, $ndf->fin, $user);

	if ($id == null) {
		dol_syslog("DoliSCAN:   pas de doublon, creation d'un ExpenseReport");
		$object->date_debut = $ndf->debut;
		$object->date_fin = $ndf->fin;
		$object->fk_user_author = $user->id;
		$object->fk_statut = ExpenseReport::STATUS_DRAFT;
		$object->fk_c_paiement = null;
		$object->fk_user_validator = null;
		$object->note_public = null;
		$object->note_private = null;

		//dans quelle entité ???

		$db->begin();

		$id = $object->create($user);
		if ($id <= 0) {
			$error++;
		}

		if (!$error) {
			$db->commit();
			// Header("Location: ".$_SERVER["PHP_SELF"]."?id=".$id);
			// exit;
		} else {
			setEventMessages($object->error, $object->errors, 'errors');
			$db->rollback();
			$id = -1;
		}
	}

	dol_syslog("DoliSCAN::create_ndf return id=$id");
	return $id;
}


/**
 * download_fichier : télécharge le fichier de DoliSCAN -> un fichier local
 *
 * @param  mixed $src
 * @param  mixed $dst
 * @return stdClass
 */
function download_fichier($src, $dst, $user, $linkToObject, $src_object_type, $force = false)
{
	global $conf, $db;
	global $dolibarr_main_data_root;
	$ecm = new EcmFiles($db);
	$r = new stdClass();
	$r->error = null;
	$r->message = null;

	$src = str_replace(['.jpeg','.jpg','.pdfs'], '.pdf', $src);
	$dst = str_replace(['.jpeg','.jpg','.pdfs'], '.pdf', $dst);

	if (file_exists($dst)) {
		//print "<p>download_fichier: le fichier $dst existe déjà ...</p>";
		dol_syslog("DoliSCAN: download_fichier $dst file already exists, short return");
		$filepath = str_replace($dolibarr_main_data_root . "/", '', dirname($dst));
		$result = $ecm->fetchAll('', '', 0, 0, array('t.filename' => basename($dst), 't.filepath' => $filepath));
		if ($result <= 0) {
			$ecm->label = basename($dst);
			$ecm->entity = $conf->entity;
			$ecm->filename = basename($dst);
			$ecm->filepath = $filepath;
			$ecm->fk_user_c = $user->id;
			$ecm->src_object_id = $linkToObject->id;
			$ecm->src_object_type = $src_object_type;
			$r->ecmID = $ecm->create($user);
		}
		$r->error = "";
		$r->message = "DoliSCAN: file already downloaded :" . basename($dst);
		return $r;
	} else {
		dol_syslog("DoliSCAN: download_fichier $src -> $dst pour userid=" . $user->id);
	}

	$endpoint = dsbackport_getDolGlobalString('DOLISCAN_MAINSERVER','');
	$applicationKey = dsbackport_getDolGlobalString('DOLISCAN_MAINACCOUNT_APIKEY','');
	$account = new MyAccount($db);
	$accountRes = $account->fetchFirst($user->id);

	$userAPI = $accountRes->ds_api ?? '';
	$userMail = $accountRes->ds_login ?? '';

	if ($userAPI == "") {
		dol_syslog("DoliSCAN: ERROR, UserAPI is empty (" . $user->id . ")!", LOG_CRIT);
		$r->error = "DoliSCAN: ERROR, UserAPI is empty (" . $user->id . ")!";
		$r->message = "DoliSCAN: ERROR, UserAPI is empty(" . $user->id . ") !";
		return $r;
	}

	dol_syslog("DoliSCAN: Download file from $src to $dst ...");

	$moreHeaders = doliSCANApiCommonHeader($userAPI); //or $applicationKey
	array_push($moreHeaders, 'Cache-Control: no-cache');
	array_push($moreHeaders, 'DocumentFormat: signedPDF');

	$param = [];
	$result = getURLContent($endpoint . '/api'.$src, 'GET', json_encode($param), 1, $moreHeaders, ['http','https'], 2);
	if (isset($result['curl_error_no'])) {
		handleDoliscanTimeoutBlacklist($result);
	}

	if (is_array($result) && $result['http_code'] == 200 && isset($result['content'])) {
		$data = json_decode($result['content']);
		// dol_syslog("DoliSCAN: Request response body :  " . json_encode($data));
		usleep(1000000);
		//dolibarr n'aime pas avoir "..." dans le nom du fichier
		$filename = str_replace('...', '', basename($data->filename));
		if (empty($dst) || is_dir($dst)) {
			//pasgenial : il faut avoir la ref objet en prefixe si on veut bénéficier des astuces dolibarr
			$dst = dirname($dst) . "/" . $filename;
		}
		//Il faut virer le /var/www/dolibarr-data/
		$filepath = str_replace($dolibarr_main_data_root . "/", '', dirname($dst));
		//Fichier déjà existant ? par exemple pour document_indisponible.jpeg
		if (file_exists($dst)) {
			dol_syslog("DoliSCAN: $dst exists, check if exists into ECM database");
			// print "<p>download_fichier: le fichier $dst existe déjà ...</p>";
			// $r->ecmID = $ecm->create($user);
			$result = $ecm->fetchAll('', '', 0, 0, array('t.filename' => $filename, 't.filepath' => $filepath));
			if ($result > 0) {
				dol_syslog("DoliSCAN: fichier déjà existant :  " . $result);
				dol_syslog(json_encode($ecm));
				$r->ecmID = $ecm->lines[0]->id;
			}
		} else {
			dol_syslog("DoliSCAN: $dst does not exists, create then check into ECM database if not exists too");
			$result = $ecm->fetchAll('', '', 0, 0, array('t.filename' => $filename, 't.filepath' => $filepath));
			if ($result > 0) {
				dol_syslog("DoliSCAN: fichier déjà existant (2) :  " . $result);
				dol_syslog(json_encode($ecm));
				$r->ecmID = $ecm->lines[0]->id;
			} else {
				dol_syslog("DoliSCAN: create into ecm database (2) :  " . $result);
				$ecm->label = $filename;
				$ecm->entity = $conf->entity;
				$ecm->filename = $filename;
				$ecm->filepath = $filepath;
				$ecm->fk_user_c = $user->id;
				$ecm->src_object_id = $linkToObject->id;
				$ecm->src_object_type = $src_object_type;
				$r->ecmID = $ecm->create($user);
				if ($r->ecmID > 0) {
					dol_syslog("DoliSCAN: ECM CREATE  :" . $r->ecmID);
					if ($ecm->errors) {
						dol_syslog(implode(',', $ecm->errors));
					}

					$fileContent = $data->fileContent;
					file_put_contents($dst, base64_decode($fileContent));
					if (file_exists($dst)) {
						$r->message .= "Téléchargement réussi.";
						dol_syslog("DoliSCAN: Download OK");
					}

				} else {
					dol_syslog("DoliSCAN: ECM CREATE Error");
					$r->message .= "<b>Erreur d'ajout du document dans la base de données</b>";
					$r->error = "Erreur d'ajout du document dans la base de données";
				}
			}
		}
	} else {
		dol_syslog("DoliSCAN: Download ERR");
		$r->message .= "<b>Erreur de téléchargement du document</b>";
		$r->error = "Erreur de téléchargement du document";
	}
	dol_syslog("DoliSCAN: end of download... ");

	// dol_syslog("DoliSCAN:  ========================= END request 2 for " . $ndfOK->import_key . " ============================= ");
	return $r;
}

function ds_nf($number)
{
	if ($number < 0) {
		//une reduction pourrait être négative alors on claque juste un message d'erreur
		dol_syslog("DoliSCAN: Maybe an error on price < 0 : " . $number, LOG_ERR);
	}
	return $number;
	//return number_format($number, 0, ',', '');
}


/**
 * update importKey field for llx_expensereport_det (not yet implemented in dolibarr core)
 *
 * @return  [type]  [return description]
 */
function updateLdfImportKey($llxid, $importKey)
{
	global $db;
	$sql = "UPDATE " . MAIN_DB_PREFIX . "expensereport_det SET import_key='" . $importKey . "' WHERE rowid='" . $llxid . "'";
	$resql = $db->query($sql);
	if ($resql) {
		dol_syslog("DoliSCAN: set document number import ref $importKey for llx line $llxid");
	}
}

/**
 * update importKey field for llx_facture_fourn
 *
 * @return  [type]  [return description]
 */
function updateFacFouImportKey($llxid, $importKey)
{
	global $db;
	$sql = "UPDATE " . MAIN_DB_PREFIX . "facture_fourn SET import_key='" . $importKey . "' WHERE rowid='" . $llxid . "'";
	$resql = $db->query($sql);
	if ($resql) {
		dol_syslog("DoliSCAN: set facFou number import ref $importKey for llx line $llxid");
	}
}

/**
 * Import NDF from DoliSCAN to Dolibarr database
 * Importe la note de frais ref id
 *
 * @param  int  $id        ID of NDF to import
 * @param  User $user      User that creates
 * @param  $prevResult if that ndf was downloaded soon (ex. in syncNDF function)
 * @param  $prevURI    the last URI requested to compare
 * @return string          html code to display
 */
function importNDF($id, $user, $prevResult = null, $prevURI = null)
{
	global $conf, $db, $langs;
	global $dolibarr_main_data_root;

	require_once DOL_DOCUMENT_ROOT . '/core/lib/files.lib.php';
	require_once DOL_DOCUMENT_ROOT . '/core/class/link.class.php';
	require_once DOL_DOCUMENT_ROOT . '/expensereport/class/expensereport.class.php';

	dol_syslog("DoliSCAN::importNDF (#$id) for user=" . json_encode($user->id));
	$modDoliscan = new modDoliscan($db);

	$endpoint = dsbackport_getDolGlobalString('DOLISCAN_MAINSERVER','');
	$applicationKey = dsbackport_getDolGlobalString('DOLISCAN_MAINACCOUNT_APIKEY','');

	$message = '';
	$error = 0;

	$account = new MyAccount($db);

	$myndf = new MyNDF($db);
	$myndf->fetch($id);

	//Si jamais la ndf est déjà importée dans dolibarr on sort direct
	if ($myndf->status == $myndf::STATUS_VALIDATED) {
		dol_syslog("DoliSCAN: this ndf (#$id) is already validated, do not reimport it !");
		$html = '<div class="fichecenter">';
		$html .= '<div class="underbanner clearboth"></div>';
		$html .= "<p>Cette note de frais a déjà été importée. Si vous voulez la réimporter veuillez la supprimer et relancer la procédure.</p>";
		$html .= '</div>';
		return $html;
	}

	//comme on peut eventuellement importer la ndf d'un autre utilisateur l'appel de la fonction passe le $user ad hoc
	$accountRes = $account->fetchFirst($user->id);

	$userLogin = $accountRes->ds_login;
	$userAPI = $accountRes->ds_api;

	$msgAndError = new stdClass();
	$errorNDF = ""; //Specifique pour la NDF pour differencier des fact fourn

	//-1 si erreur de creation de ExpenseReport
	//num id si doublon ou si expensereport créé
	$expenseRid = create_ndf(json_decode($myndf->json), $user);

	$totalPro = $totalPerso = 0;

	$ndfRef = null;
	if ($expenseRid > 0) {
		remove_last_line_ndf($expenseRid);

		$ndfJSON = json_decode($myndf->json);
		$ndfDbarr = new ExpenseReport($db);
		$objndfDbarr = $ndfDbarr->fetch($expenseRid);
		$ndfRef = $ndfDbarr->ref;

		//On essaye de joindre le fichier NDF comme justificatif global
		$ref = dol_sanitizeFileName($ndfRef);
		$upload_dir = $conf->expensereport->dir_output . '/' . $ref;
		$file_name = ($upload_dir . "/" . $ref . "-DOLISCAN.pdf");

		if (!is_dir($upload_dir)) {
			dol_mkdir($upload_dir);
		}


		dol_syslog("DoliSCAN:  start download to file $file_name ...");
		$msgAndError->message .= "<li>Téléchargement du justificatif " . $ndfJSON->sid . " vers  $ref-DOLISCAN.pdf ...</li>";

		$url2 = $endpoint . '/api/NdeFraisPDF/'.$ndfJSON->sid;
		if (null !== $prevURI && $url2 == $prevURI) {
			dol_syslog("DoliSCAN:  download déjà fait, réutilisation");
		} else {
			dol_syslog("DoliSCAN:  download started");
			$param2 = [ 'email' => $userLogin ];
			$result2 = getURLContent($url2, 'GET', json_encode($param2), 1, doliSCANApiCommonHeader($userAPI), ['http','https'], 2);

			usleep(1000000);
			if (is_array($result2) && $result2['http_code'] == 200 && isset($result2['content'])) {
				if ($fp = fopen($file_name, 'w')) {
					fwrite($fp, $result2['content']);
					fclose($fp);
				}
			} else {
				$error++;
			}
		}
		dol_syslog("DoliSCAN: Download NDF Details from DoliSCAN ... ($applicationKey)");

		$param = [];
		dol_syslog("DoliSCAN:  ========================= DOWNLOAD PDF Request 1 for " . $myndf->import_key . " ============================= ");
		$result = getURLContent($endpoint . '/api/NdeFraisDetails/'.$myndf->import_key, 'GET', json_encode($param), 1, doliSCANApiCommonHeader($userAPI), ['http','https'], 2);
		usleep(1000000);

		if (is_array($result) && $result['http_code'] == 404) {
			//not found
			$html = '<div class="fichecenter">';
			$html .= '<div class="underbanner clearboth"></div>';
			$html .= "<p>Cette note de frais n'existe plus et doit être liée à une mise en cache de votre dolibarr (suppression / re-création de compte par exemple). Veuillez la supprimer et relancer la procédure.</p>";
			$html .= '</div>';
			return $html;
		}

		if (is_array($result) && $result['http_code'] == 200 && isset($result['content'])) {
			// print $result['content'];
			$data = json_decode($result['content']);
			$mesg = '<div class="ok">' . $langs->trans("GetTypeFraisProOK") . '</div>';
			$ladate = "";
			foreach ($data as $ldeFrais) {
				$ladate = $ldeFrais->ladate;
				//Si c'est un frais payé pro -> facture fournisseur
				if ($ldeFrais->moyen_paiement->is_pro == 1) {
					dol_syslog("DoliSCAN:   create supplier invoice for " . $ldeFrais->label);
					$ret = create_fact_fournisseur($ldeFrais, $user, $ndfJSON);
					$totalPro += $ldeFrais->ttc;
					$msgAndError->message .= $ret->message;
					$msgAndError->error .= $ret->error;
				} else {
					$fraisID = fraisIdfromSlug($ldeFrais->type_frais->slug);
					//Si jamais on a un cas particulier pas glop ... ou on ne recolle pas les frais perso
					if ($fraisID == "") {
						dol_syslog("DoliSCAN:   fraisID empty, create supplier invoice for " . $ldeFrais->label);
						$ret = create_fact_fournisseur($ldeFrais, $user, $ndfJSON);
						$totalPro += $ldeFrais->ttc;
						$msgAndError->message .= $ret->message;
						$msgAndError->error .= $ret->error;
					} else {
						if ($expenseRid > 0) {
							dol_syslog("DoliSCAN:   create personnal expense for " . $ldeFrais->label);
							$ret = add_line_ndf($expenseRid, $ldeFrais, $ndfRef, $user);
							$totalPerso += $ldeFrais->ttc;
							$msgAndError->message .= $ret->message;
							$msgAndError->error .= $ret->error;
							$errorNDF .= $ret->error;
						} else {
							dol_syslog("DoliSCAN:   ERROR (no expense [" . $expenseRid . "] and no fourn invoice) for " . $ldeFrais->label);
						}
					}
				}
			}
			if ($ladate != "") {
				//Sinon le total n'est pas bon : la dernière ligne n'est pas ajoutée au total !!??!!
				add_last_line_ndf($expenseRid, $user->socid, $ladate);
			}
		} else {
			dol_syslog("DoliSCAN:   ERROR download details " . json_encode($result));
			$error++;
			$mesg = '<div class="error">' . $langs->trans("GetNdfDetailsError") . '</div>';
			setEventMessages($mesg, [], 'errors');
		}
		dol_syslog("DoliSCAN:  ========================= END request 1 for " . $myndf->import_key . " ============================= ");
	} else {
		$msgAndError->message .= "Impossible de créer la note de frais Dolibarr";
		$msgAndError->error .= "Impossible de créer la note de frais Dolibarr";
	}
	if ($msgAndError->error == "") {
		$message = "<li>Import complet sans erreur.</li>";
	}
	if ($expenseRid > 0 && $errorNDF == "") {
		dol_syslog("DoliSCAN: New ExpenseReport(a)");
		$oburl = new ExpenseReport($db);
		$r = $oburl->fetch($expenseRid);
		$url = $oburl->getNomUrl(1, '', '', '', '', 0, 0, 0);
		$message .= "<li>Note de frais importée -> $url</li>";
	}

	$html = '<div class="fichecenter">';
	$html .= '<div class="underbanner clearboth"></div>';
	$html .= "<p>Résultat de l'import de la note de frais dans Dolibarr:</p>";
	$html .= "<ul>";
	$html .= $message;
	if(isset($msgAndError->message)) {
		$html .= $msgAndError->message;
	}

	//On cloture (au cas ou) ?
	$j = json_decode($myndf->json);

	dol_syslog("DoliSCAN: json = " . $myndf->json);

	//le lien vers la note de frais dolibarr pour les eventuelles updates
	dol_syslog("DoliSCAN: add a link between doliscan #$id and dolibarr expense report #$expenseRid");
	$myndf->fk_expensereport = $expenseRid;
	//passer la DoliNDF a validated si elle est cloturée dans doliscan pour que le message affiche maintenant 'déjà importée'
	$myndf->amount_professionnal = $totalPro;
	$myndf->amount_personnal = $totalPerso;
	if ($j->status == 0) {
		$result = $myndf->update($user);
		$result = $myndf->validate($user);
		$html .= "<li>DoliSCAN Status : CLOSED</li>";
	} else {
		if ($j->status == 2) {
			$result = $myndf->status = $j->status;
		} else {
			$result = $myndf->setDraft($user);
		}
		$result = $myndf->update($user);
		$html .= "<li>DoliSCAN Status code : " . $j->status . "</li>";
	}


	$html .= "</ul>";
	$html .= '</div>';

	return $html;
}

/**
 * recherche des montants corrects pour avoir un total
 * raccord avec la réalité et les choix des calculs d'arrondis
 * dolibarr != doliscan
 *
 * @param   [array]  $tvaTab  tableau des tva / valeurs
 * @param   [float]  $total   objectif total à atteindre
 *
 * @return  [array]           résultat
 */
function algoComputeTVAContrainst($tvaTab, $total)
{
	$target = floatval($total);
	$epsilon = 0.0001; //attention on compare des float dans cette fonction !
	$epsilonToAdd = 0.01;
	//php 7.2 et +
	if (defined('PHP_FLOAT_EPSILON')) {
		$epsilon = PHP_FLOAT_EPSILON;
	}
	//TODO verifier si écart trop important exemple note 3,38 et la saisie a été de 3,3 ... impossible de deviner la solution
	$checkTotal = 0;
	foreach ($tvaTab as $tvaTx => $tvaVal) {
		$checkTotal += ($tvaVal / $tvaTx * 100);
	}
	if(($total - $checkTotal) > 0.2) {
		dol_syslog("checkTotal=$checkTotal // target=$target >> " . ($total - $checkTotal));
		$epsilonToAdd = 0.02;
	}

	dol_syslog("Recherche de solutions pour : " . json_encode($tvaTab) . " objectif=$target");
	$tvaMin = array();
	$tvaMax = array();

	foreach ($tvaTab as $tvaTx => $tvaVal) {
		//fix race condition 1.8599999999999999
		$tvaRoundVal = round($tvaVal,4);
		//Le min possible pour tva1
		$montantPourTVA = round($tvaRoundVal * 100 / $tvaTx, 2);
		$min = $montantPourTVA;

		//vérification fraude à la TVA / tickets super louche
		if($min > $target) {
			dol_syslog("DoliSCAN error on TVA / base = $min pour $tvaTx ($tvaRoundVal), total ticket = $target", LOG_WARNING);
			//retour que de la TVA à zéro pour éviter à l'utilisateur de dolisan d'être complice d'un problème de TVA
			$keys = array_keys($tvaTab);
			$solution[$keys[0]] = -2;
			$solution[$keys[1]] = -2;
			$solution[$keys[2]] = -2;
			$solution[$keys[3]] = -2;
			dol_syslog("  Résolution (-2), solution = " . json_encode($solution) . ", total=$target !!!!");
			return $solution;
		}

		do {
			$min -= 0.01;
			$res = round($min * $tvaTx / 100, 2);
		} while ($res == $tvaRoundVal);
		// $min += 0.01;
		$tvaMin[$tvaTx] = $min;
		// dol_syslog("Solution Min : $min");

		$max = $montantPourTVA;
		do {
			$max += 0.01;
			$res = round($max * $tvaTx / 100, 2);
		} while ($res == $tvaRoundVal);
		// $max -= 0.01;
		$tvaMax[$tvaTx] = $max;
		// dol_syslog("Solution Max : $max");
	}

	dol_syslog("TvaMin = " . json_encode($tvaMin) . ", TvaMax = " . json_encode($tvaMax));

	//Max 4 taux de TVA
	$keys = array_keys($tvaTab);
	$key0 = $keys[0];
	for ($i = $tvaMin[$key0]; $i <= $tvaMax[$key0]; $i += $epsilonToAdd) {
		if (isset($keys[1])) {
			$key1 = $keys[1];
			for ($j = $tvaMin[$key1]; $j <= $tvaMax[$key1]; $j += $epsilonToAdd) {
				if (isset($keys[2])) {
					$key2 = $keys[2];
					for ($k = $tvaMin[$key2]; $k <= $tvaMax[$key2]; $k += $epsilonToAdd) {
						if (isset($keys[3])) {
							$key3 = $keys[3];
							for ($l = $tvaMin[$key3]; $l <= $tvaMax[$key3]; $l += $epsilonToAdd) {
								$tot = ($i + $j + $k + $l);
								$rtot = round($tot, 2);
								dol_syslog("Résolution (4), test avec $i,$j,$k,$l total=" . $tot . " round " . $rtot);
								if (abs($rtot - $target) < $epsilon) {
									dol_syslog("  Résolution (4), solution =$i,$j,$k,$l total=$target !!!!");
									$solution[$key0] = $i;
									$solution[$key1] = $j;
									$solution[$key2] = $k;
									$solution[$key3] = $l;
									return $solution;
								}
							}
						} else {
							$tot = ($i + $j + $k);
							$rtot = round($tot, 2);
							dol_syslog("Résolution (3), test avec $i,$j,$k total=" . $tot . " round " . $rtot);
							if (abs($rtot - $target) < $epsilon) {
								dol_syslog("  Résolution (3), solution =$i,$j,$k total=$target !!!!");
								$solution[$key0] = $i;
								$solution[$key1] = $j;
								$solution[$key2] = $k;
								return $solution;
							}
						}
					}
				} else {
					$tot = ($i + $j);
					$rtot = round($tot, 2);
					dol_syslog("Résolution (2), test avec $i,$j total=$tot round=$rtot* target=$target*");
					if (abs($rtot - $target) < $epsilon) {
						dol_syslog("  Résolution (2), solution =$i,$j total=$target !!!!");
						$solution[$key0] = $i;
						$solution[$key1] = $j;
						return $solution;
					}
				}
			}
		} else {
			$tot = ($i);
			$rtot = round($tot, 2);
			dol_syslog("Résolution (1), test avec $i total=" . $tot . " round " . $rtot);
			if (abs($rtot - $target) < $epsilon) {
				dol_syslog("  Résolution (1), solution =$i total=$target !!!!");
				$solution[$key0] = $i;
				return $solution;
			}
		}
	}
	$keys = array_keys($tvaTab);
	$solution[$keys[0]] = -1;
	$solution[$keys[1]] = -1;
	$solution[$keys[2]] = -1;
	$solution[$keys[3]] = -1;
	dol_syslog("  Résolution (-1), no solution = " . json_encode($solution) . ", total=$target !!!!");
	return $solution;
}

function algoComputeTVAContrainst2($tvaVal1, $tvaTx1, $tvaVal2, $tvaTx2, $target)
{
	//1. une liste de toutes les solutions qui donnent le résultat a cause des arrondis
	dol_syslog("Recherche de solutions pour : $tvaVal1, $tvaTx1, $tvaVal2, $tvaTx2 objectif=$target");
	//Le min possible pour tva1
	$montantPourTVA1 = round($tvaVal1 * 100 / $tvaTx1, 2);
	$min1 = $montantPourTVA1;
	do {
		$min1 -= 0.01;
		$res = round($min1 * $tvaTx1 / 100, 2);
	} while ($res == $tvaVal1);
	$min1 += 0.01;
	// dol_syslog("Solution Min TVA1 : $min1");

	$max1 = $montantPourTVA1;
	do {
		$max1 += 0.01;
		$res = round($max1 * $tvaTx1 / 100, 2);
	} while ($res == $tvaVal1);
	$max1 -= 0.01;
	// dol_syslog("Solution Max TVA1 : $max1");

	//2. idem pour le 2° taux
	$montantPourTVA2 = round($tvaVal2 * 100 / $tvaTx2, 2);
	$min2 = $montantPourTVA2;
	do {
		$min2 -= 0.01;
		$res = round($min2 * $tvaTx2 / 100, 2);
	} while ($res == $tvaVal2);
	$min2 += 0.01;
	// dol_syslog("Solution Min TVA2 : $min2");

	$max2 = $montantPourTVA2;
	do {
		$max2 += 0.01;
		$res = round($max2 * $tvaTx2 / 100, 2);
	} while ($res == $tvaVal2);
	$max2 -= 0.01;
	// dol_syslog("Solution Max TVA2 : $max2");

	//3. maintenant on recherche la bonne solution
	for ($i = $min1; $i <= $max1; $i += 0.01) {
		for ($j = $max2; $j >= $min2; $j -= 0.01) {
			$tot = ($i + $j);
			dol_syslog("Résolution, test avec $i,$j total=" . $tot . " round " . round(($tot), 2));
			if (round($tot, 2) == $target) {
				dol_syslog("  Résolution, solution =$i,$j total=$target !!!!");
				$solution["ht1"] = $i;
				$solution["ht2"] = $j;
				return $solution;
			}
		}
	}
	$solution["ht1"] = 0;
	$solution["ht2"] = 0;
	return $solution;
}

/**
 * vérifie s'il manque un justificatif local par rapport à ce qui est disponible
 * sur le serveur pour une NDF ($id) d'un utilisateur($user)
 *
 * @return  [type]  [return description]
 */
function resyncAllDocumentsNDF($id, $user)
{
	global $conf, $db, $langs;
	global $dolibarr_main_data_root;
	$html = '';
	$error = 0;

	require_once DOL_DOCUMENT_ROOT . '/core/lib/files.lib.php';
	require_once DOL_DOCUMENT_ROOT . '/core/class/link.class.php';
	require_once DOL_DOCUMENT_ROOT . '/expensereport/class/expensereport.class.php';
	dol_include_once('/doliscan/class/myaccount.class.php');
	dol_include_once('/doliscan/class/myndf.class.php');

	if (empty($user->id)) {
		dol_syslog("DoliSCAN::resyncAllDocumentsNDF user id is empty, return", LOG_WARNING);
		return;
	}

	dol_syslog("DoliSCAN::resyncAllDocumentsNDF (#$id) for user=" . json_encode($user->id));
	$modDoliscan = new modDoliscan($db);

	$endpoint = dsbackport_getDolGlobalString('DOLISCAN_MAINSERVER','');
	$applicationKey = dsbackport_getDolGlobalString('DOLISCAN_MAINACCOUNT_APIKEY','');

	$account = new MyAccount($db);

	$myndf = new MyNDF($db);
	$myndf->fetch($id);

	//comme on peut eventuellement importer la ndf d'un autre utilisateur l'appel de la fonction passe le $user ad hoc
	$accountRes = $account->fetchFirst($user->id);

	if (empty($accountRes)) {
		dol_syslog("DoliSCAN:  error accountRes is empty !!!!", LOG_WARNING);
		return false;
	}

	$userLogin = $accountRes->ds_login;
	$userAPI = $accountRes->ds_api;

	// print "<p>login=$userLogin, api=$userAPI, id=" . $user->id . "</p>";

	$msgAndError = new stdClass();
	$errorNDF = ""; //Specifique pour la NDF pour differencier des fact fourn

	//-1 si erreur de creation de ExpenseReport
	//num id si doublon ou si expensereport créé
	$expenseRid = create_ndf(json_decode($myndf->json), $user);
	if ($expenseRid > 0) {
		$ndfJSON = json_decode($myndf->json);
		$ndfDbarr = new ExpenseReport($db);
		$objndfDbarr = $ndfDbarr->fetch($expenseRid);
		$ndfRef = $ndfDbarr->ref;

		//On essaye de joindre le fichier NDF comme justificatif global
		$ref = dol_sanitizeFileName($ndfRef);
		$upload_dir = $conf->expensereport->dir_output . '/' . $ref;
		$file_name = ($upload_dir . "/" . $ref . "-DOLISCAN.pdf");

		if (!is_dir($upload_dir)) {
			dol_mkdir($upload_dir);
		}

		$param = [];
		dol_syslog("DoliSCAN:  ========================= DOWNLOAD PDF Request 1 for " . $myndf->import_key . " ============================= ");
		$result = getURLContent($endpoint . '/api/NdeFraisDetails/'.$myndf->import_key, 'GET', json_encode($param), 1, doliSCANApiCommonHeader($userAPI), ['http','https'], 2);
		usleep(1000000);

		if (is_array($result) && $result['http_code'] == 200 && isset($result['content'])) {
			// print "<p>Details de la NDF :" . json_encode($result['content']) . "</p>";

			$data = json_decode($result['content']);

			$ladate = "";
			foreach ($data as $ldeFrais) {
				$needRedownload = false;

				$ladate = $ldeFrais->ladate;
				$filename = $ldeFrais->fileName;
				$filebase = substr($filename, strrpos($filename, '-')+1);
				$filecode = substr($filebase, 0, strpos($filebase, '.'));

				$ecm = new EcmFiles($db);
				$ecmTmp = new EcmFiles($db);
				//nettoyage de la bdd parfois des fichiers sont orphelins
				// print "<p>recherche d'orphelins pour $filecode</p>";
				$resECM = $ecm->fetchAll('ASC', 'position', 0, 0, array('filename' => $filecode));
				if ($resECM > 0) {
					foreach ($ecm->lines as $e) {
						if (null === $e->src_object_id) {
							// print "<p>orphelin a nettoyer : " . json_encode($e) . "</p>";
							$ecmTmp->fetch($e->id);
							$ecmTmp->delete($user);
						} else {
							// print "<p>pas d'orphelin pour " . json_encode($e) . "</p>";
							$fullFileNameToSearch = DOL_DATA_ROOT . '/' . $e->filepath . '/' . $e->filename;
							if (file_exists($fullFileNameToSearch)) {
								//ok
							} else {
								dol_syslog("fichier $fullFileNameToSearch existe en bdd mais pas sur le FS ...");
								$ecmTmp->fetch($e->id);
								$ecmTmp->delete($user);
							}
						}
					}
				}

				$facid = null;
				if ($ldeFrais->moyen_paiement->is_pro == 1) {
					//La facture fournisseur
					$facfou = doublon_fac_fournisseur($ldeFrais, $user);
					if(is_object($facfou)) {
						$facid = $facfou->id;
					}
					$resECM = $ecm->fetchAll('ASC', 'position', 0, 0, array('filename' => $filecode, 't.src_object_id' => $facid));
				} else {
					$resECM = $ecm->fetchAll('ASC', 'position', 0, 0, array('filename' => $filecode));
				}

				if ($resECM > 0) {
					$needRedownload = false;
				} else {
					$needRedownload = true;
				}

				if ($needRedownload) {
					$html .= "<li>Fichier $filename manquant ...<br />";
					if ($ldeFrais->moyen_paiement->is_pro == 1) {
						$facfou = new FactureFournisseur($db);
						$html .= "C'est un justificatif de frais payé pro (facture fournisseur) ...";
						if ($facid != null && $facfou->fetch($facid)) {
							$html .= " Re-Téléchargement du justificatif et ajout à la facture " . $facfou->ref . "<br />";
							$ref = dol_sanitizeFileName($facfou->ref);
							$upload_dir = $conf->fournisseur->facture->dir_output . '/' . get_exdir($facfou->id, 2, 0, 0, $facfou, 'invoice_supplier') . $ref;

							if (!is_dir($upload_dir)) {
								dol_mkdir($upload_dir);
							}

							if (is_dir($upload_dir) && ($ldeFrais->fileName != "")) {
								$uri = "/ldfImages/" . $user->email . "/" . $ldeFrais->fileName;
								$ext = pathinfo($ldeFrais->fileName, PATHINFO_EXTENSION);
								//$dest = $upload_dir . "/" . $facfou->ref_supplier . ".$ext";
								//20230724 - ajout du prefixe ref
								$dest = $upload_dir . "/" . $ref . '-' . str_replace('...', '', $ldeFrais->fileName);
								// file_put_contents($file_name, file_get_contents($url));
								$html .= "<ul><li>Téléchargement du document justificatif <b>" . basename($dest) . "</b>  : ";

								if (file_exists($dest) && sha1_file($dest) == $ldeFrais->fileCheck) {
									dol_syslog("DoliSCAN: (re)download_fichier $dest : bingo sha1 identique ça ne sert à rien de le re-télécharger !");
								} else {
									$ret = download_fichier($uri, $dest, $user, $facfou, "invoice_supplier");
									if (!empty($ret->error)) {
										$html .= "<p style='red'>" . $ret->error . "</p>";
									}
								}
								$html .= "</li></ul>";
							}
						}
					} else {
						$html .= "C'est un justificatif de frais payé perso ...";

						$ref = dol_sanitizeFileName($ndfRef);
						$upload_dir = $conf->expensereport->dir_output . '/' . $ref;
						if (!is_dir($upload_dir)) {
							dol_mkdir($upload_dir);
						}

						if (is_dir($upload_dir) && ($ldeFrais->fileName != "")) {
							$uri = "/ldfImages/" . $user->email . "/" . $ldeFrais->fileName;
							$ext = pathinfo($ldeFrais->fileName, PATHINFO_EXTENSION);
							$dest = $upload_dir . "/" . str_replace('...', '', $ldeFrais->fileName);
							if (file_exists($dest)) {
								dol_syslog("DoliSCAN: add_line_ndf do not (re)download file $dest");
							} else {
								$downloadPiece = download_fichier($uri, $dest, $user, $ndfDbarr, "expensereport");
								if ($downloadPiece->error !== null) {
									$html .= "<li>" . $ldeFrais->fileName . " pas téléchargé (" . $downloadPiece->message . ")</li>";
									$idFichierJustif = null;
								} else {
									$idFichierJustif = $downloadPiece->ecmID;
									$html .= "<li>" . $downloadPiece->message . " pour " . $ldeFrais->fileName . " -> id fichier $idFichierJustif</li>";
								}
							}
						}
					}
				} else {
					$html .= "<li>Fichier justificatif " . $filename . " déjà présent dans dolibarr</li>";
				}
				$html .= "</li>";
			}
			if ($ladate != "") {
				//Sinon le total n'est pas bon : la dernière ligne n'est pas ajoutée au total !!??!!
				// add_last_line_ndf($expenseRid, $user->socid, $ladate);
			}
		} else {
			$error++;
			$mesg = '<div class="error">' . $myndf->import_key . ' for ' . $userLogin . '</div>';
			setEventMessages($mesg, [], 'errors');
		}
		dol_syslog("DoliSCAN:  ========================= END request 1 for " . $myndf->import_key . " ============================= ");
	}

	return $html;
}


/**
 * lance la re-synchronisation de toutes les pièces jointes manquantes
 * pour tous les utilisateurs
 *
 * @return  [type]  [return description]
 */
function resyncAllDocuments()
{
	global $conf, $db, $langs;
	$html = "";

	$sql = "SELECT rowid,login FROM " . MAIN_DB_PREFIX . "user WHERE statut='1' AND entity IN (" . getEntity('user') . ")";
	// print $sql;
	$resql = $db->query($sql);

	$html .= "<ul>";

	if ($resql) {
		while ($objp = $db->fetch_object($resql)) {
			$useridtoupdate = $objp->rowid;
			//On cherche ses 12 dernières notes de frais
			$sql2 = "SELECT rowid as ndfid FROM " . MAIN_DB_PREFIX . "doliscan_myndf WHERE fk_user='" . $useridtoupdate . "' ORDER BY date_creation DESC LIMIT 12";
			$resql2 = $db->query($sql2);
			if ($resql2) {
				$userUpdate = new User($db);
				if ($userUpdate->fetch($useridtoupdate)) {
					while ($objp2 = $db->fetch_object($resql2)) {
						if (!empty($objp2->ndfid)) {
							// print "<p>" . $sql2 . " :: " . json_encode($objp2) . "</p>";exit;
							$html .= "<li> Vérification pour " . $userUpdate->firstname . " " . $userUpdate->lastname . " ( NDF id#" . $objp2->ndfid . ") : </li>";
							$html .= "<ul>";
							$html .= resyncAllDocumentsNDF($objp2->ndfid, $userUpdate);
							$html .= "</ul>";
						}
					}
				} else {
					dol_syslog("DoliSCAN::resyncAllDocuments error while fetching user id $useridtoupdate", LOG_WARNING);
				}
			}
		}
	}

	$html .= "</ul>";
	return $html;
}
