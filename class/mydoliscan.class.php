<?php
/* Copyright (C) 2017  Laurent Destailleur <eldy@users.sourceforge.net>
 * Copyright (C) 2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file        class/mydoliscan.class.php
 * \ingroup     doliscan
 * \brief       This file is a CRUD class file for MyNDF (Create/Read/Update/Delete)
 */

// Put here all includes required by your class file
require_once DOL_DOCUMENT_ROOT . '/core/class/commonobject.class.php';
require_once DOL_DOCUMENT_ROOT . '/user/class/user.class.php';
require_once __DIR__ . '/myndf.class.php';
require_once __DIR__ . '/myaccount.class.php';
// require_once __DIR__ . '/../vendor/autoload.php';
dol_include_once('/doliscan/lib/doliscan_myndf.lib.php');
dol_include_once('/doliscan/core/modules/modDoliscan.class.php');
dol_include_once('/doliscan/lib/doliscan.lib.php');


/**
 * Class for MyDoliSCAN
 */
class MyDoliSCAN extends CommonObject
{
	public $socid;
	public $labelStatusShort;
	public $labelStatus;
	public $output;
	public $user_creation;
	public $user_validation;

	/**
	 * @var string ID to identify managed object
	 */
	public $element = 'mydoliscan';

	/**
	 * @var int  Does this object support multicompany module ?
	 * 0=No test on entity, 1=Test with field entity, 'field@table'=Test with link by field@table
	 */
	public $ismultientitymanaged = 0;

	/**
	 * @var int  Does object support extrafields ? 0=No, 1=Yes
	 */
	public $isextrafieldmanaged = 0;

	/**
	 * Compare two dates
	 *
	 * @param   string  $a  first date
	 * @param   string  $b  second date
	 *
	 * @return  int  0 if a = b
	 *               1 if a > b
	 *              -1 if a < b
	 *              -2 if a is empty or null or not set
	 *              -3 if b is empty or null or not set
	 */
	private function date_compare($a, $b)
	{
		// dol_syslog("DoliSCAN: date_compare $a / $b");
		//convert 14/06/2021 to 2021-06-14
		$time = strtotime(str_replace('/', '-', $a));
		$a = date('Y-m-d', $time);

		$time = strtotime(str_replace('/', '-', $b));
		$b = date('Y-m-d', $time);

		$ret = -10;
		if (!is_null($a)) {
			if (!is_null($b)) {
				// dol_syslog("DoliSCAN:   date 1 : " . json_encode($a));
				// dol_syslog("DoliSCAN:   date 2 : " . json_encode($b));
				$date1 = new DateTime($a);
				$date2 = new DateTime($b);

				if ($date1 > $date2)
					$ret= 1;
				if ($date1 < $date2)
					$ret= -1;
				if ($date1 == $date2)
					$ret= 0;
			} else {
				$ret= -3;
			}
		} else {
			$ret= -2;
		}

		dol_syslog("DoliSCAN: date_compare $a / $b -> return $ret");
		return $ret;
	}

	/**
	 * Action executed by scheduler
	 * CAN BE A CRON TASK. In such a case, parameters come from the schedule job setup field 'Parameters'
	 * Use public function doScheduledJob($param1, $param2, ...) to get parameters
	 *
	 * @return	int			0 if OK, <>0 if KO (this function is used also by cron so only 0 is OK)
	 */
	public function doScheduledJob()
	{
		global $conf, $langs;

		//$conf->global->SYSLOG_FILE = 'DOL_DATA_ROOT/dolibarr_mydedicatedlofile.log';

		$error = 0;
		$this->output = '';
		$this->error = '';

		dol_syslog(__METHOD__, LOG_DEBUG);

		$now = dol_now();

		$this->db->begin();

		// ...

		$this->db->commit();

		return $error;
	}

	/**
	 * cron for DoliSCAN
	 *
	 * @return  [type]  [return description]
	 */
	public function startCron()
	{
		global $db, $conf;

		//Sync projects tags
		if (dsbackport_getDolGlobalString('DOLISCAN_PROJECT_NEED_PUSH',-1) > 0) {
			dol_syslog("DoliSCAN::cron call doliSCANpushProjects");
			doliSCANpushProjects();
		} else {
			dol_syslog("DoliSCAN::cron do not call doliSCANpushProjects due to DOLISCAN_PROJECT_NEED_PUSH settings");
		}

		//une seule fois par jour max (donc test sur 23h pour gérer les petits aléas de cron)
		//		if ($diffTime > (60*60*23)) {
		$diffTime = dol_now() - dsbackport_getDolGlobalString('DOLISCAN_LAST_SYNC',0);
		if ($diffTime > (60*60*23)) {
			dol_syslog("DoliSCAN::cron call syncAllAccounts");
			$this->syncAllAccounts();
		} else {
			dol_syslog("DoliSCAN::cron do not call syncAllAccounts due to DOLISCAN_LAST_SYNC settings (dol_now=" . dol_now() .", DOLISCAN_LAST_SYNC=" . $conf->global->DOLISCAN_LAST_SYNC . ", diff=" . $diffTime . ")");
		}

		//todo add flag like DOLISCAN_PROJECT_NEED_PUSH
		resyncAllDocuments();

		return 0;
	}

	/**
	 * Sync between DoliSCAN and Dolibarr for all users
	 *
	 * @return	int			0 if OK, <>0 if KO (this function is used also by cron so only 0 is OK)
	 */
	public function syncAllAccounts($force = 0)
	{
		global $conf, $db, $user,$langs;
		$success = false;
		dol_syslog("DoliSCAN::syncAllAccounts");

		//erics 20210913 - il faut pouvoir synchroniser les frais "pro" quotidiennement ...
		// //cron lauch every day by default, so please sync only after close date (doliscan close date is 6)
		// if (($force == 0) && (date('j') != 6)) {
		//     dol_syslog("DoliSCAN: (syncAllAccounts) Please do this action only the 6th day of the month because of closed time in doliscan.fr is on the 6 !");
		//     return -1;
		// }

        if(dsbackport_getDolGlobalString('DOLISCAN_REFRESH_DRAFT','') != ''){
            $ndf = new MyNDF($db) ;
            $res=$ndf->fetchAll('','','','',array('status'=>'0'));
            foreach ($res as $row){
                $ndf->fetch($row->id);
                $ndf->delete($user);
            }
        }

		$endpoint = dsbackport_getDolGlobalString('DOLISCAN_MAINSERVER','');
		//Pour chaque utilisateur qui a un compte doliscan on lance un sync sous son identité
		$account = new MyAccount($db);
		$accountRes = $account->fetchAll();
		// dol_syslog("DoliSCAN::syncAllAccounts :: " . json_encode($accountRes));
		foreach ($accountRes as $key => $val) {
			$userid = $val->fk_user;
			$ds_login = $val->ds_login;
			$ds_api = $val->ds_api;

			$dateStart = null;
			if ($val->sync_date_start > 0)
				$dateStart = date("Y-m-d", $val->sync_date_start);

			$dateStop = null;
			if ($val->sync_date_stop > 0)
				$dateStop = date("Y-m-d", $val->sync_date_stop);

			$u = new User($db);
			//C'est la qu'on risque d'avoir un pb si l'utilisateur a été supprimé de dolibarr et pas propagé dans doliscan !
			if ($u->fetch($userid)) {
				/** @phpstan-ignore-next-line */
				if ($u->statut == User::STATUS_ENABLED) {
					dol_syslog("DoliSCAN::user dolibarr found ! sync data for this account ($ds_login) dolibarr userid=" . json_encode($u->id));
					$this->syncAccount($u, $val, $endpoint, $dateStart, $dateStop);
					$success = true;
				} else {
					dol_syslog("DoliSCAN::user dolibarr found but that user is disabled in dolibarr, next");
				}
			} else {
				dol_syslog("DoliSCAN::user dolibarr not found ! please delete this account on doliscan server ...");
			}
		}
		dol_syslog("DoliSCAN::end syncAllAccounts set DOLISCAN_LAST_SYNC to now()");
		//only on success
		if($success) {
			dolibarr_set_const($db, "DOLISCAN_LAST_SYNC", (string) dol_now(), 'chaine', 0, '', $conf->entity);
		}
		return 0;
	}

	public function syncNDF($baruser, $endpoint, $userAPI, $ndfOK)
	{
		global $conf, $db, $langs;
		dol_syslog("DoliSCAN::syncNDF");

		//Si jamais la ndf est déjà importée dans dolibarr on sort direct
		if ($ndfOK->status == $ndfOK::STATUS_VALIDATED) {
			dol_syslog("DoliSCAN::syncNDF this ndf (#" . $ndfOK->id . ") is already closed, do not reimport it !");
			return;
		}

		dol_syslog("DoliSCAN:  ========================= DOWNLOAD PDF Request 2 for " . $ndfOK->import_key . " ============================= ");

		$ref = dol_sanitizeFileName($ndfOK->ref);
		$upload_dir = $conf->doliscan->dir_output . "/myndf/" . $ref;
		$file_name = ($upload_dir . "/" . $ref . ".pdf");

		dol_syslog("DoliSCAN:  download pour $ref : $upload_dir -> $file_name");

		if (!is_dir($upload_dir)) {
			dol_mkdir($upload_dir);
		}
		if (is_dir($upload_dir)) {
			dol_syslog("DoliSCAN:  start download to $file_name ...");

			//Ajout du document joint ?
			$param2 = [];
			$url2 = $endpoint . '/api/NdeFraisPDF/'.$ndfOK->import_key;
			$result2 = getURLContent($url2, 'GET', json_encode($param2), 1, doliSCANApiCommonHeader($userAPI), ['http','https'], 2);

			usleep(1000000);
			if (is_array($result2) && $result2['http_code'] == 200 && isset($result2['content'])) {
				if ($fp = fopen($file_name, 'w')) {
					fwrite($fp, $result2['content']);
					fclose($fp);
				}
				//Maintenant on recupere le contenu complet de la NDF
				importNDF($ndfOK->id, $baruser, $result2, $url2);
			}
		}
		dol_syslog("DoliSCAN:  ========================= END request 2 for " . $ndfOK->import_key . " ============================= ");
		return;
	}

	public function syncAccount(User $baruser, MyAccount $userDoliSCAN, $endpoint, $dateStart, $dateStop)
	{
		global $conf, $db, $langs, $mysoc;
		$ds_login = $userDoliSCAN->ds_login;
		$ds_api = $userDoliSCAN->ds_api;
		$applicationKey = dsbackport_getDolGlobalString('DOLISCAN_MAINACCOUNT_APIKEY','');
		$errMessage = "";

		$forbiden = [ $conf->global->DOLISCAN_MAINACCOUNT_EMAIL ];
		if (in_array($ds_login, $forbiden)) {
			return;
		}

		$messageType="errors";
		$error = 0;
		// print "<pre>$dateStart :: $dateStop</pre>";
		//print_r($userAPI);
		// exit;
		$modDoliscan = new modDoliscan($db);

		if (dsbackport_getDolGlobalString('DOLISCAN_STARTDATEIMPORT','') != "") {
			$dateStart = dsbackport_getDolGlobalString('DOLISCAN_STARTDATEIMPORT','');
		}
		if ($dateStop == "") {
			//Fin du mois sinon pas de sync de la ndf en cours
			$dateStop = date("Y-m-t");
		}

		dol_syslog("DoliSCAN: (syncAccount) Test if credential to endpoint " . $endpoint . " with userAPI and $ds_login works, dateStart=$dateStart, dateStop=$dateStop");

		$param = [ 'email' => $ds_login ];
		if ($dateStart != "") {
			$result = getURLContent($endpoint ."/api/NdeFrais/from/$dateStart/to/$dateStop", 'GET', json_encode($param), 1, doliSCANApiCommonHeader($ds_api), ['http','https'], 2);
		} else {
			$result = getURLContent($endpoint ."/api/NdeFrais", 'GET', json_encode($param), 1, doliSCANApiCommonHeader($ds_api), ['http','https'], 2);
		}
		if (is_array($result) && $result['http_code'] == 200 && isset($result['content'])) {
			dol_syslog("DoliSCAN: Request response OK"); // :  " . json_encode($data));
			$data = json_decode($result['content']);
			// dol_syslog("DoliSCAN: Request response body :  " . json_encode($data));
			$mesg = '<div class="ok">' . $langs->trans("CheckConnectOK") . '</div>';


			foreach ($data as $key => $val) {
				dol_syslog("DoliSCAN: Try to import NDF ($val->label) end : " . $val->fin);
				// dol_syslog("DoliSCAN: " . json_encode($val));

				//Filtrage des dates globales
				if (dsbackport_getDolGlobalString($conf->global->DOLISCAN_STARTDATEIMPORT, '') !== '' &&
					$this->date_compare(trim($val->fin), dsbackport_getDolGlobalString('DOLISCAN_STARTDATEIMPORT',0)) == -1) {
					// dsbackport_getDolGlobalString('DOLISCAN_STARTDATEIMPORT','') != '' && ($val->fin < dsbackport_getDolGlobalString('DOLISCAN_STARTDATEIMPORT',''))) {
					dol_syslog("DoliSCAN: Do not import this NDF due to global date settings :  " . $val->fin . " < " . dsbackport_getDolGlobalString('DOLISCAN_STARTDATEIMPORT',''));
					continue;
				}

				//Filtrage des dates de l'utilisateur
				if ($dateStart !== null && $this->date_compare(trim($val->fin), $dateStart) == -1) {
					dol_syslog("DoliSCAN: Do not import this NDF due to user date settings :  " . $val->fin . " < " . $dateStart);
					continue;
				}
				if ($dateStop !== null && $this->date_compare(trim($val->fin), $dateStop) == 1) {
					dol_syslog("DoliSCAN: Do not import this NDF due to user date settings :  " . $val->fin . " > " . $dateStop);
					continue;
				}

				//Filtrage si cette NDF est déjà validée dans dolibarr
				//fix #7, la ndf doliscan locale peut être validée mais pour autant la note de frais dolibarr non, dans ce cas il faut pouvoir
				//forcer la mise à jour de la note de frais ... il faut donc aller chercher la ref de la note de frais dolibarr et s'appuyer sur
				//son état et/ou vérifier si le "contenu" de la doliscan locale correspond au contenu de la doliscan distante
				$ndfClosed = new MyNDF($db);
				$closed = $ndfClosed->fetchAll('', '', 0, 0, array('status' => $ndfClosed::STATUS_VALIDATED, 'label' => $val->label, 'customsql' => 't.fk_user = ' . $baruser->id));
				if (count($closed) > 0) {
					$forceRefresh = false;
					$testClosed = reset($closed);
					dol_syslog("DoliSCAN: that doliscan NDF ($val->label) is closed on dolibarr side ... let me do a better check");

					//Premiere piste, si le montant est different
					if ($val->montant != $testClosed->amount) {
						dol_syslog("DoliSCAN: force refresh this NDF ($val->label): local amount ($testClosed->amount) is different than remote ($val->montant)");
						$forceRefresh = true;
					} else {
						//seconde piste, last update
						$remoteUpdate = strtotime($val->updated_at);
						$localUpdate = $testClosed->tms;
						if ($remoteUpdate > $localUpdate) {
							dol_syslog("DoliSCAN: force refresh this NDF ($val->label): local update timstamp is older than remote ($val->updated_at)");
							$forceRefresh = true;
						}
					}

					if (!$forceRefresh) {
						dol_syslog("DoliSCAN: Do not import this NDF ($val->label): already closed on dolibarr side");
						continue;
					}
				}

				$dsID = $val->id;
				// print "on a id = $dsID";
				//On cherche si la NDF existe deja et si c'est le cas on update
				$n = new MyNDF($db);
				$n->amount = $val->montant;
				$n->label = $val->label;
				$n->fk_user = $baruser->id;
				$n->json = json_encode($val); //On sauvegarde le contenu du json
				$n->description = $langs->trans("ImportFromDoliSCANFor") . " " . $val->label . " (" . $baruser->firstname . " " . $baruser->lastname . ")";
				$n->status = MyNDF::STATUS_DRAFT;
				$n->import_key = $val->sid;
				$doublon = $n->fetchAll('', '', 0, 0, array('label' => $val->label, 'customsql' => 't.fk_user = ' . $baruser->id));
				$update = 0;

				if (count($doublon) > 0) {
					//TODO : update la NDF
					$ndfID = array_pop($doublon)->id;
					$update = 1;
					dol_syslog("DoliSCAN: Doublon detected -> update NDF ref " . $ndfID);
				} else {
					//note: risque de collision -> TODO
					$n->ref = "DOLISCAN-" . substr(dol_string_unaccent($baruser->firstname), 0, 1) . substr(dol_string_unaccent($baruser->lastname), 0, 1) . str_replace("-", '', $val->debut);
					$ndfID = $n->create($baruser);
				}
				if ($ndfID) {
					$ndfOK = new MyNDF($db);
					if ($ndfOK->fetch($ndfID)) {
						if ($update == 1) {
							//sync data si on a déjà sync cette ndf plus tôt dans le mois
							dol_syslog("DoliSCAN: Update existing NDF #" . $ndfOK->id . "with details=" . json_encode($ndfOK));
							$ndfOK->json = json_encode($val);
							$ndfOK->amount = $val->montant;
							$ndfOK->setDraft($baruser);
							$ndfOK->update($baruser, true);
						}

						dol_syslog("DoliSCAN: syncNDF #" . $ndfOK->id);
						$this->syncNDF($baruser, $endpoint, $ds_api, $ndfOK);
					}
				}
			}
			// $num = $this->ref;
				// $this->newref = $num;
		} else {
			dol_syslog("DoliSCAN: Request response ERROR code = " . $result['http_code']); // :  " . json_encode($data));
			if ($result['http_code'] == 401) {
				//tentative - une fois - de créer la clé d'api au vol
				dol_syslog("DoliSCAN: tentative de créer la clé d'API pour $ds_login");
				$cpt = $_SESSION['globalCreateAccount'.$ds_login] ?? 0;
				if ($cpt < 2) {
					$_SESSION['globalCreateAccount'.$ds_login] = $cpt+1;
					//tentative de re-créer le lien automatiquement
					dol_syslog("DoliSCAN: tentative pour " . json_encode($userDoliSCAN));
					$resCreate = doliSCANcreateAccount($userDoliSCAN, $baruser);
					if ($resCreate == 0) {
						dol_syslog("DoliSCAN: création de clé d'API pour $ds_login SUCCESS");
						$errMessage = $langs->trans("tokenRenewForUser", $ds_login);
						$messageType="warnings";
					} else {
						dol_syslog("DoliSCAN: création de clé d'API pour $ds_login ERROR");
						$errMessage = $langs->trans("tokenErrorForUser", $ds_login) . " (1)";
						$error++;
					}
				}
			} else {
				$errMessage = $langs->trans("tokenErrorForUser", $ds_login) . " (2)";
				$messageType="errors";
				$error++;
			}
			if($errMessage != "") {
				$mesg = '<div class="error">' . $langs->trans("CheckConnectError") . ' [' . $errMessage . ']</div>';
				setEventMessages($mesg, [], $messageType);
			}
		}
		$action = '';
		dol_syslog("DoliSCAN: end of syncAccount");
		return $error;
	}
}
