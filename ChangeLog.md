# CHANGELOG DOLISCAN FOR [DOLIBARR ERP CRM](https://www.dolibarr.org)

## 2.0.69 --

* code cleanup (getDolGlobalString)

## 2.0.68 -- 20250213

* add an other check to avoid expense multi-payment
* better log messages

## 2.0.66 -- 20250107

* new refresh on synchro thanks to william mead (inovea)
* use default payment configured on thirdpart in priority
  and fallback to module global setup
* better connectivity tests
* better multicompany compatibility

## 2.0.64 -- 20241030

* solution for false VAT tickets amounts

## 2.0.62 -- 20241028

* better sync all missing doc via cron call
* avoid double payment if invoice is already paid
* new double check on ht/ttc totals

## 2.0.58 -- 20240916

* fix cron auto start sync
* change default sort order by date
* better log debugs & data

## 2.0.56 -- 20240611

* better auto de-blacklist support
* better multicompany compatibility
* fix big error on vat value for special french fiscal rules on gazoline

## 2.0.50 -- 20240505

* fix multiple identical indexes in llx_doliscan_myaccount
* fix default entity in case of old migration data
* fix vat rate special rules for french system (0% VAT on 20%)
* fix self extract from blacklist for shared ip addresses like ovh mutualized hosting systems and cap-rel global firewall
* fix display 'Undef' fields on module setup
* fix better messages in case of outdated api key

## 2.0.48 -- 20240425

* fix warnings & compat php 8 thanks to ATM-Irvine

## 2.0.46 -- 20240410

* fix race condition create/delete/re-create account with old expense report
* better multicompany support thanks to regis
* fix sql alias t error
* fix stdobject / string search error

## 2.0.40

* fix fontawesome 4 -> 5
* better check against ecm database before make insert
* fix list length for carbu listing
* fix backports (form file)
* better checks before insert into ECM table entries
* better tests before (re) downloading files
* fix #20: compute HT/TTC values
* fix #22: display config on payment modes (admin/setup)
* fix #21: deprecated libelle on supplier invoices switch to label

## 2.0.38

* fix warning on ini file
* fix collision with bornan numbers and datepaye

## 2.0.36 -- 20240131

* more tests against global firewall conditions
* fix error if admin do not clic on save button for 1st setup step

## 2.0.32 -- 20240126

* fix getentity on doliscan -> user
* better type of message on warning on api key renew

## 2.0.26 -- 20240115

* fix race condition on configurationpro.php
* new option to add/remove external accounts into doliscan list
* new option : each user could have a "cb-pro" on different bank / account
* new handle cap-rel firewall backlist with a self-unblack system
* new option for specific french rules against caburant taxes

## 2.0.20 -- 20231207

* first try with php 8
* remove composer

## 2.0.18 -- 20231016

* sync tag : priority use main admin doliscan account, then company or user account

## 2.0.17 -- 20230925

* resync 12 old ndf to get all missed files

## 2.0.16 -- 20230724

* exp: rename CamelCase database fields to be compatible with pgsql (and others)
* new: new action on main screen : re-sync all missed files from doliscan main server if you

## 2.0.14 -- 20230602

* handle tests if your dolibarr public ip is blacklisted
* add status_frozen like official doliscan status
* do not redownload files if already here
* fix multientity race condition (shared user account)
* update url_lastversion module config key

## 2.0.10

* switch to applicationKey for myaccount sync
* syncme is back

## 2.0.6

* fix a race condition on multientity setup
* better tokenErrorForUser search
* company email can be override in setup priority
* remove syncme button, too many garbage for hotline support

## 2.0.3

* fix download image on some race conditions
* better tags upload

## 2.0.2

* fix bug on supplier invoice with no project

## 2.0.1

* better error message (add curl error message) on connexion test with server
* handle pdf/jpeg files

## 2.0.0

* share projects with doliscan app
* better lists display perso/pro amounts
* add a new link to create user on doliscan server directly from dolibarr
* add a trigger on dolibarr new user to propagate and create a new account on doliscan
* add a trigger on dolibarr enable/disable account to propagate on doliscan
* better error messages
* better upgrade/update/refresh expenses (better cache)
* new check to make it impossible to use an admin account like a user one and have exepenses for admin

## 1.9.40

* fix a bug : downloading jpeg files joined to invoices
* code cleanup

## 1.9.38

* do not sync disabled dolibarr account

## 1.9.36

* dolibarr 16 + php 8.1 tests ok
* correctif return $r on add_line_ndf
* massive code cleanup (remove all guzzle depends, switch to dolibarr internal http requests)
* Rename AllAccounts to DoliSCANAllAccounts (due to dolibarr core collision)

## 1.9.32

* correctifs mineurs (typo)
* modification des icones (refresh / download)
* modification de la configuration (page de configuration admin)
* correctif pour avoir les icones refresh valables (compatible font awesome 4 -> 6)

## 1.9.28

* Corrige un bug de configuration initiale des liens (frais pro - fournisseurs dolibarr)

## 1.9.26

* Prise en compte du cas particulier de l'utilisateur dolibarr qui a été supprimé mais pas son compte doliscan
  (normalement impossible vu la contrainte de clé externe au niveau bdd mais c'est arrivé chez un utilisateur)

## 1.9.24

* Fusion complète de la branche "multicompany" qui est donc maintenant dans le code central du connecteur
* Début de traduction en espagnol, utilisation de weblate comme plate-forme de traduction
* https://hosted.weblate.org/projects/doliscan/

## 1.7.20

* Corrige un bug de détection de la configuration du module multisociété
* Modifie la date de fin de sync au dernier jour du mois pour avoir la note de frais en cours

## 1.7.14

* très grosse amélioration du processus initial de configuration avec un envoi d'un code secret de vérification
  par le serveur doliscan pour gérer les cas particuliers de comptes préexistants sur doliscan !


## 1.7.12

* fin de l'optimisation globale qui evite de re-télécharger les notes de frais clôturées côté dolibarr

## 1.7.11

* utilisation d'une constante DOLISCAN_UPDATE_1_7_10 configuree a la date de deploiement du module pour eviter les
  doublons des notes de frais et ne pas avoir a supprimer les notes de frais comme indiqué dans le changelog pour la
  version 1.7.8
* nouvel algo de recherche des montants HT/TTC pour les frais a multiples taux de tva pour gérer les différences de
  calculs d'arrondis entre dolibarr et doliscan (et les tickets des fournisseurs)

## 1.7.9

* Modification visuelle du bouton "sync all" pour qu'il soit différent du bouton "sync"
* Ajoute les dates de début / fin de synchronisation dans la requête envoyée au serveur doliscan (optimisation), avant
  le serveur doliscan retournait tout et c'était le module qui appliquait le filtre ... (pas super optimal)
* Corrige le user agent (signature envoyée au serveur doliscan): dans certains cas le uuid pouvait être vide
  (aucune conséquence, c'est juste pour améliorer le tracking des logs côté doliscan)
* Meilleur contrôle sur les calculs de tva partiels pour (message d'erreur si < 0)
* Utilisation du taux de TVA par défaut pour la zone géographique lorsqu'on importe des frais de carburant (avant on
  appliquait 20%, problématique pour les zones hors France Métro)
* Prise en charge du cas particulier du document justificatif déjà
  présent pour éviter un message d'erreur MySQL

## 1.7.8

* Autorise la synchronisation quotidienne (utile pour les frais payés "pro" qui deviennent des factures fournisseurs)
* ATTENTION pour la mise à jour et éviter d'avoir des lignes en doublons dans les notes de frais des utilisateurs
  allez sur la liste des notes de frais (/expensereport/list.php?leftmenu=expensereport&mainmenu=hrm)
  et supprimez les notes de frais du mois en cours qui sont encore marquées comme provisoires
  (sinon vous aurez des lignes de frais importés en doublons)
* Modification de la clé unique pour vérifier les factures fournisseurs: utilisation du champ import_key plus "solide"
  que la référence fournisseur qui pouvait être modifiée par un utilisateur dolibarr (et donc provoquer des imports
  multiples) et/ou références pas forcément uniques s'il y avait deux frais de même montant le même jour

## 1.7.5

* Corrige la signature du client dolibarr sur le serveur (aide au debug)

## 1.7.4

* Corrige un bug lié à l'implémentation multisociété
* Corrige un bug de comparaison de dates
* Accepte des champs vides sur dates de début / date de fin

## 1.7.3

* Vérifie le format de saisie de la date dans la configuration du module

## 1.7.2

* Début d'amélioration des tests de connexion internet pour les cas de problèmes de configuration initialle

## 1.7.0 + 1.7.1

* Début de compatibilité avec le module multi-société


## 1.6.2

* Améliore le libellé des frais de carburant (ajout du véhicule)
* Ajoute une dernière ligne sur les NDF pour forcer dolibarr à faire le total incluant la "dernière ligne" (sinon le montant total était erroné)
* Améliore la gestion du cas particulier du carburant: utilisation de la TVA pour la note de fais de l'utilisateur, le calcul de la spécificité fiscale n'est pas à gérer à ce moment là mais lors de la bascule en compta

## 1.6.1

* Corrige un bug important: les frais de carburant payés par un moyen presonnel étaient "ventilés" en factures
  fournisseurs et donc pas présent sur la note de frais dolibarr :-(
    -> pensez bien à désactiver le module puis le réactiver après la mise à jour
       ET ensuite
       allez dans la configuration du module pour "connecter" le "nouveau" type de frais "carburant doliscan"
       avec le frais dolibarr correspondant !

## 1.6.0

* Ajout d'une option DOLISCAN_STARTDATEIMPORT pour pouvoir spécifier la date à partir de laquelle vous
  souhaitez importer les notes de frais de DoliSCAN -> Dolibarr.

* Ajout d'une option syncDateStart et syncDateStop pour chaque utilisateur permettant de ne pas synchroniser
  les NDF d'un utilisateur en dehors des dates spécifiées (utile en cas d'arrivée dans une société avec un
  historique d'utilisation dans doliscan)

## 1.5.1

* Correction affichage des onglets dans l'administration du module

## 1.5

 * Corrige le lancement de la tâche planifiée qui télécharge toutes les notes de frais le 6 du mois pour
   être certain que le serveur DoliSCAN ait eu le temps de générer tous les PDF avant ...

## 1.4

 * Corrige un problème de droits d'accès : ajout des droits pour que les utilisateurs puissent consulter
   et supprimer leurs propres notes de frais
 * Ajoute le numéro de version du plugin dolibarr dans les requêtes réseau pour permettre d'améliorer les
   diagnostics côté serveur doliscan
 * Implémente la possibilité pour l'admin de supprimer les relations doliscan/dolibarr tout comme il peut
   les créer
 * Ajout d'un bouton "sync all" pour synchroniser toutes les notes de frais sur la page d'un utilisateur
   qui a les droits en question (en complément de la tâche planifiée qui fait la même chose)

## 1.3

 * Prise en charge de dolibarr 13

## 1.2

 * Modification de l'affichage de la liste des comptes pour n'avoir
   que les comptes actifs

## 1.1

 * Amélioration des requêtes réseau pour faciliter l'identification et les logs côté serveur DoliSCAN
 * Création d'une page permettant à un responsable RH de créer les comptes des utilisateurs du dolibarr
 * Note pour les revendeurs : Voir le fichier partner.ini pour renseigner des informations personnalisées

## 1.0

Initial version
